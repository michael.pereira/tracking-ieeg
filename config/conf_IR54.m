cfg.subject = 'IR54';

cfg.ttrial = 10;
cfg.photodiodethr = 0.9e4;
cfg.selgrid = cell(1,3);
cfg.behavorder = [1,2,1,2,1,2,1,2];
cfg.behavversion = 2;
cfg.badchannels = { ...
   'LAM10','RAM1','RAM2','RAM8','RAM9','RAM10','RHH1','RHH2','RHH3','RHH4', ...
   'ROF1','LAC5','LAC6','LAC7','LAC8','LOF5','LOF6','LOF7','LOF8','LOF9','LOF10' ...
};
cfg.acc_contra.elecs = {'RAC4','RAC5','RAC6'};
cfg.acc_contra.anat_label = 'ctx-rh-caudalanteriorcingulate';
cfg.acc_ipsi.elecs = {'LAC2','LAC3','LAC4'};  
cfg.acc_ipsi.anat_label = 'ctx-lh-caudalanteriorcingulate';

cfg.sfg_contra.elecs = {'RAC9','RAC10'};
cfg.sfg_contra.anat_label = {'ctx-lh-superiorfrontal'};

cfg.hc_contra.elecs = {'RTH2','RTH3'};
cfg.hc_contra.anat_label = {'Right-Hippocampus'};

cfg.hc_ipsi.elecs = {'LHH3'};
cfg.hc_ipsi.anat_label = {'Left-Hippocampus'};

cfg.ofc_ipsi.elecs = {'LOF3'};
cfg.ofc_ipsi.anat_label = {'ctx-lh-lateralorbitofrontal','ctx-lh-medialorbitofrontal'};
% 
cfg.recon_dir = '/Volumes/MichaelData/data/ecog/Coregistration_fieldtrip/IR54';
cfg.elec_loc_file = [cfg.recon_dir '/Recon_Mar_2017/FT_Pipeline/Electrodes/IR54_elec_mni_v.mat'];
cfg.atlas_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/aparc+aseg.mgz'];
cfg.pial_file_contra = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/rh.pial'];
cfg.pial_file_ipsi = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/lh.pial'];

%cfg.elec_loc_file = 'F:/data/ecog/IR54/Recon_Mar_2017/FT_Pipeline/Electrodes/IR54_elec_tal_f.mat';
%cfg.atlas_file = 'F:/data/ecog/IR54/freesurfer_pre-op/freesurfer/mri/aparc+aseg.mgz';
%cfg.pial_file_contra = 'F:/data/ecog/IR54/freesurfer_pre-op/freesurfer/surf/rh.pial';
%cfg.pial_file_ipsi = 'F:/data/ecog/IR54/freesurfer_pre-op/freesurfer/surf/lh.pial';

cfg.artefacts = { ...
    [ ...
    	46,47; ...
        312,314; ...
    ],[ ...
        110,114; ...
    ],[...
        152,154; ...
    ],[],[...
        281,282; ...
    ],[],...
    };
depth_grids = {'RAM','RHH','RTH','RAC','ROF','LAM','LHH','LTH','LAC','LOF'};
grid_side =   {'R',  'R',  'R',  'R',  'R',  'L',  'L',  'L',  'L',  'L'};

%%
for i=1:length(depth_grids)
cfg.selgrid{i}.name = depth_grids{i};
cfg.selgrid{i}.type = 'D';
cfg.selgrid{i}.side = grid_side{i};
cfg.selgrid{i}.selchans = cell(1,10);
for ch=1:10
    cfg.selgrid{i}.selchans{ch} = [cfg.selgrid{i}.name num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{i}.selchans,cfg.badchannels);
cfg.selgrid{i}.selchans(ibad) = [];
cfg.selgrid{i}.nsel = length(cfg.selgrid{i}.selchans);
end