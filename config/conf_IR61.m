cfg.subject = 'IR61';

cfg.ttrial = 10;
cfg.photodiodethr = 1e4;
cfg.selgrid = cell(1,3);
cfg.behavorder = [1,2,1];%???
cfg.behavversion = 2;
cfg.badchannels = { ...
   'LAM1','LAM2','LAM3','LHH1','LHH2','LHH3','LHH7','LTH1','LTH2','LOF10','LAC10',...
   'RAM2','RAM10','RHH1','RHH3','RHH4','RTH2','RTH3','RTH4','RTH5','ROF10' ...
};

%% TODO
cfg.acc_contra.elecs = {'LSMA3','LSMA4','LSMA5','LAC3','LAC4'};
cfg.acc_contra.anat_label = 'ctx-lh-caudalanteriorcingulate';
cfg.acc_ipsi.elecs = {'RSM2','RAC2','RAC3'}; % RSM in depth of the SFG
cfg.acc_contra.anat_label = 'ctx-rh-caudalanteriorcingulate';

cfg.sfg_contra.elecs = {'LOF7','LOF8','LSMA9','LAC7','LAC8'};
cfg.sfg_contra.anat_label = 'ctx-lh-superiorfrontal';
cfg.sfg_ipsi.elecs = {'RSM5','RAC6'}; % depth of the SFG!
cfg.sfg_ipsi.anat_label = 'ctx-rh-superiorfrontal';

cfg.mfg_contra.elecs = {'LIN7','LIN10'};
cfg.mfg_contra.anat_label = 'ctx-lh-caudalmiddlefrontal';
cfg.mfg_ipsi.elecs= {'RTI9','RTI10','RSM6','RSM7','RAC9','ROF9','ROF10'};
cfg.mfg_ipsi.anat_label = 'ctx-rh-caudalmiddlefrontal';

cfg.ins_ipsi.elecs = {'RIN4','RIN5','RTI2','RTI3'};
cfg.ins_ipsi.anat_label = {'ctx-rh-insula'};
cfg.ins_contra.elecs = {'LIN2','LIN3','LIN4','LTI2'};
cfg.ins_contra.anat_label = {'ctx-lh-insula'};

cfg.ifg_contra.elecs = {'LTI7','LTI8','LTI9','LTI10'};
cfg.ifg_contra.anat_label = {'ctx-lh-insula'};

cfg.ofc_contra.elecs = {'LOF2','LOF3'};
cfg.ofc_contra.anat_label = {'ctx-lh-lateralorbitofrontal','ctx-lh-medialorbitofrontal'};

cfg.recon_dir = '/Volumes/MichaelData/data/ecog/Coregistration_fieldtrip/IR61/';

 cfg.elec_loc_file = [cfg.recon_dir '/Recon_Sep_2017/FT_Pipeline/Electrodes/IR61_elec_mni_v.mat'];
 cfg.atlas_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/aparc+aseg.mgz'];
 cfg.pial_file_contra = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/lh.pial'];
 cfg.pial_file_ipsi = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/rh.pial'];%

%cfg.elec_loc_file = 'F:/data/ecog/IR57/Recon_Apr_2017/FT_Pipeline/Electrodes/IR57_elec_tal_f.mat';
%cfg.atlas_file = 'F:/data/ecog/IR57/freesurfer_post-op/freesurfer/mri/aparc+aseg.mgz';
%cfg.pial_file_contra = 'F:/data/ecog/IR57/freesurfer_post-op/freesurfer/surf/lh.pial';
%cfg.pial_file_ipsi = 'F:/data/ecog/IR57/freesurfer_post-op/freesurfer/surf/rh.pial';

cfg.artefacts = {...
    [ ...
    	433,435; ...
    ],[ ...
        156,158; ... 
        336,338; ...
        620,640; ...
        860,862; ... 
    ]};
depth_grids = {'LAM','LHH','LTH','LOF','LAC','RAM','RHH','RTH','RAC','ROF'};
grid_side =   {'L'  ,'L'  ,'L'  ,'L'  ,'L'  ,'R'  ,'R'  ,'R'  ,'R'  ,'R' };

%%
for i=1:length(depth_grids)
cfg.selgrid{i}.name = depth_grids{i};
cfg.selgrid{i}.type = 'D';
cfg.selgrid{i}.side = grid_side{i};
cfg.selgrid{i}.selchans = cell(1,10);
for ch=1:10
    cfg.selgrid{i}.selchans{ch} = [cfg.selgrid{i}.name num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{i}.selchans,cfg.badchannels);
cfg.selgrid{i}.selchans(ibad) = [];
cfg.selgrid{i}.nsel = length(cfg.selgrid{i}.selchans);
end