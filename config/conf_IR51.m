cfg.subject = 'IR51';

cfg.ttrial = 10;
cfg.photodiodethr = 1e3;
cfg.selgrid = cell(1,3);
cfg.behavorder = [1,1,1,2,2];
cfg.behavversion = 1;
cfg.badchannels = { ...
   'RHH2','RHH3','RAM2','RAM3','LTH3','LTH4','RAM10','LTH10', ...
};
cfg.acc_ipsi.elecs = {'SMA2','SMA3','SMA4','RAC2','RAC3','RAC4'};    
cfg.acc_ipsi.anat_label = 'ctx-rh-caudalanteriorcingulate';
cfg.sfg_ipsi.elecs = {'RAC7'};
cfg.sfg_ipsi.anat_label = 'ctx-rh-superiorfrontal';
cfg.mfg_ipsi.elecs = {'ROF9','ROF10','RIN7','RIN8','RIN9','RAC9','SMA8','SMA9','SMA10'};
cfg.mfg_ipsi.anat_label = 'ctx-rh-caudalmiddlefrontal';

cfg.ofc_ipsi.elecs = {'ROF2','ROF3'};
cfg.ofc_ipsi.anat_label = {'ctx-rh-lateralorbitofrontal','ctx-rh-medialorbitofrontal'};

cfg.ins_ipsi.elecs = {'RIN2'};
cfg.ins_ipsi.anat_label = {'ctx-rh-insula'};
cfg.ins_contra.elecs = {'LAM6'};
cfg.ins_contra.anat_label = {'ctx-lh-insula'};

cfg.hc_contra.elecs = {'LHH2','LHH3'};
cfg.hc_contra.anat_label ={'Left-Hippocampus'};

cfg.recon_dir = '/Volumes/MichaelData/data/ecog/Coregistration_fieldtrip/IR51/';
cfg.elec_loc_file = [cfg.recon_dir '/Recon_Feb_2017/FT_Pipeline/Electrodes/IR51_elec_mni_v.mat'];
cfg.atlas_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/aparc+aseg.mgz'];
cfg.t1_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/T1.mgz'];
cfg.pial_file_contra = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/rh.pial'];
cfg.pial_file_ipsi = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/lh.pial'];

%cfg.elec_loc_file = 'F:/data/ecog/IR51/Recon_Feb_2017/FT_Pipeline/Electrodes/IR51_elec_tal_f.mat';
%cfg.atlas_file = 'F:/data/ecog/IR51/freesurfer/mri/aparc+aseg.mgz';
%cfg.t1_file = 'F:/data/ecog/IR51/freesurfer/mri/T1.mgz';
%cfg.pial_file_contra = 'F:/data/ecog/IR51/freesurfer/surf/rh.pial';
%cfg.pial_file_ipsi = 'F:/data/ecog/IR51/freesurfer/surf/lh.pial';

cfg.artefacts = {[ ...
    1220, 1222
]};
depth_grids = {'ROF','RIN','RAC','SMA','RAM','RHH','RTH','LAM','LTH','LHH'};
grid_side =   {'R',  'R',  'R',  'R',  'R',  'R',  'R',  'L',  'L',  'L'};
%%
for i=1:length(depth_grids)
cfg.selgrid{i}.name = depth_grids{i};
cfg.selgrid{i}.type = 'D';
cfg.selgrid{i}.side = grid_side{i};
cfg.selgrid{i}.selchans = cell(1,10);
for ch=1:10
    cfg.selgrid{i}.selchans{ch} = [cfg.selgrid{i}.name num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{i}.selchans,cfg.badchannels);
cfg.selgrid{i}.selchans(ibad) = [];
cfg.selgrid{i}.nsel = length(cfg.selgrid{i}.selchans);
end