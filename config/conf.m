cfg.fs = 500;

cfg.datadir = '/Volumes/MichaelData/data/ecog/';
cfg.matdir = '/Volumes/MichaelData/data/ecog/preproc/';
%cfg.datadir = 'F:/data/ecog/';
%cfg.matdir = 'F:/data/ecog/preproc/';

if 1
fpass = 2*[1,150]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.bp = design(fd);

if 1
fpass = 2*[1,15]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.low = design(fd);

fpass = 2*[3,7]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.theta = design(fd);

fpass = 2*[8,14]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.mu = design(fd);

fpass = 2*[15,30]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.beta = design(fd);

fpass = 2*[18,25]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.beta2 = design(fd);

fpass = 2*[40,70]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.lgamma = design(fd);

end
fpass = 2*[70,80]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma1 = design(fd);
fpass = 2*[80,90]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma2 = design(fd);
fpass = 2*[90,100]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma3 = design(fd);
fpass = 2*[100,110]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma4 = design(fd);
fpass = 2*[110,120]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma5 = design(fd);
fpass = 2*[120,130]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma6 = design(fd);
fpass = 2*[130,140]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma7 = design(fd);
fpass = 2*[140,150]./cfg.fs;
fd = fdesign.bandpass('N,F3dB1,F3dB2',8,fpass(1),fpass(2));
cfg.filters.band.hgamma8 = design(fd);

%fpass = 2*[200]./cfg.fs;
%fd = fdesign.highpass('N,F3dB',8,fpass);
%cfg.filters.band.hgamma = design(fd);

f0 = 2*60/cfg.fs;
fbw = 2*3/cfg.fs;
fdesign0 = fdesign.notch('N,F0,BW',8,1*f0,fbw);
fdesign1 = fdesign.notch('N,F0,BW',8,2*f0,fbw);
fdesign2 = fdesign.notch('N,F0,BW',8,3*f0,fbw);
fdesign3 = fdesign.notch('N,F0,BW',8,4*f0,fbw);
cfg.filters.notch.notch0 = design(fdesign0);
cfg.filters.notch.notch1 = design(fdesign1);
cfg.filters.notch.notch2 = design(fdesign2);
cfg.filters.notch.notch3 = design(fdesign3);
end