cfg.subject = 'IR49';

cfg.ttrial = 10;
cfg.photodiodethr = 1e4;
cfg.selgrid = cell(1,1);
cfg.behavorder = [1,1,1,2,2];
cfg.behavversion = 1;
cfg.badchannels = { ...
    'LF17','LF24','LF31','LF49','LF50','LF54','LF23','LF32','LF57','LF58'...
};
cfg.artefacts = [ ...
    433,446+10;...
    685,703+2;...
    829,838+2;...
    882,890+2;...
    894,907+10;...
    941,956+2;...
].*500;


%%
cfg.selgrid{1}.name = 'LF';
cfg.selgrid{1}.type = 'G';
cfg.selgrid{1}.selchans = cell(1,64);
for ch=1:64
    cfg.selgrid{1}.selchans{ch} = [cfg.selgrid{1}.name num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{1}.selchans,cfg.badchannels);
cfg.selgrid{1}.selchans(ibad) = [];
cfg.selgrid{1}.nsel = length(cfg.selgrid{1}.selchans);
cfg.selgrid{1}.side = 'L';

