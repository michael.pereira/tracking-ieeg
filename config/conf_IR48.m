cfg.subject = 'IR48';

cfg.ttrial = 10;
cfg.photodiodethr = 1e4;
cfg.selgrid = cell(1,3);
cfg.behavorder = [1,1,2,2];
cfg.behavversion = 1;
cfg.badchannels = { ...
    'FG25','FG27','FG28','FG29','FG37','FG38','FG43','FG44','FG46','FG47','AF12'...
};
cfg.artefacts = {[  131,138+2],[],[],[]}; ...


cfg.sfg_ipsi.elecs = {'MF1','MF2','MF3','MF4','MF5','MF6','AF7'};
cfg.sfg_ipsi.anat_label ='ctx-rh-superiorfrontal';


cfg.acc_ipsi.elecs = {'MF7','MF8','MF9','MF10','MF11','MF12'};
cfg.acc_ipsi.anat_label ='ctx-rh-caudalmiddlefrontal';

cfg.mfg_ipsi.elecs = {'AF3','AF4','AF10','AF8','FG62','FG61','FG52','FG60','FG51','FG59'};
cfg.ifg_ipsi.elecs = {'AF11','AF6','AF9'};

cfg.parietalsup_ipsi.elecs = {'FG8','FG16','FG24','FG32','FG15','FG22','FG21'};
cfg.parietalinf_ipsi.elecs = {'FG13','FG12','FG20'};
cfg.supramarginal_ipsi.elecs = {'FG3','FG11','FG19','FG27','FG2'};
cfg.ipl_ipsi.elecs = {'FG10','FG50','FG58','FG49','FG57'};

cfg.postcentral_ipsi.elecs = {'FG30','FG38','FG37','FG36','FG35','FG26','FG33'};
cfg.precentral_ipsi.elecs = {'FG46','FG54','FG45','FG53','FG44','FG34','FG42'};

cfg.recon_dir = '/Volumes/MichaelData/data/ecog/Coregistration_fieldtrip/IR48/';
cfg.elec_loc_file = [cfg.recon_dir '/Recon_Nov_2016/FT_Pipeline/Electrodes/IR48_elec_mni_v.mat'];
cfg.atlas_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/aparc+aseg.mgz'];
cfg.t1_file = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/mri/T1.mgz'];
cfg.pial_file_contra = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/rh.pial'];
cfg.pial_file_ipsi = [cfg.recon_dir '/freesurfer_pre-op/freesurfer/surf/lh.pial'];


x = 0;
cfg.fr_ipsi = {};
for i=1:64
    el = ['FG' num2str(i)];
    if ~any(strcmp(cfg.badchannels,el))
        x = x+1;
        cfg.fr_ipsi.elecs{x} = el;
    end
end

cfg.selgrid{1}.name = 'FG';
cfg.selgrid{1}.type = 'G';
cfg.selgrid{1}.selchans = cell(1,64);
for ch=1:64
    cfg.selgrid{1}.selchans{ch} = ['FG' num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{1}.selchans,cfg.badchannels);
cfg.selgrid{1}.selchans(ibad) = [];
cfg.selgrid{1}.nsel = length(cfg.selgrid{1}.selchans);
cfg.selgrid{1}.side = 'R';

cfg.frontal.elecs = cfg.selgrid{1}.selchans;
cfg.frontal.anat_label ='ctx-rh-caudalmiddlefrontal';


%%
cfg.selgrid{2}.name = 'MF';
cfg.selgrid{2}.type = 'S';
cfg.selgrid{2}.selchans = cell(1,12);
for ch=1:12
    cfg.selgrid{2}.selchans{ch} = ['MF' num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{2}.selchans,cfg.badchannels);
cfg.selgrid{2}.selchans(ibad) = [];
cfg.selgrid{2}.nsel = length(cfg.selgrid{2}.selchans);
cfg.selgrid{2}.side = 'R';

%%
cfg.selgrid{3}.name = 'AF';
cfg.selgrid{3}.type = 'S';
cfg.selgrid{3}.selchans = cell(1,12);
for ch=1:12
    cfg.selgrid{3}.selchans{ch} = ['AF' num2str(ch)];
end
[~,ibad] = intersect(cfg.selgrid{3}.selchans,cfg.badchannels);
cfg.selgrid{3}.selchans(ibad) = [];
cfg.selgrid{3}.nsel = length(cfg.selgrid{3}.selchans);
cfg.selgrid{3}.side = 'R';

%cfg.selchans = {cfg.selgrid{1}.selchans{:} cfg.selgrid{2}.selchans{:} cfg.selgrid{3}.selchans{:} };