clear
addpath toolboxes
%addpath /Volumes/MichaelData/code/ECoG_analysis/toolboxes/fieldtrip-20170721 
addpath /Users/michaelpereira/Dropbox/Work/Projects/ecog-tracking/analysis/toolboxes/fieldtrip-20170721 

addpath config
conf

doVisual = 0;

for s=[5]
    if s==1
        conf_IR48;
    elseif s==2
        conf_IR49;
    elseif s==3
        conf_IR51;
    elseif s==4
        conf_IR54;
    elseif s==5
        conf_IR57;
    elseif s==6
        conf_IR61;
    end
    
    
    %%
    fprintf('** Prealocating %s\n',cfg.subject);
    besafiles = dir([cfg.datadir '/' cfg.subject '/*.besa']);
    nTotSamp = 0;
    % for each ECoG datafile, we check the total number of samples after
    % downsampling
    for bf = 1:length(besafiles)
        file = [cfg.datadir '/' cfg.subject '/' besafiles(bf).name];
        header = read_besa_besa(file);
        dwnsfac = header.Fs/cfg.fs;
        nTotSamp = nTotSamp + floor(header.nSamples / dwnsfac);
    end
    % now we can pre-allocate the structures
    resamp = cell(1,length(cfg.selgrid));
    for grid = 1:length(cfg.selgrid)
        % prepare structure
    

        nChans = cfg.selgrid{grid}.nsel - (cfg.selgrid{grid}.type == 'D');
        
        resamp{grid} = struct();
        resamp{grid}.notched = zeros(nTotSamp,nChans);
        resamp{grid}.bad = false(nTotSamp,nChans);

        bands = fieldnames(cfg.filters.band);
        %for fb = bands.'
        %    resamp{grid}.(cell2mat(fb)) = zeros(nTotSamp,nChans);
        %    resamp{grid}.([cell2mat(fb) '_hilb']) = zeros(nTotSamp,nChans);
        %end
    end
    status = zeros(nTotSamp,1);
    bad = zeros(nTotSamp,1);
    
    %%
    kStart = 0;
    % for each datafile...
    for bf = 1:length(besafiles)
        % load the file
        file = [cfg.datadir '/' cfg.subject '/' besafiles(bf).name];
        header = read_besa_besa(file);
        data = read_besa_besa(file, header,[],[], 2);
        % resample photodiode channel and aggregate
        statuschannel = downsample(data,dwnsfac); clear data
        nSamp = length(statuschannel);
        
        bad_ = false(size(statuschannel));
        nar = size(cfg.artefacts{bf},1);
        for ar = 1:nar
            if cfg.artefacts{bf}(ar,2)*cfg.fs < nSamp
                bad_((cfg.artefacts{bf}(ar,1)*cfg.fs+1):cfg.artefacts{bf}(ar,2)*cfg.fs) = 1;
            else
                fprintf('/!\ artefact out of size\n');
            end
        end
        resamp_ = filter(cfg.filters.notch.notch0,statuschannel);
        resamp_ = filter(cfg.filters.notch.notch0,resamp_(end:-1:1));
        % notch F1
        resamp_ = filter(cfg.filters.notch.notch1,resamp_(end:-1:1));
        resamp_ = filter(cfg.filters.notch.notch1,resamp_(end:-1:1));
        % notch F2
        resamp_ = filter(cfg.filters.notch.notch2,resamp_(end:-1:1));
        resamp_ = filter(cfg.filters.notch.notch2,resamp_(end:-1:1));
        % notch F3
        resamp_ = filter(cfg.filters.notch.notch3,resamp_(end:-1:1));
        resamp_ = filter(cfg.filters.notch.notch3,resamp_(end:-1:1));
        
        status(kStart+(1:nSamp)) = resamp_(end:-1:1); clear statuschannel resamp_
        bad(kStart+(1:nSamp)) = bad_;
        elecstruct = cell(1,length(cfg.selgrid));
        % for each grid...
        for grid = 1:length(cfg.selgrid)
            % select good channels
            if isempty(cfg.selgrid{grid}.selchans)
                continue;
            end
            [~,isel] = intersect(header.label,cfg.selgrid{grid}.selchans);
            isel = sort(isel);
            fprintf('** Loading electrode set %s from %s\n',cfg.selgrid{grid}.name,file);
            if isempty(isel)
                error('Electrodes not found');
            end
            % read data
            data = read_besa_besa(file, header,[],[], isel);
            
            % resample each channel
            rdata = zeros(nSamp,length(isel));
            for ch = 1:length(isel)
                rdata(:,ch) = resample(data(ch,:),1,dwnsfac);
            end
            clear data
            
            n = cfg.selgrid{grid}.nsel;
            if (cfg.selgrid{grid}.type == 'G') || (cfg.selgrid{grid}.type == 'S')
                fprintf('   ++ applying CAR referencing (grid or stripe)\n');
                rerefmat = eye(n) - 1/n;
                elecstruct{grid}.nsel = cfg.selgrid{grid}.nsel;
                elecstruct{grid}.selchans = cfg.selgrid{grid}.selchans;
                
            elseif (cfg.selgrid{grid}.type == 'D')
                fprintf('   ++ applying bipolar referencing (depth)\n');
                rerefmat = eye(n) -  triu(ones(n,n),-1) + triu(ones(n,n),0);
                rerefmat(:,end) = [];
                elecstruct{grid}.nsel = cfg.selgrid{grid}.nsel - 1;
                elecstruct{grid}.selchans = cfg.selgrid{grid}.selchans(2:end);
                
            else
                fprintf('   !! unknown type: %s, NOT REFERENCING\n',cfg.selgrid{grid}.type);
                rerefmat = eye(n);
            end
            elecstruct{grid}.name = cfg.selgrid{grid}.name;
            elecstruct{grid}.type = cfg.selgrid{grid}.type;
            elecstruct{grid}.side = cfg.selgrid{grid}.side;
            
            
            % re-reference
            rdata = rdata*rerefmat;
            
            fprintf('   ++ downsampling and filtering channels\n');
            for ch = 1:elecstruct{grid}.nsel
                % notch F0
                resamp_ = filter(cfg.filters.notch.notch0,rdata(:,ch));
                resamp_ = filter(cfg.filters.notch.notch0,resamp_(end:-1:1));
                %% notch F1
                %resamp_ = filter(cfg.filters.notch.notch1,resamp_(end:-1:1));
                %resamp_ = filter(cfg.filters.notch.notch1,resamp_(end:-1:1));
                % notch F2
                resamp_ = filter(cfg.filters.notch.notch2,resamp_(end:-1:1));
                resamp_ = filter(cfg.filters.notch.notch2,resamp_(end:-1:1));
                
                % aggregate
                resamp{grid}.notched(kStart+(1:nSamp),ch) = resamp_(end:-1:1);

            end
            
            
            clear resamp_ rdata
            
        end
        status(kStart + nSamp) = 1e6;
        kStart = kStart + nSamp;
        
        if doVisual
            %%
            conf = [];
            conf.viewmode = 'vertical';
            conf.continuous = 'yes';
            conf.plotlabels = 'yes';
            conf.blocksize = 10;
            % this sub-field is where the output of bad epoch indices will be
            conf.artfctdef.visual.artifact = cfg.artefacts{bf};
            
            data = [];
            full_data = [];
            label = {};
            
            for g=1:length(resamp)
                full_data = cat(1,full_data,resamp{g}.notched.');
                label = {label{:} cfg.selgrid{g}.selchans{1:end-1}};
            end
            data.trial = {full_data};
            n = size(full_data,2);
            data.time = {linspace(0,n/500,n)};
            data.fsample = cfg.fs;
            data.label = label;
            
            badness = false(size(status));
            conf = ft_databrowser(conf,data);
            
        end
        
    end
    
    
    %%
    fprintf('** Searching for epochs, keeping; ');
    
    [~,trigStart] = findpeaks(diff(status),'MinPeakHeight',cfg.photodiodethr);
    [~,trigEnd] = findpeaks(-diff(status),'MinPeakHeight',cfg.photodiodethr);
    figure(100); clf; hold on;
    plot(linspace(0,length(status)/cfg.fs,length(status)),status,'c--')
    plot(linspace(0,length(status)/cfg.fs,length(status)-1),diff(status),'b')
    plot(xlim,cfg.photodiodethr*[1 1],'r');
    plot(xlim,-cfg.photodiodethr*[1 1],'r');
    tr = 0;trialidx = []; 
    for tr_=1:length(trigStart)
        inext = find(trigEnd > trigStart(tr_),1,'first');
        if ~isempty(inext)
            dt = trigEnd(inext) - trigStart(tr_);
            if (dt/cfg.fs >cfg.ttrial-3) && (dt/cfg.fs < cfg.ttrial+1)
                fprintf(' [%.2f s->Y]',dt/cfg.fs);
                
                tr = tr + 1;
                trialidx(tr) = trigStart(tr_);
            else
                fprintf(' [%.2f s->N]',dt/cfg.fs);
            end
        end
    end
    stem(trialidx/cfg.fs,trialidx*0+3e5,'k*');
    fprintf('\n');
    
    fprintf('** filtering and epoching (%d trials)\n',length(trialidx));
    epochs = cell(1,length(cfg.selgrid));
    
    for grid = 1:length(cfg.selgrid)
        for fb = bands.'
            filtdata = zeros(length(status), elecstruct{grid}.nsel);
            filtdata_hilb = zeros(length(status), elecstruct{grid}.nsel);
            for ch = 1: elecstruct{grid}.nsel           
                % band-pass filter
                %fprintf('[ %s ]',cell2mat(fb));
                ff_ = filter(cfg.filters.band.(cell2mat(fb)),resamp{grid}.notched(:,ch));
                bw_ = filter(cfg.filters.band.(cell2mat(fb)),ff_(end:-1:1));
                filtdata(:,ch) = bw_(end:-1:1);
                filtdata_hilb(:,ch) = hilbert(bw_(end:-1:1));
            end
            epochs{grid}.(cell2mat(fb)) = zeros(cfg.fs*(cfg.ttrial+3), elecstruct{grid}.nsel);
            epochs{grid}.([cell2mat(fb) '_hilb']) = zeros(cfg.fs*(cfg.ttrial+3), elecstruct{grid}.nsel);

            for tr=1:length(trialidx)
                epochs{grid}.(cell2mat(fb))(:,:,tr) = filtdata(trigStart(tr)+(-cfg.fs*3:cfg.fs*cfg.ttrial-1),:);
                epochs{grid}.([cell2mat(fb) '_hilb'])(:,:,tr) = filtdata_hilb(trigStart(tr)+(-cfg.fs*3:cfg.fs*cfg.ttrial-1),:);
            end  
              
        end
        for tr=1:length(trialidx)
            epochs{grid}.notched(:,:,tr) = resamp{grid}.notched(trigStart(tr)+(0:cfg.fs*cfg.ttrial-1),:);
        end
    end
    %%
    fprintf('** Loading behavioral data\n');
    behavfiles = dir([cfg.datadir '/' cfg.subject '/' cfg.subject '*_s*_behav_*.txt']);
    btr = 0;
    btrial = [];
    ltrial = [];
    dtrial = [];
    bfile = [];
    for f = 1:length(behavfiles)
        behavfile = [cfg.datadir '/' cfg.subject '/' behavfiles(f).name];
        
        scorefile = [cfg.datadir '/' cfg.subject '/' strrep(behavfiles(f).name,'behav','score')];
        scores = dlmread(scorefile);
        
        fprintf('      - %d) behav file %s\n',f,behavfile);
        bdata = loadbehav(dlmread(behavfile),50,cfg.fs,cfg.behavversion);
        b = 0;
        
        if strcmp(behavfiles(f).name,'IR49_s0_behav_12102016_105521.txt')
            fprintf('      !! This file starts from run 2\n');
            b = 1;
        end
        
        for btr_=1:length(bdata.mouse)-b
            cond = cfg.behavorder(f);
            if (cond == 2) && (mod(btr_,2) == 0)
                cond = cond+1;
            end
            
            btr = btr+1;
            ltrial(btr) = cond;
            dtrial(btr) = scores(btr_,3);
            bfile(btr) = f;
            btrial(:,1,btr) = bdata.speed{btr_+b};
            btrial(:,2,btr) = bdata.accel{btr_+b};
            btrial(:,3,btr) = bdata.mouse{btr_+b};
            btrial(:,4,btr) = bdata.velocity{btr_+b};
            btrial(:,5,btr) = bdata.target{btr_+b};
            btrial(:,6,btr) = bdata.err{btr_+b};
            btrial(:,7,btr) = bdata.serr{btr_+b};
            btrial(:,8,btr) = bdata.mousedir{btr_+b};
            btrial(:,9,btr) = bdata.targetdir{btr_+b};
            
        end
        
        
    end
    behavchnames = {'SP','ACC','POS','VEL','TPOS','ERR','SERR','DIR','TDIR'};
    fprintf('** found  %d trials for ephys and %d trials for behav\n',length(trialidx),btr);
    %%
    filebase = [cfg.matdir '/' cfg.subject];
    
    bsavefile = [filebase '_behav.mat'];
    save(bsavefile,'ltrial','dtrial','btrial','bad');
    
    for fn = fieldnames(epochs{grid}).'
        
        trials = cell(1,length(cfg.selgrid));
        esavefile = [filebase '_elphys_' cell2mat(fn) '.mat'];
        for grid = 1:length(cfg.selgrid)
            trials{grid}.(cell2mat(fn)) = epochs{grid}.(cell2mat(fn));
            trials{grid}.elecs = elecstruct{grid};
        end
        save(esavefile,'trials');
        fprintf('** saved %s\n',esavefile);
 
    end
    
    fprintf('** saved %s\n',bsavefile);
    
end
