clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};%
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
%cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = {'hgamma1','hgamma2','hgamma3','hgamma4','hgamma5','hgamma6','hgamma7','hgamma8'};
%band = {'beta'};

norm = 'scale';

for subj=[1,3:5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'btrial','dtrial','ltrial');
    acc = btrial(:,2,ltrial==1);
    accthr = std(acc(:));
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    specpow = 0;
    for b=1:length(band)
        efile = [cfg.matdir '/' subject '_elphys_' band{b} '_hilb.mat'];
        d2 = load(efile,'trials');
        [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
        if isempty(elecs.elecpos)
            error('No electrodes');
        end
        specpow_ = dataspecpow((cfg.fs*3+1):end,trialidx1,:);
        base_ = dataspecpow(cfg.fs:cfg.fs*3,trialidx1,:);
        if strcmp(norm,'ers')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),mean(abs(base_).^2,1));
        elseif strcmp(norm,'zscore')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),std(abs(base_).^2 ,1));
        elseif strcmp(norm,'scale')
            specpow_ = bsxfun(@rdivide,abs(specpow_).^2,std(abs(base_).^2 ,1));    
        elseif strcmp(norm,'erp')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,real(specpow_),mean(real(base_),1)),mean(real(base_),1));
        elseif strcmp(norm,'raw')
            specpow_ = real(specpow_);
        end
        specpow = specpow + specpow_;
    end
    specpow = specpow./b;
    
    
    efile = [cfg.matdir '/' subject '_elphys_theta_hilb.mat'];
    d1 = load(efile,'trials');
    theta = select_electrodes(d1.trials,cfg,anat);
    theta(1:cfg.fs*3,:,:) = [];
    
    thetaphase = angle(theta);
    thetareal = real(theta);
    
    
    
    %% GET EEG EPOCHS
    
    nbins = 11;
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    nperm = 200;
    acc = squeeze(btrial(:,2,ltrial==1));
    err = squeeze(real(btrial(:,6,ltrial==1)));
    dev = squeeze(real(btrial(:,7,ltrial==1)));
    
    acc_ = acc;
    acc = zscore(acc(:));
    err = zscore(err(:));
    
    phacc = struct();
    
    pheeg = struct();
    pheeg_ = struct();
  
    bins = linspace(-pi,pi,nbins+1);
    
    figure(subj*10+200); clf;
    figure(subj*10+201); clf;
    
    thrsig = dev(:);
    thr = 0;%median(acc_(:));
    
    for e=1:ne
        
        %%
        ph = thetaphase(:,ltrial==1,e);
        %ph = angle(hilbert(acc_));
        
        %ph = circshift(ph(:),[1231 0]);
        
        spec = (specpow(:,ltrial==1,e));
        %spec = zscore(spec(:));
        
        phamp_acc = abs(sum(acc(:).*exp(1i*ph(:))));
        phamp_eeg = abs(sum(spec(:).*exp(1i*ph(:))));
        [ phacc ] = pacbin(phacc, ph, acc ,thrsig, thr, bins, e);
        [ pheeg ] = pacbin(pheeg, ph, spec ,thrsig, thr, bins, e);

        for i=1:nperm
            ph_ = circshift(ph(:),[round(cfg.fs+cfg.fs*8*rand()),0]);
            sphamp(i) = abs(sum(acc(:).*exp(1i*ph_(:))));  
        end
        pacc(e) = mean(sphamp > phamp_acc);
        
        for i=1:nperm
            ph_ = circshift(ph(:),[round(cfg.fs+cfg.fs*8*rand()),0]);
            sphamp(i) = abs(sum(spec(:).*exp(1i*ph_(:)))); 
            %[ pheeg_ ] = pacbin(pheeg_, ph_, spec ,thrsig, thr, bins, e);
            %spdiff(:,i) = pheeg_.t_diff(:,e);
        end
        %fprintf('\n');
        peeg(e) = mean(sphamp > phamp_eeg);
        
        
        figure(subj*10+200);
        subplot(3,3,e); hold on;
        theta = bins(1:end-1) + mode(diff(bins))./2;
        % errorbar(theta,phacc.all(:,e),phacc.all_sem(:,e),'c');
        plot(1,NaN,'c');
        %errorbar(theta,pherr(:,e),pherr_sem(:,e),'r');
        errorbar(theta,pheeg.all(:,e),pheeg.all_sem(:,e),'k');
        xlim([-pi,pi]);
        legend({sprintf('Accel. (p=%.3f)',pacc(e)),sprintf('HGA (p=%.3f)',peeg(e))});
       
        
        th = thetareal(:,ltrial==1,e);
        w = 10;
        [~,ipk] = findpeaks(th(:));
        ipk(ipk-cfg.fs/2 <= 0) = [];
        ipk(ipk+cfg.fs/2 > length(spec(:))) = [];
        pkhga = zeros(length(ipk),1);
        pkdev = zeros(length(ipk),cfg.fs+1);
        pkacc = zeros(length(ipk),1);
        for p=1:length(ipk)
            pkhga(p) = mean(spec(ipk(p)+(-w:w)));
            pkdev(p,:) = dev(ipk(p)+(-cfg.fs/2:cfg.fs/2));
            pkacc(p) = mean(acc(ipk(p)+(-w:w)));
        end
%         [~,ppk] = ttest2(pkhga(pkdev < 0),pkhga(pkdev > 0));
%         if ppk<0.05
%             tit = [tit ' *'];
%         else
%             tit = [tit ' -'];
%         end
        [~,ith] = findpeaks(-th(:));
        ith(ith-cfg.fs/2 <= 0) = [];
        ith(ith+cfg.fs/2 > length(spec(:))) = [];
        thhga = zeros(length(ith),1);
        thdev = zeros(length(ith),cfg.fs+1);
        thacc = zeros(length(ith),1);
        for p=1:length(ith)
            thhga(p) = mean(spec(ith(p)+(-w:w)));
            thdev(p,:) = dev(ith(p)+(-cfg.fs/2:cfg.fs/2));
            thacc(p) = mean(acc(ith(p)+(-w:w)));
        end
        %[~,pth] = ttest2(thhga(thdev < 0),thhga(thdev > 0));
        %if pth<0.05
        %    tit = [tit ' *'];
        %else
        %    tit = [tit ' -'];
        %end
        c1 = corr(pkhga(1:end),(pkdev));
        c2 = corr(thhga(1:end),(thdev));
        c1d = corr(pkhga(2:end),diff(pkdev));
        c2d = corr(thhga(2:end),diff(thdev));
        title(sprintf('%s (%s)',subject,elecs.labels{e}));

         figure(subj*10+201);
         subplot(3,3,e); hold on;
         t = linspace(-0.5,0.5,cfg.fs+1);
         plot(t,c1,'b');
         plot(t,c2,'c');
         plot(t,c1d,'r--');
         plot(t,c2d,'m--');
                 title(sprintf('(%.2f,%.2f)',max(abs(c1)),max(abs(c2))));

        pause(0.1);
    end
   
    
    
    
end