function [ tsum,clusters ] = m_clustertstattopo( tstat,p,pthr,elecs)
%M_CLUSTERTSTAT Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    elecs = 1:64;
end
clusters = {};
montage = [...
    0  0  0  0  1  1+32  2+32  0     0     0     0    ;...
    0  0  0  2  3  5+32  4+32  3+32  0     0     0    ;...
    0  7  6  5  4  6+32  7+32  8+32  9+32 10+32  0    ;...
    0  8  9 10 11 15+32 14+32 13+32 12+32 11+32  0    ;...
    0 15 14 13 12 16+32 17+32 18+32 19+32 20+32  0    ;...
    0 16 17 18 19 32    24+32 23+32 22+32 21+32  0    ;...
    24 23 22 21 20 31    25+32 26+32 27+32 28+32 29+32 ;...
    0  0  0 25 26 30    31+32 30+32  0     0     0    ;...
    0  0  0  0 27 29    32+32  0     0     0     0    ;...
    0  0  0  0  0 28     0     0     0     0     0    ];
montage(montage==0) = NaN;
p_montage = montage*NaN;
tstat_montage = montage*NaN;


for el = elecs
    p_montage(montage==el) = p(el);
end
for el = elecs
    tstat_montage(montage==el) = tstat(el);
end



permclust1 = bwconncomp((p_montage < pthr) & sign(tstat_montage)>0);
permclust2 = bwconncomp((p_montage < pthr) & sign(tstat_montage)<0);

tsum = zeros(1,permclust1.NumObjects+permclust2.NumObjects);
for c1 = 1:permclust1.NumObjects
    idx = permclust1.PixelIdxList{c1};
    tsum(c1) = sum(tstat_montage(idx));
    clusters{c1}.idx = montage(permclust1.PixelIdxList{c1});
    clusters{c1}.tsum = tsum(c1);
end

for c2 = 1:permclust2.NumObjects
    idx = permclust2.PixelIdxList{c2};
    tsum(permclust1.NumObjects+c2) = sum(tstat_montage(idx));
    clusters{permclust1.NumObjects+c2}.idx = montage(permclust2.PixelIdxList{c2});
    clusters{permclust1.NumObjects+c2}.tsum = tsum(c2);
end



end

