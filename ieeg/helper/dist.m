function [ d ] = dist( a,b, w )
%DIST Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    w = ones(1,size(a,2));
end

na = size(a,1);
nb = size(b,1);
d = nan(na,nb);
for i=1:na
   d_ = bsxfun(@times,w,(bsxfun(@minus,b,a(i,:))));
   d(i,:) = (mean(d_.*conj(d_),2));
end
d = log(d+1);
s = std(d(:));
m = mean(d(:));
d = d./s;
%d = (d - m)./s;

end

