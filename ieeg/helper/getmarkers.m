
function markers = getmarkers(trigger_channel)

% Extracts trigger onsets from triggers channel, removing onset
triggers = trigger_channel(:)-mode(trigger_channel) ;
% no negative triggers
triggers(triggers<0) = 0;
% find rising edge by differentiation
dtriggers = diff(triggers);

markers.position = find(dtriggers>0)+1;
markers.value = triggers(markers.position);

% some recordings had values of 25 instead of 23
markers.value(markers.value==25) = 23;


    

    
    
    
    
    
    
    
    



