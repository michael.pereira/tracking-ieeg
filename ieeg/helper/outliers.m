function [ out ] = outliers( data, w )
%UNTITLED11 Summary of this function goes here
%   Detailed explanation goes here
if nargin == 1
    w = 1.5;
end

q1 = quantile(data,0.25);
q3 = quantile(data,0.75);
out = (data > q3 + w*(q3-q1)) | (data < q1 - w*(q3-q1));
end

