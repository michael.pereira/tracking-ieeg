function plot_on_mri( t1,eleccoord,color,markercolor,spl)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if (nargin < 5)
    spl=1;
end
if (nargin < 4)
    markercolor = [];
end
if (nargin < 3)
    color = [];
end

t = pinv(t1.hdr.vox2ras);

slice = round([eleccoord(:).' 1]*t.');

if isfield(t1,'anatomy')
    anat = t1.anatomy;
    if isempty(color)
        color = grey(255);
    end
    if isempty(markercolor)
        markercolor = 'r';
    end
else
    for f=fieldnames(t1).'
        sz = size(t1.(cell2mat(f)));
        if (length(sz)==3) && all(size(t1.(cell2mat(f))) == [256 256 256])
            anat = t1.(cell2mat(f));
            fprintf('Found anatomy: %s\n',cell2mat(f));
        end
    end
    if isempty(color)
        color = jet(255);
    end
    if isempty(markercolor)
        markercolor = 'w';
    end
    
end


%%
if spl
    figure();
    subplot(1,3,1); hold on;
else
    figure(); hold on;
end
imagesc(squeeze(anat(slice(1),:,:)));
colormap(color);
plot(slice(3),slice(2), [markercolor 'o'],'MarkerFaceColor',markercolor,'MarkerSize',3)
axis ij
axis square
axis([1,256,1,256])
set(gca,'XTick',[],'YTick',[]);

%%
if spl
    subplot(1,3,2); hold on;
else
    figure(); hold on;
end
imagesc(squeeze(anat(:,slice(2),:)));
colormap(color);
plot(slice(3),slice(1), [markercolor 'o'],'MarkerFaceColor',markercolor,'MarkerSize',3)
axis ij
axis square
axis([1,256,1,256])
set(gca,'XTick',[],'YTick',[]);

%%
if spl
    subplot(1,3,3); hold on;
else
    figure(); hold on;
end
imagesc(squeeze(anat(:,:,slice(3))).');
colormap(color);
plot(slice(1),slice(2), [markercolor 'o'],'MarkerFaceColor',markercolor,'MarkerSize',3)
axis ij
axis square
axis([1,256,1,256])
set(gca,'XTick',[],'YTick',[]);
p = get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),560,200]);

end

