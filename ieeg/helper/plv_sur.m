function [ pl,plnorm,p] = plv_sur( x,y,nsur,n,m,eog)
%PLV Summary of this function goes here
%   Detailed explanation goes here

if nargin < 6
    eog = x*0;
end
if nargin < 5
    n = 1;
end
if nargin < 4
    m = 1;
end
per = 256;
[l,ntrials] = size(x);
hx = hilbert(x);
hy = hilbert(y);

pl_ = plv(hx,hy,n,m);
pl_(eog == 1) = NaN;
pl_(eog == 1) = NaN;
pl = pl_(129:end-128,:);


splv = zeros(nsur,ntrials);
if nsur > 0
   for i=1:nsur
       
       if prod(1:ntrials) >= nsur
           perm = randperm(size(hx,2));
           sx = hx(:,perm);
           seog = eog(:,perm);
       else
           perm = randi([per+1,l-per],1);
           sx = hx([perm+1:l, 1:perm],:);
           seog = eog([perm+1:l, 1:perm],:);
           perm_(i) = perm;
       end

       spl_ = plv(sx,hy,n,m);
       spl_(seog == 1) = NaN;
       spl_ = spl_(129:end-128,:);
       splv(i) = abs(nanmean(spl_(:),1));
   end
end

%p = mean(phaselock > quantile(splv,0.95));
%p = nanmean(bsxfun(@gt,splv,abs(nanmean(pl_,1))));
p = nanmean(splv > abs(nanmean(pl(:))));

[mu,rho] = normfit(splv(:));
plnorm = (pl - mu)/rho;

end

