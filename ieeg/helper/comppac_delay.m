function [ pkdat ] = comppac_delay( etrial_thetaph, eegwav, chidx, btrial)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

[nt,nch,ntr] = size(etrial_thetaph);
nf = size(eegwav,2);
nchwav = size(eegwav,4);
win = 250;
%pkdat.allamp =  zeros(2000,2*win+1,nf);
pkdat.amp =  zeros(2000,nf,nchwav);
pkdat.amp_z1th =  zeros(2000,nf,nchwav);
pkdat.amp_z2th =  zeros(2000,nf,nchwav);
pkdat.amp_z1pk =  zeros(2000,nf,nchwav);
pkdat.amp_z2pk =  zeros(2000,nf,nchwav);
pkdat.theta =  zeros(2000,2*win+1);

pkdat.sp =  zeros(2000,2*win+1);
pkdat.cursor =  zeros(2000,2*win+1);
pkdat.target =  zeros(2000,2*win+1);
pkdat.vel =  zeros(2000,2*win+1);
pkdat.acc =  zeros(2000,2*win+1);
pkdat.err =  zeros(2000,2*win+1);
pkdat.trial =  zeros(2000,1);

ipeak = 0;

for t=1:ntr
    %tlock = btrial(:,2,t);
    tlock = -etrial_thetaph(:,chidx,t);
    
    ang = angle(hilbert(tlock));
    [~,ipks] = findpeaks(ang,'MinPeakDistance',500/10,'MinPeakHeight',pi-0.1);
    %[~,ipks] = findpeaks(-tlock,'MinPeakDistance',500/10,'MinPeakHeight',0);

    
    for p=1:length(ipks)
        idx = ipks(p)+(-win:win);
        if (ipks(p) <= win) || (ipks(p) >= 500*10-win)
            continue;
        end
        if any(any(isnan(etrial_thetaph(ipks(p)+(-win:win),chidx,t))))
            continue;
        end
        srchspace = ipks(p)+(-win:-25);
        [th,ith] = findpeaks(-etrial_thetaph(srchspace,chidx,t));
        [pk,ipk] = findpeaks(etrial_thetaph(srchspace,chidx,t));
        if (length(th) < 2) || (length(pk) < 2)
            continue;
        end
        
        
        
        ipeak = ipeak+1;
        
        pkdat.cursor(ipeak,:) = btrial(idx,3,t);
        pkdat.target(ipeak,:) = btrial(idx,6,t);
        pkdat.vel(ipeak,:) = btrial(idx,4,t);
        pkdat.err(ipeak,:) = real(btrial(idx,8,t));
        pkdat.acc(ipeak,:) = btrial(idx,2,t);
        pkdat.sp(ipeak,:) = btrial(idx,1,t);
        pkdat.trial(ipeak) = t;
        %pkdat.allamp(ipeak,:,:) = abs(eegwav(idx,:,t,:).^2);
        pkdat.amp(ipeak,:,:) = squeeze(mean(abs(eegwav(ipks(p)+(-10:10),:,t,:).^2),1));
        pkdat.amp_z1th(ipeak,:,:) = squeeze(mean(abs(eegwav(srchspace(ith(end))+(-10:10),:,t,:).^2),1));
        pkdat.amp_z2th(ipeak,:,:) = squeeze(mean(abs(eegwav(srchspace(ith(end-1)),:,t,:).^2),1));
        pkdat.amp_z1pk(ipeak,:,:) = squeeze(mean(abs(eegwav(srchspace(ipk(end)),:,t,:).^2),1));
        pkdat.amp_z2pk(ipeak,:,:) = squeeze(mean(abs(eegwav(srchspace(ipk(end-1)),:,t,:).^2),1));
        pkdat.theta(ipeak,:) = etrial_thetaph(idx,chidx,t);
        
    end
end

bandlist = {'trial','amp','amp_z1th','amp_z2th','amp_z1pk','amp_z2pk','theta','sp','vel','cursor','target','acc','err'};

for band = bandlist
pkdat.(cell2mat(band))(ipeak+1:end,:,:) = [];
end


% mtheta = mean(pkdat.theta,1);
% srchspace = 1:win-25;
% [th,ith] = findpeaks(-mtheta(srchspace));
% [pk,ipk] = findpeaks(mtheta(srchspace));
% 
% pkdat.amp_z1th_avg = squeeze(mean(pkdat.allamp(:,ith(end)+(-10:10),:),2));
% pkdat.amp_z2th_avg = squeeze(mean(pkdat.allamp(:,ith(end-1)+(-10:10),:),2));
% pkdat.amp_z1pk_avg = squeeze(mean(pkdat.allamp(:,ipk(end)+(-10:10),:),2));
% pkdat.amp_z2pk_avg = squeeze(mean(pkdat.allamp(:,ipk(end-1)+(-10:10),:),2));
end
