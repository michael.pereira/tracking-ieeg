function plot_on_mesh( atlas,rois,elec,selelec,selval,colors,clim,elecsize,pial,alpha)
%PLOT_ON_MESH Plots electrode values on a mesh representing an anatomical
%region
%   INPUT ARGUMENTS
%   mandatory:
%       - atlas: a freesurfer atlas
%       - rois: a cell of strings corresponding to anatomical regions of
%       the 'atlas'
%       - elec: an elec structure (fieldtrip)
%   optional:
%       - selelec: a cell of strings corresponding to the names of
%       electrodes to select. Default: all electrodes
%       - selval: a vector of values of the size of selelec. Defaut: all to
%       zero
%       - colors: a cell array of the size of 'rois' to plot different
%       anatomical regions with colors. Can be 'r','g',... or 1x3 vector of
%       RGB values. Default: all black.
%       - clim: the maximum and minimum values. Default: minmax of selval
%
%   EXAMPLE:
%   colors = {'r','b'};
%   rois = {'ctx-rh-caudalanteriorcingulate','ctx-rh-superiorfrontal'};%
%   atlas = ft_read_atlas([cfg.datadir 'IR51' '/freesurfer/mri/aparc+aseg.mgz']);
%   atlas.coordsys = 'acpc';
%   sel = {'RAC1','RAC2','RAC3','RAC4','RAC5','SMA1','SMA2','SMA3','SMA4','SMA5'};
%   val = [1:10]+0.123;
%   elec = load([cfg.datadir 'IR51/Recon_Feb_2017/FT_Pipeline/Electrodes/IR51_elec_tal_f.mat']);
%   plot_on_mesh(atlas,rois,elec.elec_tal_f,sel,val,colors,[],2);
%   view([120 40]);
%   lighting gouraud;
%   camlight;
%   


if (nargin < 4) || isempty(selelec)
    selelec = elec.label;
end
if (nargin < 5) || isempty(selval)
    selval = zeros(size(selelec));
end
if (nargin < 6) || isempty(colors)
    colors = cell(size(rois));
    for i=1:length(colors)
        colors{i} = 'k';
    end
end
if (nargin < 7) || isempty(clim)
    clim = minmax(selval);
end
if (nargin < 8) || isempty(elecsize)
    elecsize = 1;
end
if (nargin < 9)
    pial = [];
end
if (nargin < 10) || isempty(alpha)
    alpha = 0.5;
end

mask = cell(1,length(rois));

if ~iscell(rois)
    error('ROIS should be a cell with names of anatomical regions from the ATLAS');
end

if length(colors)~=length(rois)
    error('Length of COLORS should be equal to the number of ROIS');
end

if length(selelec) ~= length(selval)
    error('Length of SELELEC should be equal to the length of SELVAL');
end


% colmap = [ ...
% 0.8*ones(100,1) ...    
% 0.8*linspace(1,0,100).' ...
%     0.8*linspace(1,0,100).' ...
%     ];

colmap = hot(100); 
% loop through anatomical regions
for r=1:length(rois)
    
    % create ROI mask
    conf = [];
    conf.inputcoord = 'acpc';
    conf.atlas = atlas;
    conf.roi = {rois{r}};
    mask{r} = ft_volumelookup(conf, atlas);

    % create ROI mesh
    seg = keepfields(atlas, {'dim', 'unit','coordsys','transform'});
    seg.brain = mask{r};
    conf = [];
    conf.method = 'iso2mesh';
    conf.radbound = 2;
    conf.maxsurf = 0;
    conf.tissue = 'brain';
    conf.numvertices = 1000;
    conf.smooth = 3;
    mesh = ft_prepare_mesh(conf, seg);
    
    
    ft_plot_mesh(mesh,'facecolor',colors{r},'facealpha',0.5,'edgealpha',0)
end

if ~isempty(pial)
    ft_plot_mesh(pial,'FaceColor',[1 1 1]*0.7,'FaceAlpha',alpha)

end

% get index of selected electrodes
[~,selidx] = intersect(elec.label,selelec);

elsel.unit = 'mm';
elsel.coordsys = 'spm';

% loop through electrodes
for e=1:length(selidx)
    
    % decide on the color based on the value
    colper = max(min((selval(selidx(e)) - clim(1))/(clim(2) - clim(1)),1),0);
    col = colmap(round(colper*99)+1,:);
    
    % make electrode structure with selected electrodes
    elsel.label = elec.label(selidx(e));
    elsel.elecpos = elec.elecpos(selidx(e),:);
    elsel.chanpos = elec.elecpos(selidx(e),:);
    
    % okit sensor
    ft_plot_sens(elsel,'elecshape','sphere','elecsize',elecsize,'facecolor',col);
end


end

