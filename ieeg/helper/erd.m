function [ data ] = erd( data, baseline, erd)
%ERD Summary of this function goes here
%   Detailed explanation goes here
if nargin == 2
    erd = 1;
end

if erd==1
    data = bsxfun(@minus,data,baseline);
    data = bsxfun(@rdivide,data,baseline);
else
    data = bsxfun(@minus,20*log10(data),20*log10(baseline));
    %data = zscore(data,[],3);
end
end

