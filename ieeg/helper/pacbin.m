function [ str ] = pacbin( str, ph, amp,thrsig, thr, bins, e )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
nbins = length(bins)-1;
str = struct();
for b = 1:nbins
    idx = ph(:) >= bins(b) & ph(:) < bins(b+1);
    str.all(b,e) = mean(amp(idx));
    
    str.all_sem(b,e) = std(amp(idx))./sqrt(sum(idx));
    
    idx_bw = ph(:) >= bins(b) & ph(:) < bins(b+1) & thrsig(:) < thr;
    str.all_bw(b,e) = mean(amp(idx_bw));
    
    str.all_bw_sem(b,e) = std(amp(idx_bw))./sqrt(sum(idx_bw));
    
    idx_fw = ph(:) >= bins(b) & ph(:) < bins(b+1) & thrsig(:) > thr;
    str.all_fw(b,e) = mean(amp(idx_fw));
    
    str.all_fw_sem(b,e) = std(amp(idx_fw))./sqrt(sum(idx_fw));
    
    [~,str.p_diff(b,e),~,tst_] = ttest2(amp(idx_bw),amp(idx_fw));
    str.t_diff(b,e) = tst_.tstat;
end

end

