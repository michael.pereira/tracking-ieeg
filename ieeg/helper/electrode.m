function [ e ] = electrode( id )
%ELECTRODE Summary of this function goes here
%   Detailed explanation goes here
elecs = {  ... % names of sensors (corresponding to Biosemi) /!\ EEG first !
    'Fp1','AF7','AF3','F1','F3','F5','F7','FT7', ... % 1-8
    'FC5','FC3','FC1','C1','C3','C5','T7','TP7', ... % 9-16
    'CP5','CP3','CP1','P1','P3','P5','P7','P9', ... % 17-24
    'PO7','PO3','O1','Iz','Oz','POz','Pz','CPz', ... % 25-32
    'Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4', ... % 33-40
    'F6','F8','FT8','FC6','FC4','FC2','FCz','Cz', ... % 41-48
    'C2','C4','C6','T8','TP8','CP6','CP4','CP2', ... % 49-56
    'P2','P4','P6','P8','P10','PO8','PO4','O2', ... % 56-6
    };
e = find(strcmp(id,elecs));
end

