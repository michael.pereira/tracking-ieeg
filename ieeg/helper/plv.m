function ph = plv( hx,hy, n,m )
%PLV Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    n = 1;
end
if nargin < 3
    m = 1;
end

phx = unwrap(angle(hx));
if nargin > 1
    phy = unwrap(angle(hy));
    ph = exp(1i*(bsxfun(@minus,m*phx,n*phy)));
else
    ph = exp(1i*phx);
end
end

