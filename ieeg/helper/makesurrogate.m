function [ surx ] = makesurrogate( x, block )
%MAKESURROGATE makesurrogate(x,block) constructs a surrogate of a signal by scrambling blocks in
%time
%   Blocks of 'block' samples with 50% overlap are permuted in time, with a
%   different permutation for each electrode. A Hanning window is used to
%   smooth transitions. If there were no permutation, the signal would be
%   perfectly reconstructed except for the 'block/2' first and last samples
%   input:
%   * x: a Nt x Ne matrix with Nt samples and Ne electrodes
%   * block: the size of a block of permutation data in samples
%   output:
%   * surx: a Nt x Ne matrix of permuted data
    

shift = block/2;

% prealocate
surx = x*0;
% for each electrode
for i=1:size(x,2)
    % buffer signal into nbl blocks with 50% overlap
    bsur1 = buffer(x(:,i),block,shift,'nodelay');
    nbl = size(bsur1,2);
    % apply hanning window
    bsur1 = bsxfun(@times,bsur1,hanning(block));
    % scramble
    bsur1 = bsur1(:,randperm(nbl));
    % reconstruct signals 
    sur1_ = x(:,i)*0;
    for b=1:nbl
        sur1_((b-1)*shift+(1:block)) = sur1_((b-1)*shift+(1:block)) + bsur1(:,b);
    end
    surx(:,i) = sur1_;
end

end

