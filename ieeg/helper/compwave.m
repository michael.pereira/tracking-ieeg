function [ varargout ] = compwave(raweeg,psi,Hd_hp,cfg )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n = size(psi,1)/2;
nf = size(psi,2);
[nt,nch] = size(raweeg);
eegwav_mu = zeros(nt,nch);
eegwav_beta = zeros(nt,nch);
eegwav_c3 = zeros(nt,nf);
eegwav_c4 = zeros(nt,nf);
eegwav_fcz = zeros(nt,nf);

for channel = 1:cfg.neeg
    fprintf('[%d]',channel);
    if channel==electrode('C3')
        for f=1:cfg.nfreq
            allconv = conv(raweeg(:,channel),psi(:,f));
            eegwav_c3(:,f) = single(allconv(n+1:end-n+1));
        end
    elseif channel==electrode('C4')
        for f=1:cfg.nfreq
            allconv = conv(raweeg(:,channel),psi(:,f));
            eegwav_c4(:,f) = single(allconv(n+1:end-n+1));
        end
    elseif channel==electrode('FCz')
        for f=1:cfg.nfreq
            allconv = conv(raweeg(:,channel),psi(:,f));
            eegwav_fcz(:,f) = single(allconv(n+1:end-n+1));
        end
    end
    fmu = 8:12;
    tmp = zeros(nt,length(fmu));
    for f=1:length(fmu)
        allconv = conv(raweeg(:,channel),psi(:,fmu(f)));
        tmp(:,f) = allconv(n+1:end-n+1);
    end
    eegwav_mu(:,channel) = mean(abs(tmp).^2,2);
    %eegwav_mu_hp_ = filter(Hd_hp,eegwav_mu(:,channel));
    %eegwav_mu_hp_ = filter(Hd_hp,eegwav_mu_hp_(end:-1:1));
    %eegwav_mu_hp(:,channel) = eegwav_mu_hp_(end:-1:1);      
    fbeta = 15:30;
    tmp = zeros(nt,length(fbeta));
    for f=1:length(fbeta)
        allconv = conv(raweeg(:,channel),psi(:,fbeta(f)));
        tmp(:,f) = allconv(n+1:end-n+1);
    end
    eegwav_beta(:,channel) = mean(abs(tmp).^2,2);
    
    %eegwav_beta_hp_ = filter(Hd_hp,eegwav_beta(:,channel));
    %eegwav_beta_hp_ = filter(Hd_hp,eegwav_beta_hp_(end:-1:1));
    %eegwav_beta_hp(:,channel) = eegwav_beta_hp_(end:-1:1);
    
end
fprintf('\n');
eegwav_c3 = single(eegwav_c3);
eegwav_c4 = single(eegwav_c4);
eegwav_fcz = single(eegwav_fcz);

varargout = {eegwav_mu eegwav_beta eegwav_c3 eegwav_c4 eegwav_fcz};
end

