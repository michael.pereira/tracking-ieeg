function [ id,acc ] = adjusttrials( aint,acc,id,e,trg,margin )
%ADJUSTTRIALS Summary of this function goes here
%   Detailed explanation goes here
id_ = find(id);
[~,srtacci]=sort(aint(id_),'descend');
j=1;
while((acc(e) > acc(trg)+margin) && (sum(id) > 40))
    id(id_(srtacci(j))) = 0;
    acc(e) = mean(aint(id));
    j=j+1;
end

[~,srtacci]=sort(aint(id_),'ascend');
j=1;
while((acc(e) < acc(trg)-margin) && (sum(id) > 40))
    id(id_(srtacci(j))) = 0;
    acc(e) = mean(aint(id));
    j=j+1;
end

end

