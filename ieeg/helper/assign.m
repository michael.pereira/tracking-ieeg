function [ gavg ] = assign( gsubj,gavg,idx, i )
%ASSIGN Summary of this function goes here
%   Detailed explanation goes here
for f=fieldnames(gsubj).'
    %fprintf('%s\n',cell2mat(f));
    
    gavg.(cell2mat(f))(i,:,:) = mean(gsubj.(cell2mat(f))(idx,:,:)); 
    %if strncmp(cell2mat(f),'spectrum',8)
    %   n = size(gsubj.(cell2mat(f))(idx,:,:),1);
    %    gavg.([cell2mat(f) '_semdb'])(s,i,:,:) = std(20*log10(gsubj.(cell2mat(f))(idx,:,:)))./sqrt(n); 
    %end
end
if islogical(idx)
    gavg.nsum(i) = sum(idx);
else
    gavg.nsum(i) = length(idx);
end

end

