function [ tmin,tmax,stat ] = nullperm( data,type,pthr,nrand,nsamp1,testtype)
%NULLPERM Estimates the null distribution
%   Input variables: 
%       * data
%       * type
%       * pthr
%       * nrand
%       * nsamp1
%       * testtype
%   Output variables:
%       * tmin
%       * tmax
%       * tsum
%       * stat

if (nargin < 6) || isnumeric(testtype);
    testtype = 'ttest';
end
[nsamp,n1,n2] = size(data);
if nargin < 5
    nsamp1 = floor(nsamp/2);
end
if (nargin < 4 ) || isempty(nrand)
    nrand = 5e3;
end
if (nargin < 3) || isempty(pthr)
    pthr = 0.05;
end

nsamp2 = nsamp - nsamp1;

if length(pthr) == 2
    bpthr = pthr(2);
else
    bpthr = pthr(1);
end
pthr = pthr(1);
fprintf('BOOTSTRAP: #c1=%d, #c2=%d - n= [%dx%d] - repeat: %d)',nsamp1,nsamp2,n1,n2,nrand);

tsampmax = zeros(1,nrand);
tsampmin = zeros(1,nrand);
tsampsize = zeros(1,nrand);

fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
for r=1:nrand
    if mod(r,nrand/10)==1
        rperc = ceil(r/nrand*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
    
    perm = randperm(nsamp1+nsamp2);
    if strcmp(testtype,'ttest') && (nsamp2 ~= 0)
        % 
        data1 = zeros(nsamp1,size(data(:,:),2));
        data2 = zeros(nsamp1,size(data(:,:),2));
        for s=1:nsamp1
            if rand(1) > 0.5
                data1(s,:) = data(s,:);
                data2(s,:) = data(nsamp1+s,:);
            else
                data1(s,:) = data(nsamp1+s,:);
                data1(s,:) = data(s,:);
            end
        end
        [~,rp,~,rstat] = ttest(data1,data2);
        stat.rstat = rstat.tstat;
    elseif strcmp(testtype,'ttest') && (nsamp2 == 0)
        idx = rand(1,nsamp1);
        data(idx>0.5,:) = -data(idx>0.5,:);
            [~,rp,~,rstat] = ttest(data);
            stat.rstat = rstat.tstat;
    elseif strcmp(testtype,'ttest2')
            [~,rp,~,rstat] = ttest2(data(perm(1:nsamp1),:),data(perm(nsamp1+(1:nsamp2)),:));
            stat.rstat = rstat.tstat;
    elseif strcmp(testtype,'corr'),
            [stat.rstat,rp] = corr(data(perm(1:nsamp1),:).',data(perm(nsamp1+(1:nsamp2)),:).','type','Pearson','rows','complete');  
    end
    if strcmp(type,'1d')
        [rtsum,rtsize] = clustertstat1D( rstat.tstat,rp,pthr );
    elseif strcmp(type,'2d')
        rp = reshape(rp,n1,n2);
        stat.reshape = reshape(rstat.tstat,n1,n2);
        [rtsum,rtsize] = clustertstat2D( rstat.tstat,rp,pthr );
    elseif strcmp(type,'topo')
        [rtsum,rtsize] = clustertstattopo( stat.rstat,rp,pthr,elecs );
    else
        debug(0,'Not implemented');
    end
    if isempty(rtsum)
        tsampmax(r) = max(rstat.tstat(:));
        tsampmin(r) = min(rstat.tstat(:));
      %  tsampsize(r) = 0;
    else
        tsampmax(r) = max(rtsum(:));
        tsampmin(r) = min(rtsum(:));
%        tsampsize(r) = max(rtsize);
    end
end

tmin = quantile(tsampmin,bpthr/2);
tmax = quantile(tsampmax,1-bpthr/2);
%tsize = quantile(tsampsize,1-bpthr);

if nargout == 4
   stat.tsampmin = tsampmin;
   stat.tsampmax = tsampmax;
   stat.tsampsize = tsize;
end
    fprintf('\n');

end

