function signif = plotMVRA( behav, eeg, factor_, plots, nrep, pthr, colr)
%PLOTMVRA Summary of this function goes here
%   Detailed explanation goes here
upsamp = 1; 
x = linspace(-0.5,0.5,length(factor_));
x2 = linspace(-0.5,0.5,length(behav));

factor = squeeze(mean(factor_,1));
nsubj = size(factor_,1);
[~,p_,~,stat_] = ttest(factor_,[],pthr,'both',1);
[ tsum,clusters ] = clustertstat2D( squeeze(stat_.tstat),squeeze(p_),pthr );
tperm_min = nan(1,nrep);
tperm_max = nan(1,nrep);
fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
    
for rep = 1:nrep
    if mod(rep,nrep/10)==1
        rperc = ceil(rep/nrep*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
    sign = round(rand(1,nsubj))*2-1;
    newfactor = bsxfun(@times,factor_,sign.');
    [~,p_,~,stat_] = ttest(newfactor,[],pthr,'both',1);
    tpermsum = clustertstat2D( squeeze(stat_.tstat),squeeze(p_),pthr );
    if ~isempty(tpermsum)
        tperm_min(rep) = min(tpermsum);
        tperm_max(rep) = max(tpermsum);
    end
end
fprintf('\n');
signif = factor*0;
for cl=1:length(clusters)
   if (tsum(cl) > quantile(tperm_max,1-pthr)) || (tsum(cl) < quantile(tperm_min,pthr))
       signif(clusters{cl}.idx) = 1;
   end
end
ax(3) = subplot('Position',[0.15 0.25 0.1 0.65]);
plot(behav,x2,'LineWidth',2,'Color',colr(1,:));hold on

ax(2) = subplot('Position',[0.25 0.15 0.65 0.1]);
plot(x2,eeg,'Color',colr(2,:),'LineWidth',2)
hold on;
plot(xlim(),[0 0],'k');
ax(1) = subplot('Position',[0.25 0.25 0.65 0.65]);
imagesc(x,x,imresize(factor,upsamp)); axis xy; hold on;
if ~isempty(signif)
    contour(x,x,signif,'-','Color','k','LineWidth',1);
end
colormap(plots.color.colormap);
plot(x,x,'k','LineWidth',2);
plot([0 0],ylim(),'k--','LineWidth',1);
plot(xlim(),[0 0],'k--','LineWidth',1);
tickpositions = -0.4:0.2:0.4;
set(ax(1),'XTick',tickpositions); set(ax(1),'XTickLabel',[])
set(ax(1),'YTick',tickpositions); set(ax(1),'YTickLabel',[])
set(ax(1),'XGrid','on','XColor',[0 0 0],'YGrid','on','YColor',[0 0 0])
set(ax(2),'XTick',tickpositions); set(ax(2),'XTickLabel',tickpositions,'FontSize',16)
%set(ax(2),'YTick', get(ax(2),'YLim').*0.5,'YTickLabel',{'-','+'},'FontSize',14); 
set(ax(2),'YTick', [] ,'FontSize',16); 
set(ax(2),'XGrid','on'); 
set(ax(3),'YTick',tickpositions); set(ax(3),'YTickLabel',tickpositions,'FontSize',16)
xl = get(ax(3),'XLim');
set(ax(3),'XTick', xl+diff(xl)*0.3*[1,-1],'XTickLabel',{'-','+'},'FontSize',16); 
set(ax(3),'YGrid','on'); 
hlink1 = linkprop(ax(1:2),'XLim');
hlink2 = linkprop(ax([1 3]),'YLim');
hlinks = [hlink1 hlink2];
set(gcf,'userdata',hlinks);

end

