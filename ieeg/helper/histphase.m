function [ phdata,phlist ] = histphase( ph,amp, n, eogmask, circ )
%HISTPHASE Summary of this function goes here
%   Detailed explanation goes here
if nargin < 5
    circ = 0;
end
if nargin < 4
   eogmask = false(size(phidx));
end
if nargin < 3
    n = 12;
end
phlist = linspace(-pi,pi,n);

phdata = nan(length(phlist),size(amp,2));

dph = mode(diff(phlist));
phidx = ((ph > pi-dph/2) | (ph < -pi+dph/2)) ;


if circ
    phdata(1,:) = circ_mean(amp(phidx & ~eogmask,:));
else
    phdata(1,:) = mean(amp(phidx & ~eogmask,:));
end

for i = 2:length(phlist)-1
    phidx = (ph > phlist(i)-dph/2) & (ph < phlist(i)+dph/2);
    if circ
        phdata(i,:) = circ_mean(amp(phidx & ~eogmask,:));
    else
        phdata(i,:) = mean(amp(phidx & ~eogmask,:));
    end
end

phdata(i+1,:) = phdata(1,:);

end

