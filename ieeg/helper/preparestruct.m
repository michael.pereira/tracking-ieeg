function gavg = preparestruct(fields,nfields,len,nchan)
gavg = struct();


f_ = 0;
for f=fields
    f_ = f_+1;
    
    %gavg{c}.lag = struct;
    gavg.(cell2mat(f)).iie = nan(nfields(f_),len);
    gavg.(cell2mat(f)).sp = nan(nfields(f_),len);
    gavg.(cell2mat(f)).acc = nan(nfields(f_),len);
    gavg.(cell2mat(f)).tacc = nan(nfields(f_),len);
    gavg.(cell2mat(f)).traj = nan(nfields(f_),len);
    gavg.(cell2mat(f)).low1 = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).low2 = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).low2ph = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).theta = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).thetaph = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).mu = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).beta = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).lgamma = nan(nfields(f_),len,nchan);
    gavg.(cell2mat(f)).hgamma = nan(nfields(f_),len,nchan);
    
end

