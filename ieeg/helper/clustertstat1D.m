function [ tsum,tsize,clusters ] = clustertstat1D( tstat,p,pthr )
%M_CLUSTERTSTAT Summary of this function goes here
%   Detailed explanation goes here

% 
np = length(p);
diff = (p(1:end-1)>pthr) - (p(2:end)>pthr);
clustOn = find(diff==1);
clustOff = find(diff==-1);
if p(1) < pthr
    clustOn = [1 clustOn];
end
if p(end) < pthr
    clustOff = [clustOff np]; 
end

nclust = min(length(clustOn),length(clustOff));
tsum = zeros(nclust,1);
tsize = zeros(nclust,1);
clusters = cell(nclust,1);
for c=1:nclust
   clusters{c}.idx = clustOn(c)+1:clustOff(c);
   tsum(c) = sum(tstat(clusters{c}.idx));
   clusters{c}.tsum = tsum(c);
   tsize(c)= length(clusters{c}.idx);
   
end

end

