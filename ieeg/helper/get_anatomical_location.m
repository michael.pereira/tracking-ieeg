function [ label , elname] = get_anatomical_location( elec,atlas,selelec )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
if nargin < 3
    selelec = elec.label;
end

[~,selidx] = intersect(elec.label,selelec);

t = pinv(atlas.hdr.vox2ras);
label = cell(1,length(selidx));
elname = cell(1,length(selidx));

for e=1:length(selidx)
    pos = round([elec.elecpos(selidx(e),:) 1]*t.');
    aparcid = atlas.aparc(pos(1),pos(2),pos(3));
    if aparcid == 0
        label{e} = 'UNKNOWN';
    else
       label{e} = atlas.aparclabel{aparcid};
    end
    elname{e} = elec.label{selidx(e)};
end

end

