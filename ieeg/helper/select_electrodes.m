function [ data2,elecs2,region ] = select_electrodes( trials,cfg, anat,bip)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 4
    bip = 1;
end

for f=fieldnames(trials{1}).'
    if ndims(trials{1}.(cell2mat(f))) == 3
        sz = size(trials{1}.(cell2mat(f)));
        fprintf('Using *%s* as data field with %d samples and %d trials\n',cell2mat(f),sz(1),sz(3));
        datafield = cell2mat(f);
        
    end
end

if ~iscell(anat)
    anat = {anat};
end

nsel = 0;
for na = 1:length(anat)
    if isfield(cfg,anat{na});
        nsel = nsel + length(cfg.(anat{na}).elecs);
    end
end

if ~isfield(cfg,'elec_loc_file') || ~exist(cfg.elec_loc_file,'file')
    fprintf('WARNING: could not find electrode location file (SKIPPING)\n');
    elecs = [];
    elecs2 = [];
else
elecs = load(cfg.elec_loc_file);
elecs2.unit = elecs.elec_mni_v.unit;
elecs2.coordsys = elecs.elec_mni_v.coordsys;
elecs2.cfg = elecs.elec_mni_v.cfg;
elecs2.labels = cell(nsel,1);
elecs2.elecpos = zeros(nsel,3);
end
data2 = zeros(sz(1),sz(3),nsel);
region = nan(1,nsel);
ngrids = length(trials);
el = 0;
for na = 1:length(anat)
    if ~isfield(cfg,anat{na});
        continue;
    end
    for g=1:ngrids
        nelec = trials{g}.elecs.nsel;
        %%
        for e=1:nelec
            % check if the electrode is to be included
            elecname = trials{g}.elecs.selchans{e};
            idx = strcmp(elecname,cfg.(anat{na}).elecs);
            if all(idx==0)
                continue;
            end
            el = el+1;
            
            % if yes,
            region(el) = na;
            data2(:,:,el) = squeeze(trials{g}.(datafield)(:,e,:));
            elecs2.labels{el} = elecname;
            if ~isempty(elecs)
                idx2 = find(strcmp(elecname,elecs.elec_mni_v.label));
                if bip
                    fprintf('Found %s - %s\n',elecs.elec_mni_v.label{idx2},elecs.elec_mni_v.label{idx2-1});
                    elecs2.elecpos(el,:) = 0.5*(elecs.elec_mni_v.elecpos(idx2,:) + elecs.elec_mni_v.elecpos(idx2-1,:));
                else
                    fprintf('Found %s\n',elecs.elec_mni_v.label{idx2});
                    elecs2.elecpos(el,:) = elecs.elec_mni_v.elecpos(idx2,:);
                end
            end
        end
    end
end
if el ~= nsel
    
    fprintf('!!! Could not find all electrodes\n');
end

