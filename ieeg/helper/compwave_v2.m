function [ varargout ] = compwave_v2(raweeg,psi,cfg )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n = size(psi,1)/2;
nf = size(psi,2);
fmu = 11;
fbeta = 22;
femg = 60;
[nt,nch] = size(raweeg);
eegwav_mu = zeros(nt,nch);
eegwav_beta = zeros(nt,nch);
eegwav_emg = zeros(nt,nch);

eegwav_c3 = zeros(nt,nf);
eegwav_c4 = zeros(nt,nf);
%eegwav_fcz = zeros(nt,nf);
fpsi = fft(psi,nt+2*n-1,1);
for channel = 1:size(raweeg,2)
    fprintf('[%d]',channel);
    if channel==electrode('C3')
        for f=1:cfg.nfreq
            %allconv = conv(raweeg(:,channel),psi(:,f));
            allconv = ifft(fft(raweeg(:,channel),nt+2*n-1).*fpsi(:,f));
            eegwav_c3(:,f) = single(allconv(n+1:end-n+1));
        end
    elseif channel==electrode('C4')
        for f=1:cfg.nfreq+1
            allconv = ifft(fft(raweeg(:,channel),nt+2*n-1).*fpsi(:,f));
            eegwav_c4(:,f) = single(allconv(n+1:end-n+1));
        end
    end

    allconv = ifft(fft(raweeg(:,channel),nt+2*n-1).*fpsi(:,fmu));
    %allconv = conv(raweeg(:,channel),psi(:,fmu));
    eegwav_mu(:,channel) = single(allconv(n+1:end-n+1));
    
    allconv = ifft(fft(raweeg(:,channel),nt+2*n-1).*fpsi(:,fbeta));
    eegwav_beta(:,channel) = single(allconv(n+1:end-n+1));
    
    allconv = ifft(fft(raweeg(:,channel),nt+2*n-1).*fpsi(:,end));
    eegwav_emg(:,channel) = single(allconv(n+1:end-n+1));
end
fprintf('\n');


varargout = {eegwav_mu eegwav_beta eegwav_emg eegwav_c3 eegwav_c4};
end

