function [ tsum,clusters ] = clustertstat2D( tstat,p,pthr )
%M_CLUSTERTSTAT Summary of this function goes here
%   Detailed explanation goes here

% ROI clustering (p<0.05)
permclust = bwconncomp(p < pthr);

clusters = cell(permclust.NumObjects,1);
tsum = zeros(1,permclust.NumObjects);

for c = 1:permclust.NumObjects
    % for every cluster, sum up t-test statistic for every pixel
    tsum(c) = sum(tstat(permclust.PixelIdxList{c}));
    clusters{c}.idx = permclust.PixelIdxList{c};
    clusters{c}.tsum = tsum(c);
end
end

