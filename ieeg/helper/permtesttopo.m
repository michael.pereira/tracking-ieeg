function [ signif,signif2 ] = permtesttopo( factor_,nrep,pthr,pclust,connect)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 6
   connect = 4; 
end
if ~ismember(connect,[4,8])
    error('Connect should be 4 or 8, not %d',connect);
end
if nargin < 5
    
end
if nargin < 4
    pclust = 0.05;    
end
if nargin < 3
    pthr = 0.05;
end
if nargin < 2
    nrep = 1000;
end
fprintf('Repeating %d permutations, p(cluster)=%.3f, p(adjusted)=%.3f, clustering connectivity=%d\n',nrep,pclust,pthr,connect);
if iscell(factor_)
    factor = squeeze(mean(factor_{1}-factor_{2},1));
    [~,p,~,stat_] = ttest(factor_{1},factor_{2});
    [nsubj,nvar] = size(factor_{1}); 
else
    factor = squeeze(mean(factor_,1));
    [~,p,~,stat_] = ttest(factor_);
    [nsubj,nvar] = size(factor_);
end

elecs = {  ... % names of sensors (corresponding to Biosemi) /!\ EEG first !
    'Fp1','AF7','AF3','F1','F3','F5','F7','FT7', ... % 1-8
    'FC5','FC3','FC1','C1','C3','C5','T7','TP7', ... % 9-16
    'CP5','CP3','CP1','P1','P3','P5','P7','P9', ... % 17-24
    'PO7','PO3','O1','Iz','Oz','POz','Pz','CPz', ... % 25-32
    'Fpz','Fp2','AF8','AF4','AFz','Fz','F2','F4', ... % 33-40
    'F6','F8','FT8','FC6','FC4','FC2','FCz','Cz', ... % 41-48
    'C2','C4','C6','T8','TP8','CP6','CP4','CP2', ... % 49-56
    'P2','P4','P6','P8','P10','PO8','PO4','O2', ... % 56-6
    };

[ tsum,~,clusters ] = clustertstattopo( stat_.tstat,p,pclust,[],connect );
tperm_min = zeros(1,nrep);
tperm_max = zeros(1,nrep);
fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
    
for rep = 1:nrep
    if mod(rep,nrep/10)==1
        rperc = ceil(rep/nrep*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
    if iscell(factor_)
          rnd = rand(1,nsubj) > 0.5;
        var1p = bsxfun(@times,factor_{1},rnd.') + bsxfun(@times,factor_{2},~rnd.');
        var2p = bsxfun(@times,factor_{1},~rnd.') + bsxfun(@times,factor_{2},rnd.');
        % paired ttest
        [~,p_,~,stat_] = ttest(var1p,var2p);
        
        %perm = randperm(2*nsubj);
        %alldata = [factor_{1} ; factor_{2}];
        %lab = [zeros(1,nsubj) ones(1,nsubj)];
        %lab = lab(perm);
        %[~,p_,~,stat_] = ttest(alldata(lab==0,:),alldata(lab==1,:));
    else
        sign = round(rand(1,nsubj))*2-1;
        newfactor = bsxfun(@times,factor_,sign.');
        [~,p_,~,stat_] = ttest(newfactor,[]);
    end
    tpermsum = clustertstattopo( squeeze(stat_.tstat),squeeze(p_),pclust,[],connect );
    
    tperm_min(rep) = 0;%min(stat_.tstat);
    tperm_max(rep) = 0;%max(stat_.tstat);
    tmin = min(tpermsum(tpermsum < 0));
    tmax = max(tpermsum(tpermsum > 0));
    if ~isempty(tmin)
        tperm_min(rep) = tmin;
    end
    if ~isempty(tmax)
        tperm_max(rep) = tmax;
    end

end
fprintf('\n');
signif = false(1,nvar);
signif2 = false(1,nvar);

[tsum,reorder] = sort(abs(tsum),'descend');
for cl=1:length(clusters)
    signif2(clusters{reorder(cl)}.idx) = 1;
   if (cl == 1) && ((tsum(cl) > quantile(tperm_max,1-pthr)) || (tsum(cl) < quantile(tperm_min,pthr)))
       signif(clusters{reorder(cl)}.idx) = 1;
       fprintf('=== KEEP Cluster %d: tsum=%.1f [%.1f,%.1f],\n',cl,tsum(cl),quantile(tperm_max,1-pthr),quantile(tperm_min,pthr));
       fprintf('%s-',elecs{clusters{reorder(cl)}.idx});
       fprintf('\n');
   else
       
       fprintf('--- DEL Cluster %d: tsum=%.1f [%.1f,%.1f],\n',cl,tsum(cl),quantile(tperm_max,1-pthr),quantile(tperm_min,pthr));
       fprintf('%s-',elecs{clusters{reorder(cl)}.idx});
       fprintf('\n');

   end
end

end

