function [ trials ] = gettrials( sig, markers, cond, fs )
%GETTRIALS Summary of this function goes here
%   Detailed explanation goes here
tlength = 20;
iendmark = find(ismember(markers.value,20:29));
bad = markers.value(iendmark+1) ~= 30;
iendmark(bad) = [];

ntrial = length(iendmark);
nsens = size(sig,2);

trials_ = nan(tlength*fs,nsens,ntrial);
for t = 1:ntrial
    trials_(:,:,t) = sig(markers.position(iendmark(t)) + (1:fs*tlength),:);
end

trialtype = markers.value(iendmark);

trials = cell(1,length(cond));
for c=1:length(cond)
trials{c} = trials_(:,:,ismember(trialtype,cond{c}));
end
%trials{3} = trials_(:,:,trialtype==26);




