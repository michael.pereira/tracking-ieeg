function [ tmin,tmax,tabs,stat ] = bootstrap( data,type,pthr,nrand,nsamp1,elecs,testtype)
%M_BOOTSTRAP1D Summary of this function goes here
%   Detailed explanation goes here
if (nargin < 3) || isempty(pthr)
    pthr = 0.05;
end
if (nargin < 4 ) || isempty(nrand)
    nrand = 5e3;
end
if (nargin < 6) || isempty(elecs)
    elecs = 1:size(data(:,:),2);
end
if (nargin < 7) || isnumeric(testtype);
    testtype = 'ttest';
end

[nsamp,n1,n2] = size(data);
if nargin < 5
nsamp1 = floor(nsamp/2);
end
nsamp2 = nsamp - nsamp1;


if length(pthr) == 2
    bpthr = pthr(2);
else
    bpthr = pthr(1);
end
pthr = pthr(1);
fprintf('BOOTSTRAP: #c1=%d, #c2=%d - n= [%dx%d] - repeat: %d)',nsamp1,nsamp2,n1,n2,nrand);

tsampmax = zeros(1,nrand);
tsampmin = zeros(1,nrand);
tsampabs = zeros(1,nrand);

fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
for r=1:nrand
    if mod(r,nrand/10)==1
        rperc = ceil(r/nrand*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
    
    perm = randperm(nsamp1+nsamp2);
    if strcmp(testtype,'ttest') && (nsamp2 ~= 0)
        [~,rp,~,rstat] = ttest(data(perm(1:nsamp1),:),data(perm(nsamp1+(1:nsamp2)),:));
        stat = rstat.tstat;
    elseif strcmp(testtype,'ttest') && (nsamp2 == 0)
            [~,rp,~,rstat] = ttest(data(perm(1:nsamp1),:));
            stat = rstat.tstat;
    elseif strcmp(testtype,'ttest2')
            [~,rp,~,rstat] = ttest2(data(perm(1:nsamp1),:),data(perm(nsamp1+(1:nsamp2)),:));
            stat = rstat.tstat;
    elseif strcmp(testtype,'corr'),
            [stat,rp] = corr(data(perm(1:nsamp1),:).',data(perm(nsamp1+(1:nsamp2)),:).','type','Pearson','rows','complete');  
    end
    if strcmp(type,'1d')
        rtsum = clustertstat1D( rstat.tstat,rp,pthr );
    elseif strcmp(type,'2d')
        rp = reshape(rp,n1,n2);
        stat = reshape(stat,n1,n2);
        [rtsum,cl] = clustertstat2D( stat,rp,pthr );
    elseif strcmp(type,'topo')
        rtsum = clustertstattopo( stat,rp,pthr,elecs );
    else
        debug(0,'Not implemented');
    end
    if isempty(rtsum)
       % rtsum = stat;
    else
    tsampmax(r) = max(rtsum(:));
    tsampmin(r) = min(rtsum(:));
    tsampabs(r) = max(abs(rtsum(:)));
    end
end

tmin = quantile(tsampmin,bpthr/2);
tmax = quantile(tsampmax,1-bpthr/2);
tabs = quantile(tsampabs,1-bpthr/2);

if nargout == 4
   stat.tsampmin = tsampmin;
   stat.tsampmax = tsampmax;
   stat.tsampabs = tsampabs;
end
    fprintf('\n');

end

