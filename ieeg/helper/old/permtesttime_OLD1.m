function [ signif,fpos ] = permtesttime( variables,nrep,pthr,pclust,testtype,tail)
%permtesttime Outputs significance maps, corrected for multiple comparisons
%using Maris and Oostenveld (2007), Nonparametric statistical testing of EEG- and MEG-data, Journal of Neuroscience Methods 
%   The function estimates a null distribution by permuting the data (for
%   two variables) or permuting the sign (for one variable) and compares
%   the clusters in the data to to the null distribution
%   Input arguments
%       * variables: input variable(s) to test. If a 2 item cell array is supplied, a
%       cluster-based permutation test will be done by shuffling the
%       classes from both cells. If a matrix is supplied, the function will do a 
%       sign-permutation cluster test.
%       * nrep: the number of repetitions to estimate the null distribution
%       (default: 1000)
%       * pthr: the threshold of significance (default: 0.05)
%       * pclust: the threshold for defining the cluster (default: 0.05,
%       but must not necessarily be < 0.05 ... if you're good at convincing reviewers)
%       * testtype: can be 'ttest' or 'ttest2' (for single-subject
%       analysis)
%       * tail: default is 0, can be set to -1 or 1 for one-tail tests (if
%       you have a strong a-priory reason of doing that). Set to -1 to test
%       var1 < 0 or var1 < var2 and to 1 to test var1 > 0 or var1 > var2
%   Output arguments
%       * signif: a binary map of significance
%       * fpos: a binary map of (pclust) significance without the correction
%      

if nargin < 6
   tail = 0; 
end
if nargin < 5
   testtype = 'ttest'; 
else
   if ~iscell(variables)
      warning('you wanted a %s test, but you are not providing two variables, I will use a ttest',testtype)
   end
end
if ~any(strcmp(testtype,{'ttest','ttest2'}))
    error('Test type should be either ttest or ttest2');
end
if nargin < 4
    pclust = 0.05;    
end
if nargin < 3
    pthr = 0.05;
end
if nargin < 2
    nrep = 1000;
end
fprintf('Repeating %d permutations, p(cluster)=%.3f, p(adjusted)=%.3f\n',nrep,pclust,pthr);

% Get test statistics
if iscell(variables)
    if strcmp(testtype,'ttest')
        [~,p,~,stat_] = ttest(variables{1},variables{2});
    elseif strcmp(testtype,'ttest2')
        [~,p,~,stat_] = ttest2(variables{1},variables{2});
    end
    [nsubj,nvar1,nvar2] = size(variables{1}); 
else
    [~,p,~,stat_] = ttest(variables);
    [nsubj,nvar1,nvar2] = size(variables);
end

% Cluster the data (works also for 1D)
[ tsum,~,clusters ] = clustertstat2D( squeeze(stat_.tstat),squeeze(p),pclust );
if nvar2 == 1
    % if there is only one variable, we cannot shuffle so we permute signs
    tperm_min = zeros(1,nrep);
    tperm_max = zeros(1,nrep);
    fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
    for rep = 1:nrep
        if mod(rep,nrep/10)==1
            rperc = ceil(rep/nrep*10);
            fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
            fprintf('|');
            for j=1:10
                if j<=rperc
                    fprintf('=');
                else
                    fprintf(' ');
                end
            end
            fprintf('|');
        end
        if iscell(variables)
            
            perm = randperm(2*nsubj);
            alldata = [variables{1} ; variables{2}];
            lab = [zeros(1,nsubj) ones(1,nsubj)];
            lab = lab(perm);
            [~,p_,~,stat_] = ttest(alldata(lab==0,:),alldata(lab==1,:));
        else
            sign = round(rand(1,nsubj))*2-1;
            newfactor = bsxfun(@times,variables,sign.');
            [~,p_,~,stat_] = ttest(newfactor,[]);
        end
        tpermsum = clustertstat2D( squeeze(stat_.tstat),squeeze(p_),pclust );
        if ~isempty(tpermsum)
            tperm_min(rep) = min(tpermsum);
            tperm_max(rep) = max(tpermsum);
        else
            tperm_min(rep) = min(stat_.tstat);
            tperm_max(rep) = max(stat_.tstat);
        end
    end
    fprintf('\n');
else
    if iscell(variables)
        permdata = [variables{1} ; variables{2}];
    else
        permdata = variables;
    end
    [ tperm_min,tperm_max ] = nullperm(permdata,'2d', [pthr pthr],nrep,nsubj,[],'ttest');
    
end

if iscell(variables)
    % pre-allocate significance mask
    signif = false(size(variables{1},2),size(variables{1},3));
    % and also false positives
    fpos = false(size(variables{1},2),size(variables{1},3));
else
    % pre-allocate significance mask
    signif = false(size(variables,2),size(variables,3));
    % and also false positives
    fpos = false(size(variables,2),size(variables,3));
end

if tail==0
    % two-tailed
    qmax = 1-pthr/2;
    qmin = pthr/2;  
elseif tail == -1
    % one tailed < 
    qmax = 1;
    qmin = pthr;    
elseif tail == -1
    % one tailed >
    qmax = 1-pthr;
    qmin = 0;      
end

for cl=1:length(clusters)

    if (tsum(cl) > quantile(tperm_max,qmax)) || (tsum(cl) < quantile(tperm_min,qmin))
        % clusters significantly different from the null distribution
        signif(clusters{cl}.idx) = 1;
        fprintf('=== KEEP Cluster %d: tsum=%.1f [%.1f,%.1f],\n',cl,tsum(cl),quantile(tperm_max,1-pthr),quantile(tperm_min,pthr));
        fprintf('\n');
    else
        % false positive clusters
        fpos(clusters{cl}.idx) = 1;
        fprintf('--- DEL Cluster %d: tsum=%.1f [%.1f,%.1f],\n',cl,tsum(cl),quantile(tperm_max,1-pthr),quantile(tperm_min,pthr));
        fprintf('\n');
    end
end

end

