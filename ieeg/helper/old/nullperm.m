function [ tmin,tmax,tsum,stat ] = nullperm( data,type,pthr,nrand,testtype)
%NULLPERM Estimates the null distribution
%   Input variables: 
%       * data
%       * type
%       * pthr
%       * nrand
%       * nsamp1
%       * testtype
%   Output variables:
%       * tmin
%       * tmax
%       * tsum
%       * stat

if (nargin < 5) || isnumeric(testtype);
    testtype = 'ttest';
end
if (nargin < 4 ) || isempty(nrand)
    nrand = 1000;
end
if (nargin < 3) || isempty(pthr)
    pthr = 0.05;
end


fprintf('BOOTSTRAP: #c1=%d, #c2=%d - n= [%dx%d] - repeat: %d)',nsamp1,nsamp2,n1,n2,nrand);

tperm_min = zeros(1,nrand);
tperm_max = zeros(1,nrand);

fprintf('completed: |          |');%fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
for r=1:nrand
    if mod(r,nrand/10)==1
        rperc = ceil(r/nrand*10);
        fprintf('\b\b\b\b\b\b\b\b\b\b\b\b');
        fprintf('|');
        for j=1:10
            if j<=rperc
                fprintf('=');
            else
                fprintf(' ');
            end
        end
        fprintf('|');
    end
     if iscell(variables)
            
            perm = randperm(2*nsubj);
            alldata = [variables{1} ; variables{2}];
            lab = [zeros(1,nsubj) ones(1,nsubj)];
            lab = lab(perm);
            [~,p_,~,stat_] = ttest(alldata(lab==0,:),alldata(lab==1,:));
        else
            sign = round(rand(1,nsubj))*2-1;
            newfactor = bsxfun(@times,variables,sign.');
            [~,p_,~,stat_] = ttest(newfactor,[]);
        end
        tpermsum = clustertstat2D( squeeze(stat_.tstat),squeeze(p_),pclust );
        if ~isempty(tpermsum)
            tperm_min(rep) = min(tpermsum);
            tperm_max(rep) = max(tpermsum);
        else
            tperm_min(rep) = min(stat_.tstat);
            tperm_max(rep) = max(stat_.tstat);
        end
        
     
end

tmin = quantile(tsampmin,bpthr/2);
tmax = quantile(tsampmax,1-bpthr/2);
tsum = quantile(tsampsum,1-bpthr/2);

if nargout == 4
   stat.tsampmin = tsampmin;
   stat.tsampmax = tsampmax;
   stat.tsampsum = tsampsum;
end
    fprintf('\n');

end

