function [ select,mind_ ] = match( d , thr)
%MATCH Summary of this function goes here
%   Detailed explanation goes here
select = []; mind = 0; i = 0;
while i < thr
    i = i+1;
    [mind,pos] = nanmin(d(:));
    if isnan(mind)
        break
    end
    [posx,posy] = ind2sub(size(d),pos);
    d(posx,:) = NaN;
    d(:,posy) = NaN;
    mind_(i) = mind;
    select(i,:) = [posx,posy];
end

fprintf('Matching %d x %d, found %d pairs, thr=%f\n',size(d,1),size(d,2),length(mind_),mind);

end

