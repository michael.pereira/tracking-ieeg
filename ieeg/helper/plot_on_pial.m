function plot_on_pial( pial,elec,selelec,selval,colors,clim,alpha,elecsize)
%PLOT_ON_MESH Plots electrode values on a mesh representing an anatomical
%region
%   INPUT ARGUMENTS
%   mandatory:
%       - pial: a freesurfer pial surface mesh
%       - rois: a cell of strings corresponding to anatomical regions of
%       the 'atlas'
%       - elec: an elec structure (fieldtrip)
%   optional:
%       - selelec: a cell of strings corresponding to the names of
%       electrodes to select. Default: all electrodes
%       - selval: a vector of values of the size of selelec. Defaut: all to
%       zero
%       - colors: a cell array of the size of 'rois' to plot different
%       anatomical regions with colors. Can be 'r','g',... or 1x3 vector of
%       RGB values. Default: all black.
%       - clim: the maximum and minimum values. Default: minmax of selval
%
%   EXAMPLE:


if (nargin < 3) || isempty(selelec)
    selelec = elec.label;
end
if (nargin < 4) || isempty(selval)
    selval = zeros(size(selelec));
end
if (nargin < 5) || isempty(colors)
    colors = cell(size(rois));
    for i=1:length(colors)
        colors{i} = 'k';
    end
end
if (nargin < 6) || isempty(clim)
    clim = minmax(selval);
end
if (nargin < 7) || isempty(alpha)
    alpha = 1;
end
if (nargin < 8) || isempty(elecsize)
    elecsize = 1;
end


if length(selelec) ~= length(selval)
    error('Length of SELELEC should be equal to the length of SELVAL');
end


ft_plot_mesh(pial,'FaceAlpha',alpha)


colmap = jet(100);
% get index of selected electrodes
[~,selidx] = intersect(elec.label,selelec);

elsel.unit = 'mm';
elsel.coordsys = 'spm';

% loop through electrodes
for e=1:length(selidx)
    
    % decide on the color based on the value
    colper = max(min((selval(e) - clim(1))/(clim(2) - clim(1)),1),0);
    col = colmap(round(colper*99)+1,:);
    
    % make electrode structure with selected electrodes
    elsel.label = elec.label(selidx(e));
    elsel.elecpos = elec.elecpos(selidx(e),:);
    elsel.chanpos = elec.elecpos(selidx(e),:);
    
    % okit sensor
    ft_plot_sens(elsel,'elecshape','sphere','elecsize',elecsize,'facecolor',col);
end


end

