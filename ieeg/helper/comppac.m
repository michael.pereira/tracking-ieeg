function [ pkdat ] = comppac( etrial_thetaph, eegwav, chidx, btrial, shuffle)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 5
    shuffle = 0;
end

[nt,nch,ntr] = size(etrial_thetaph);
nf = size(eegwav,2);
win = 250;
pkdat.amp =  zeros(2000,2*win+1,nf);
pkdat.theta =  zeros(2000,2*win+1);
pkdat.acc =  zeros(2000,2*win+1);
pkdat.err =  zeros(2000,2*win+1);
pkdat.nextaccpk =  zeros(2000,1);
pkdat.prevaccpk =  zeros(2000,1);
pkdat.closeaccpk =  zeros(2000,1);
pkdat.trial =  zeros(2000,1);

ipeak = 0;
if shuffle
    iperm = round(nt*0.1 + rand(1)*nt*0.8);
else
    iperm = 0;
end
for t=1:ntr
    %tlock = btrial(:,2,t);
    tlock = etrial_thetaph(:,chidx,t);
    
    ang = angle(hilbert(circshift(tlock,[iperm 0])));
    [~,ipks] = findpeaks(ang,'MinPeakDistance',500/10,'MinPeakHeight',pi-0.1);
    %[~,ipks] = findpeaks(-tlock,'MinPeakDistance',500/10,'MinPeakHeight',0);

    
    for p=1:length(ipks)
        idx = ipks(p)+(-win:win);
        if (ipks(p) <= win) || (ipks(p) >= 500*10-win)
            continue;
        end
        if any(any(isnan(etrial_thetaph(ipks(p)+(-win:win),chidx,t))))
            continue;
        end
        if ~shuffle
           
            nextpk = findpeaks(btrial(idx(win+1:end),2,t),'MinPeakHeight',0);
            if isempty(nextpk)
                continue;
            end
            prevpk = findpeaks(btrial(idx(1:win),2,t),'MinPeakHeight',0);
            if isempty(prevpk)
                continue;
            end
            [closepk,ipk] = findpeaks(btrial(idx,2,t),'MinPeakHeight',0);
            if isempty(closepk)
                continue;
            end
            nextth = findpeaks(-btrial(idx(win+1:end),2,t),'MinPeakHeight',0);
            if isempty(nextth)
                continue;
            end
            prevth = findpeaks(-btrial(idx(1:win),2,t),'MinPeakHeight',0);
            if isempty(prevth)
                continue;
            end
            [closeth,ith] = findpeaks(-btrial(idx,2,t),'MinPeakHeight',0);
            if isempty(closeth)
                continue;
            end
            
            [~,iclosepk] = min(abs(ipk-251));
            [~,icloseth] = min(abs(ith-251));
            
            ipeak = ipeak+1;
            
            
            pkdat.nextaccpk(ipeak) = nextpk(1);
            pkdat.prevaccpk(ipeak) = prevpk(end);
            pkdat.closeaccpk(ipeak) = closepk(iclosepk);
            pkdat.nextaccth(ipeak) = nextth(1);
            pkdat.prevaccth(ipeak) = prevth(end);
            pkdat.closeaccth(ipeak) = closeth(icloseth);
            pkdat.err(ipeak,:) = real(btrial(idx,8,t));
            pkdat.acc(ipeak,:) = btrial(idx,2,t);
            pkdat.sp(ipeak,:) = btrial(idx,1,t);
            pkdat.trial(ipeak) = t;
        else
            ipeak = ipeak+1;
        end

        pkdat.amp(ipeak,:,:) = abs(eegwav(idx,:,t).^2);
        pkdat.theta(ipeak,:) = etrial_thetaph(idx,chidx,t);
     
    end
end
if ~shuffle
    bandlist = {'trial','amp','theta','acc','err','nextaccpk','prevaccpk','closeaccpk','nextaccth','prevaccth','closeaccth'};
else
    bandlist = {'amp','theta'};
end
for band = bandlist
pkdat.(cell2mat(band))(ipeak+1:end,:,:) = [];
end

end

