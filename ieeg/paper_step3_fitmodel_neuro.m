clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = 'beta';

for subj=[1,5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'btrial','dtrial','ltrial');
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    if strcmp(band,'low') || strcmp(band,'bp')
        efile = [cfg.matdir '/' subject '_elphys_' band '.mat'];
    else
        efile = [cfg.matdir '/' subject '_elphys_' band '_hilb.mat'];
    end
    d2 = load(efile,'trials');
    
    [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
    if isempty(elecs.elecpos)
        continue;
    end
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
    
    
    %% GET EEG EPOCHS
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    %plfp = zeros(np,ne,cfg.fs+1);
    pspecpow = zeros(np,ne,cfg.fs+1);
    
    for e=1:ne
        % lfp = data((cfg.fs*3+1):end,trialidx1,e);
        if strcmp(band,'low') || strcmp(band,'bp')
            specpow = real(dataspecpow((cfg.fs*3+1):end,trialidx1,e));
            base = real(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e));
            specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
        else
            specpow = log(abs(dataspecpow((cfg.fs*3+1):end,trialidx1,e)));
            base = log(abs(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e)));
            specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
        end
        
        for p=1:np
            % plfp(p,e,:) = lfp(peaks.idx(:,p));
            pspecpow(p,e,:) = specpow(peaks.idx(:,p),peaks.trial(p));
        end
    end
  
    %% FIT MODEL
    
    midt = cfg.fs/2+1;
    tlist = round(linspace(1,cfg.fs/2+1,51));
    %tlist(end) = [];
    tlist(end+(-2:0)) = [];
    track = find(peaks.type==1);
    trainidx = track(1:floor(length(track)/2));
    testidx = track((floor(length(track)/2)+1):length(track));
    fields = {'aic','bic','coeffs','coeffs_p','coeffs_se','r2'};
    model1 = struct();
    model2 = struct();
    n1 = [1,1,2,2,2,1,1];
    n2 = [1,1,3,3,3,1,1];
    i = 0;
    for f=fields
        i = i+1;
        model1.(cell2mat(f)) = zeros([length(tlist),n1(i)]);
        model2.(cell2mat(f)) = zeros([length(tlist),length(tlist),n2(i)]);
    end
    model1.model = cell(length(tlist),1);
    model2.model = cell(length(tlist),length(tlist));
    
    for t1=1:length(tlist)
        fprintf('=');
        X = real(peaks.dev(tlist(t1),trainidx)).';
        y = peaks.accel(midt,trainidx).';
        bad = y <= 0;
        X(bad,:) = [];
        y(bad,:) = [];
        
        model1.model{t1} = fitlm(X,y);
        model1.aic(t1) = 2*4 - 2*model1.model{t1}.LogLikelihood;
        model1.coeffs(t1,:) = model1.model{t1}.Coefficients.Estimate;
        model1.coeffs_p(t1,:) = model1.model{t1}.Coefficients.pValue;
        model1.coeffs_se(t1,:) = model1.model{t1}.Coefficients.SE;
        model1.r2(t1) = model1.model{t1}.Rsquared.Adjusted;
        Xtest = real(peaks.dev(tlist(t1),testidx).');
        ytest = peaks.accel(midt,testidx).';
        pred = model1.model{t1}.predict(Xtest);
        model1.predr2(t1) = corr(ytest,pred).^2;
        
        for t2 = 1:(t1-1)
            X = [...
                real(peaks.dev(tlist(t1),trainidx)).' ...
                real(peaks.dev(tlist(t2),trainidx)).' ...
                ];
            y = peaks.accel(midt,trainidx).';
            bad = y <= 0;
            X(bad,:) = [];
            y(bad,:) = [];
            
            model2.model{t1,t2} = fitlm(X,y);
            model2.aic(t1,t2) = 2*5 - 2*model2.model{t1,t2}.LogLikelihood;
            model2.coeffs(t1,t2,:) = model2.model{t1,t2}.Coefficients.Estimate;
            model2.coeffs_p(t1,t2,:) = model2.model{t1,t2}.Coefficients.pValue;
            model2.coeffs_se(t1,t2,:) = model2.model{t1,t2}.Coefficients.SE;
            model2.r2(t1,t2) = model2.model{t1,t2}.Rsquared.Adjusted;
            Xtest = real([peaks.dev(tlist(t1),testidx).' peaks.dev(tlist(t2),testidx).']);
            ytest = peaks.accel(midt,testidx).';
            pred = model2.model{t1,t2}.predict(Xtest);
            model2.predr2(t1,t2) = corr(ytest,pred).^2;
            
            %% neuro
            X = [...
                real(peaks.dev(tlist(t1),trainidx)).'  ...
                pspecpow(trainidx,:,t2) 
                ];
            y = peaks.accel(midt,trainidx).';
            bad = y <= 0;
            X(bad,:) = [];
            y(bad,:) = [];
            
            model3.model{t1,t2} = fitlm(X,y);
            model3.aic(t1,t2) = 2*5 - 2*model3.model{t1,t2}.LogLikelihood;
            model3.coeffs(t1,t2,:) = model3.model{t1,t2}.Coefficients.Estimate;
            model3.coeffs_p(t1,t2,:) = model3.model{t1,t2}.Coefficients.pValue;
            model3.coeffs_se(t1,t2,:) = model3.model{t1,t2}.Coefficients.SE;
            model3.r2(t1,t2) = model3.model{t1,t2}.Rsquared.Adjusted;
            Xtest = real([peaks.dev(tlist(t1),testidx).' peaks.dev(tlist(t2),testidx).']);
            ytest = peaks.accel(midt,testidx).';
            pred = model3.model{t1,t2}.predict(Xtest);
            model3.predr2(t1,t2) = corr(ytest,pred).^2;
            
        end
    end
    fprintf('\n');
    %% EVALUATE MODEL
    figure(100+subj); clf; 
    model2.r2(model2.r2(:)==0) = NaN;
    
    
    r2 = max(model1.predr2);
    %relr2 = model2.predr2;
    relr2 = (model2.predr2 - r2)./r2;
    
    [model2_best,i] = nanmax(model2.predr2(:));
    [x1,x2] = ind2sub(size(relr2),i);
    bestidx = [tlist(x1),tlist(x2)];
    
    t = tlist./cfg.fs-0.5;
    imagesc(t,t,model2.predr2.'); hold on;
    plot(t(x1),t(x2),'ro','LineWidth',2);
    colorbar;
    axis xy; 
    caxis([0,0.2])
    %caxis([model2_best,nanmax(model2.aic(:))]);
    colormap('hot');
    c = model2.coeffs(x1,x2,:);
    title(sprintf('%s, max r2=%.2f t1=%.2f (c1=%.2f), t2=%.2f (c1=%.2f)',subjects{subj},model2_best,t(x1),c(2),t(x2),c(3)));
    pause(0.1); 
    
    X = [real(peaks.dev(tlist(x1),:)).' real(peaks.dev(tlist(x2),:)).'];
    y = peaks.accel(midt,:).';
    
    peaks.pred = model2.model{x1,x2}.predict(X).';
    peaks.fberr = real(peaks.dev(x2,:));
    peaks.prederr = real(peaks.dev(x1,:));
    
end