clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};
%anat = {'ofc_ipsi','ofc_contra'};

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = 'theta';

for subj=[5,1,3,4]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'ltrial');
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    
    efile = [cfg.matdir '/' subject '_elphys_' band '_hilb.mat'];
    d2 = load(efile,'trials');
    
    [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
    if isempty(elecs.elecpos) 
        continue;
    end
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
   
    
    %% GET EEG EPOCHS
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    %plfp = zeros(np,ne,cfg.fs+1);
    pspecpow = zeros(np,ne,cfg.fs+1);
    
    for e=1:ne    
       % lfp = data((cfg.fs*3+1):end,trialidx1,e);
        specpow = log(abs(dataspecpow((cfg.fs*3+1):end,trialidx1,e)));
       % base = log(abs(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e)));
        %specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
        for p=1:np
           % plfp(p,e,:) = lfp(peaks.idx(:,p));
            pspecpow(p,e,:) = specpow(peaks.idx(:,p),peaks.trial(p));
        end
    end
    
    %% 
    tlist = 1:10:cfg.fs+1;
    ypred1 = zeros(length(tlist),np);
    ypred2 = zeros(length(tlist),np);
    ypred3 = zeros(length(tlist),np);
    
    ygt1 = peaks.accel(cfg.fs/2+1,:).';
    ygt2 = peaks.pred.';
    ygt3 = peaks.prederr.';
    
    for trial = 1:ntr
        fprintf('=');
        testidx = peaks.trial == tr(trial);
        trainidx = ~(testidx);
        
        y1 = ygt1(trainidx).';
        y2 = ygt2(trainidx).';
        y3 = ygt3(trainidx).';
     
        
        
        for t=1:length(tlist)    
            models = struct();
            %X_lfp = [plfp(:,:,t) y1*0+1];
            X_specpow = pspecpow(trainidx,:,tlist(t));
            X_specpow_test = pspecpow(testidx,:,tlist(t));
           
            models.specpow1 = fitlm(X_specpow,y1);
            ypred1(t,testidx) = models.specpow1.predict(X_specpow_test);
            models.specpow2 = fitlm(X_specpow,y2);
            ypred2(t,testidx) = models.specpow2.predict(X_specpow_test);
            models.specpow3 = fitlm(X_specpow,y3);
            ypred3(t,testidx) = models.specpow3.predict(X_specpow_test);            
        end
    end
            fprintf('\n');
    %%

    
    t = tlist./cfg.fs-0.5;
    figure(subj+200); clf; 
    
    hold on;
    plot(0,NaN,'b','LineWidth',2);
    plot(0,NaN,'g','LineWidth',2);
    plot(0,NaN,'r','LineWidth',2);
    
    [c1,p1] = corr(ypred1',ygt1,'type','Spearman');
    [c2,p2] = corr(ypred2',ygt2,'type','Spearman');
    [c3,p3] = corr(ypred3',ygt3,'type','Spearman');
    
    plot(t,c1.','b','LineWidth',2)
    plot(t,c2.','g','LineWidth',2)
    plot(t,c3.','r','LineWidth',2)
    c1(p1 > 0.05) = NaN;
    c2(p2 > 0.05) = NaN;
    c3(p3 > 0.05) = NaN;
    
    plot(t,c1.','k','LineWidth',4)
    plot(t,c2.','k','LineWidth',4)
    plot(t,c3.','k','LineWidth',4)
    
    title(subject);
    legend({'Acceleration','Prediction','Feedback'});
    pause(0.1);
end