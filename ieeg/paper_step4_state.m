clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};%
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = {'hgamma1','hgamma2','hgamma3','hgamma4','hgamma5','hgamma6','hgamma7','hgamma8'};
band = {'beta'};

norm = 'zscore';

for subj=[1,3:5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'btrial','dtrial','ltrial');
    acc = btrial(:,2,ltrial==1);
    accthr = std(acc(:));
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    specpow = 0;
    for b=1:length(band)
        efile = [cfg.matdir '/' subject '_elphys_' band{b} '_hilb.mat'];
        d2 = load(efile,'trials');
        [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
        if isempty(elecs.elecpos)
            continue;
        end
        specpow_ = dataspecpow((cfg.fs*3+1):end,trialidx1,:);
        base_ = dataspecpow(cfg.fs:cfg.fs*3,trialidx1,:);
        if strcmp(norm,'ers')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),mean(abs(base_).^2,1));
        elseif strcmp(norm,'zscore')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),std(abs(base_).^2 ,1));
        elseif strcmp(norm,'erp')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,real(specpow_),mean(real(base_),1)),mean(real(base_),1));
        elseif strcmp(norm,'raw')
            specpow_ = real(specpow_);
        end
        specpow = specpow + specpow_;
    end
    specpow = specpow./b;
    
    
    
    
    
    
    %% GET EEG EPOCHS
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    %plfp = zeros(np,ne,cfg.fs+1);
    pspecpow = zeros(np,ne,cfg.fs+1);
    
    for e=1:ne
        for p=1:np
            % plfp(p,e,:) = lfp(peaks.idx(:,p));
            pspecpow(p,e,:) = (specpow(peaks.idx(:,p),peaks.trial(p),e));
        end
    end
    %%
    d = 7;
    tsel = 251+(-250:250);
    devthr1 = -10;
    devthr2 = -10;
    
    t = linspace(-0.5,0.5,cfg.fs+1);
    figure(subj+200); clf;
    hold on;
    sel1 = peaks.type==1 & peaks.accel(251,:) > accthr & real(peaks.dev(251,:)) < devthr1;
    sel2 = peaks.type==1 & peaks.accel(251,:) > accthr & real(peaks.dev(251,:)) > devthr2;
    
   % sel1 = peaks.type==1 & peaks.accel(251,:) > accthr;
   % sel2 = peaks.type==3 & peaks.accel(251,:) > accthr;
    
    %sel1 = real(peaks.accel(251,:)) > accthr & sel;
    %sel2 = real(peaks.accel(251,:)) < accthr & sel;
    
    
    
    n1 = sum(sel1);
    n2 = sum(sel2);
    fprintf('c1: %d, c2: %d\n',n1,n2);
    for e=1:length(elecs.labels)
        subplot(3,3,e); hold on;
        c1 = squeeze(mean(pspecpow(sel1,e,tsel),2));
        c2 = squeeze(mean(pspecpow(sel2,e,tsel),2));
        semshade(c1,0.4,'r',t(tsel),'LineWidth',2);
        semshade(c2,0.4,'g',t(tsel),'LineWidth',2);
        [h,p] = ttest2(c1,c2);
        %mean(p<0.05)
%         if any(p<0.05)
        % p = mafdr(p);
        c1m = mean(c1); c1m(p>0.05) = NaN;
        c2m = mean(c2); c2m(p>0.05) = NaN;
        plot(t(tsel),c1m,'k','LineWidth',2);
        plot(t(tsel),c2m,'k','LineWidth',2);
        title(sprintf('subject %s (%s) [%d %d]',subject,elecs.labels{e},n1,n2));
   %     else
        %xlim([-0.5,0.5]);
      %  title([subject ' - ' elecs.labels{e}]);
        %end
    end
    
    if 0
        %%
    figure(subj+201); clf; hold on;
    plot(squeeze(btrial(:,5,dtrial==d & ltrial==1)),'k','LineWidth',2);
    plot(squeeze(btrial(:,3,dtrial==d & ltrial==1)));
    end
    %legend({'Acceleration','Deviation'});
    pause(0.1);
    
end