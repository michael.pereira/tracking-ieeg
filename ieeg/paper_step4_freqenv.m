clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};%
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = {'hgamma1','hgamma2','hgamma3','hgamma4','hgamma5','hgamma6','hgamma7','hgamma8'};
band = {'bp'};

norm = 'raw';

for subj=[1,3:5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'btrial','dtrial','ltrial');
    acc = btrial(:,2,ltrial==1);
    accthr = std(acc(:));
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    specpow = 0;
    for b=1:length(band)
        efile = [cfg.matdir '/' subject '_elphys_' band{b} '_hilb.mat'];
        d2 = load(efile,'trials');
        [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
        if isempty(elecs.elecpos)
            continue;
        end
        specpow_ = dataspecpow((cfg.fs*3+1):end,trialidx1,:);
        base_ = dataspecpow(cfg.fs:cfg.fs*3,trialidx1,:);
        if strcmp(norm,'ers')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),mean(abs(base_).^2,1));
        elseif strcmp(norm,'zscore')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),std(abs(base_).^2 ,1));
        elseif strcmp(norm,'erp')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,real(specpow_),mean(real(base_),1)),mean(real(base_),1));
        elseif strcmp(norm,'raw')
            specpow_ = real(specpow_);
        end
        specpow = specpow + specpow_;
    end
    specpow = specpow./b;
 
    %%
    seltr1 = find(ltrial==1);
    seltr2 = find(ltrial==2);
    seltr3 = find(ltrial==3);
    
    figure(subj*10+200); clf;
    for e=1:length(elecs.labels)
        subplot(3,3,e); hold on;
        spec1 = [];
        for tr=1:length(seltr1)
            [spec1(:,tr),w] = pwelch(specpow(:,seltr1(tr),e),cfg.fs,cfg.fs/2,cfg.fs,cfg.fs);
        end
        spec2 = [];
        for tr=1:length(seltr2)
            spec2(:,tr) = pwelch(specpow(:,seltr2(tr),e),cfg.fs,cfg.fs/2,cfg.fs,cfg.fs);
        end
        spec3 = [];
        for tr=1:length(seltr3)
            spec3(:,tr) = pwelch(specpow(:,seltr3(tr),e),cfg.fs,cfg.fs/2,cfg.fs,cfg.fs);
        end
        stdshade(20*log10(spec1).',0.4,'b',w,'LineWidth',2);
        %stdshade(20*log10(spec2).',0.4,'m',w,'LineWidth',2);
        stdshade(20*log10(spec3).',0.4,'g',w,'LineWidth',2);
        xlim([0,50]); %ylim([-60,0])
        title(sprintf('subject %s (%s)',subject,elecs.labels{e}));
   %     else
        %xlim([-0.5,0.5]);
      %  title([subject ' - ' elecs.labels{e}]);
        %end
    end
    
    %x = specpow(:,seltr1(tr),e);
    
    %%
    figure(subj*10+201); clf;
    t = linspace(-20,20,cfg.fs*40+1);
    for e=1:length(elecs.labels)
        subplot(3,3,e); hold on;       
        s = squeeze(specpow(:,:,e));
        dev1 = real(btrial(:,1,:));
        dev2 = real(btrial(:,7,:));
        [xc1,~,bnd] = crosscorr(dev1(:),s(:),500*20);
        [xc2,~,bnd] = crosscorr(dev2(:),s(:),500*20);
        plot(t,xc1,'r','LineWidth',2)
        plot(t,xc2,'g','LineWidth',2)
        
        ylim([-0.5,0.5]);
        plot(xlim(),[0,0],'k');
        %plot(xlim(),[0,0]+bnd(1),'r--');
        %plot(xlim(),[0,0]+bnd(2),'r--');
        plot([0,0],ylim(),'k--');
        
        title(sprintf('subject %s (%s)',subject,elecs.labels{e}));
    end
    if 0
        %%
    figure(subj+201); clf; hold on;
    plot(squeeze(btrial(:,5,dtrial==d & ltrial==1)),'k','LineWidth',2);
    plot(squeeze(btrial(:,3,dtrial==d & ltrial==1)));
    end
    %legend({'Acceleration','Deviation'});
    pause(0.1);
    
end