clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = 'beta';

for subj=[1,3,4,5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'ltrial');
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    if strcmp(band,'low') || strcmp(band,'bp')
        efile = [cfg.matdir '/' subject '_elphys_' band '.mat'];
    else
        efile = [cfg.matdir '/' subject '_elphys_' band '_hilb.mat'];
    end
    d2 = load(efile,'trials');
    
    [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
    if isempty(elecs.elecpos) 
        continue;
    end
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
   
    
    %% GET EEG EPOCHS
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    %plfp = zeros(np,ne,cfg.fs+1);
    pspecpow = zeros(np,ne,cfg.fs+1);
    
    for e=1:ne    
       % lfp = data((cfg.fs*3+1):end,trialidx1,e);
       if strcmp(band,'low') || strcmp(band,'bp')
           specpow = real(dataspecpow((cfg.fs*3+1):end,trialidx1,e));
              base = real(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e));
            specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
       else
            specpow = log(abs(dataspecpow((cfg.fs*3+1):end,trialidx1,e)));
               base = log(abs(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e)));
        specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
       end
     
        for p=1:np
           % plfp(p,e,:) = lfp(peaks.idx(:,p));
            pspecpow(p,e,:) = specpow(peaks.idx(:,p),peaks.trial(p));
        end
    end
    %%
    t = linspace(-0.5,0.5,cfg.fs+1);
     figure(subj+200); clf; 
    hold on;
    for e=1:length(elecs.labels)
        subplot(3,3,e); hold on;
    c1 = squeeze(mean(pspecpow(peaks.type==1,e,:),2));
    c2 = squeeze(mean(pspecpow(peaks.type==3,e,:),2));
    semshade(c1,0.4,'r',t,'LineWidth',2);
    semshade(c2,0.4,'g',t,'LineWidth',2);
    [h,p] = ttest2(c1,c2);
    c1m = mean(c1); c1m(p>0.05) = NaN;
    c2m = mean(c2); c2m(p>0.05) = NaN;
    plot(t,c1m,'k','LineWidth',2);
    plot(t,c2m,'k','LineWidth',2);
    
    title([subject ' - ' elecs.labels{e}]);
    end
    %legend({'Acceleration','Deviation'});
    pause(0.1);
   
end