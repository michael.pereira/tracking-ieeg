clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra'};%
%anat = {'ofc_ipsi','ofc_contra'};
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = {'hgamma1','hgamma2','hgamma3','hgamma4','hgamma5','hgamma6','hgamma7','hgamma8'};
band = {'bp'};

norm = 'raw';

for subj=[1,3:5]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'btrial','dtrial','ltrial');
    acc = btrial(:,2,ltrial==1);
    accthr = std(acc(:));
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    specpow = 0;
    for b=1:length(band)
        efile = [cfg.matdir '/' subject '_elphys_' band{b} '_hilb.mat'];
        d2 = load(efile,'trials');
        [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
        if isempty(elecs.elecpos)
            continue;
        end
        specpow_ = dataspecpow((cfg.fs*3+1):end,trialidx1,:);
        base_ = dataspecpow(cfg.fs:cfg.fs*3,trialidx1,:);
        if strcmp(norm,'ers')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),mean(abs(base_).^2,1));
        elseif strcmp(norm,'zscore')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,abs(specpow_).^2,mean(abs(base_).^2,1)),std(abs(base_).^2 ,1));
        elseif strcmp(norm,'erp')
            specpow_ = bsxfun(@rdivide,bsxfun(@minus,real(specpow_),mean(real(base_),1)),mean(real(base_),1));
        elseif strcmp(norm,'raw')
            specpow_ = real(specpow_);
        end
        specpow = specpow + specpow_;
    end
    specpow = specpow./b;
 
    %%
    b = 2;
    seltr1 = find(ltrial==1);
    seltr2 = find(ltrial==2);
    seltr3 = find(ltrial==3);
    
    figure(subj*10+200); clf;
    for e=1:length(elecs.labels)
        subplot(3,3,e); hold on;
        spec1 = [];
        for tr=1:length(ltrial)
            [spec1(:,tr),w] = pwelch(specpow(:,tr,e),cfg.fs,cfg.fs/2,cfg.fs,cfg.fs);
        end
        
        %plot(corr(spec1(:,2:end).',squeeze(mean(btrial(:,1,1:end-1)))),'r');
       % plot(corr(spec1.',squeeze(mean(btrial(:,1,:)))),'k');
       % plot(corr(spec1(:,1:end-1).',squeeze(mean(btrial(:,1,2:end)))),'g');
        plot(abs(corr(spec1(:,3:end).',real(squeeze(mean(btrial(:,b,1:end-2)))))),'m');
        plot(abs(corr(spec1(:,2:end).',real(squeeze(mean(btrial(:,b,1:end-1)))))),'r');
        plot(abs(corr(spec1.',real(squeeze(mean(btrial(:,b,:)))))),'k');
        plot(abs(corr(spec1(:,1:end-1).',real(squeeze(mean(btrial(:,b,2:end)))))),'b');
        plot(abs(corr(spec1(:,1:end-2).',real(squeeze(mean(btrial(:,b,3:end)))))),'c');
        
      
        title([subject ' - ' elecs.labels{e}]);
        xlim([0,50]); pause(0.1);
    end
end