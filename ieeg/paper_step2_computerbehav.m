clear

addpath config/
conf
%% PARAMETERS
% list of subjects to process
opts.subjects = {'IR48','IR49','IR51','IR54','IR57','IR61'};
opts.dir = '/Volumes/MichaelData/data/ecog/';

% target sampling rate (after downsampling)
opts.fs = 50;
opts.tlength = 20;
opts.shwin = 3;


pad = zeros(opts.fs,1);

if opts.shwin > 0
    [~,sgol] = sgolay(2,opts.shwin*2+1);
end
%%

d = fdesign.lowpass('N,F3dB',16,10,opts.fs);
Hd = design(d,'butter');

for subj = opts.subjects;
    sptr = 1;
    clear behav
    subject = subj{:};
    
    mfile = [opts.dir '/' subject '/' subject '_*_behav*.txt'];
    sfile = [opts.dir '/' subject '/' subject '_*_score*.txt'];
    mousefiles = dir(mfile);
    scorefiles = dir(sfile);
    
    if isempty(mousefiles)
        error(['File not found for subject ' subject '(' mfile ')']);
    end
    if isempty(scorefiles)
        error(['File not found for subject ' subject '(' sfile ')']);
    end
    if length(scorefiles) ~= length(mousefiles)
        error(['Unequal number of score and mouse files for subject ' subject]);
    end
    disp(['Processing subject ' subject ':']);
    
    trial = 0; peak = 0;  
    %trialidx = zeros(1,100);
    
    % init behav structure
    behav = struct;
    behav.peaks = struct;
    
    for f = 1:length(mousefiles)
        mousefilename = [opts.dir '/' subject '/' mousefiles(f).name];
        scorefilename = [opts.dir '/' subject '/' scorefiles(f).name];
        
        disp([' -- behavfile(' num2str(f) '): ' mousefilename]);
        disp([' -- scorefile(' num2str(f) '):' scorefilename]);
        
        % read mouse position file
        datamous = csvread(mousefilename);
        % read score file
        datascore = csvread(scorefilename);
        
        % find trial separation
        sepmous = find(all(datamous(:,1:end-1)==0,2));
        trig = datamous(sepmous,end);
        trigstart = find(trig>20 & trig < 30);
        
        sepmous_start = sepmous(trigstart);
        
        bad = trig(trigstart+1) ~= 30;
        sepmous_start(bad) = [];
        
        ntrials = length(sepmous_start);
      
        for tr = 1:ntrials
            
            % extract trial
            mous = datamous(sepmous_start(tr)+(1:opts.tlength*opts.fs),:);
            
            trial = trial+1;
            len = (size(mous,1))./opts.fs;
            
            %% TRAJECTORy
            % perceived mouse position in complex numbers
            m = mous(:,5) + 1i*mous(:,6);
            if opts.shwin > 0
                behav.mouse(:,trial) =  sgolayfilt(m,2,2*opts.shwin+1);
            else
                behav.mouse(:,trial) = m;
            end
            
            % target position in complex numbers
            t = mous(:,7) + 1i*mous(:,8);
            if opts.shwin > 0
                behav.target(:,trial) =  sgolayfilt(t,2,2*opts.shwin+1);
            else
                behav.target(:,trial) =  t;
            end
            
            
            %% KINEMATICS
            pad1 = behav.mouse(opts.fs:-1:1,trial);
            pad2 = behav.mouse(end:-1:end-opts.fs+1,trial);
            speed_ = opts.fs*abs(filter(sgol(:,2),1,[pad1 ; m ; pad2]));
            speed_([1:(opts.fs+opts.shwin) end-opts.fs+opts.shwin+1:end]) = [];
            behav.speed(:,trial) = speed_;
            accel_ = -filter(sgol(:,2),1,[pad ; behav.speed(:,trial) ; pad]);
            accel_([1:(opts.fs+opts.shwin) end-opts.fs+opts.shwin+1:end]) = [];
            behav.accel(:,trial) = accel_;
            
            pad1 = behav.target(opts.fs:-1:1,trial);
            pad2 = behav.target(end:-1:end-opts.fs+1,trial);
            targdir_ = -filter(sgol(:,2),1,[pad1 ; behav.target(:,trial) ; pad2]);
            targdir_([1:(opts.fs+opts.shwin) end-opts.fs+opts.shwin+1:end]) = [];
            behav.targdir(:,trial) = targdir_;

            %% DEVIATION
             dif = behav.mouse(:,trial) - behav.target(:,trial);
            behav.orient(:,trial) = abs(dif).*exp(1j*(unwrap(angle(dif))-unwrap(angle(behav.targdir(:,trial)))));
            
            dotp = real(dif).*real(behav.targdir(:,trial))+imag(dif).*imag(behav.targdir(:,trial));
            behav.deviation(:,trial) = sgolayfilt(dotp./abs(behav.targdir(:,trial)),2,2*opts.shwin+1);
           
             % perceived instantaneous error (IE) between the target and the mouse position
            behav.error(:,trial) = abs(behav.target(:,trial)-behav.mouse(:,trial));
           
            
            %% STATS
            behav.psd.speed(:,trial) = pwelch(behav.speed(:,trial),opts.fs,opts.fs/2,opts.fs,opts.fs);
            behav.psd.accel(:,trial) = pwelch(behav.accel(:,trial),opts.fs,opts.fs/2,opts.fs,opts.fs);
           behav.file(trial) = f;
            %% EPOCHS
            [pkamp,pkidx] = findpeaks(behav.accel(:,trial));
            for p = 1:length(pkidx)
                if pkidx(p)-opts.fs/2 > 0 && pkidx(p)+opts.fs/2 < opts.tlength*opts.fs
                    peak = peak+1;
                    behav.peaks.idx(peak) = pkidx(p);
                    behav.peaks.trial(peak) = trial;
                    behav.peaks.file(trial) = f;                    
                    behav.peaks.mouse(:,peak) = behav.mouse(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.target(:,peak) = behav.target(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.accel(:,peak) = behav.accel(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.speed(:,peak) = behav.speed(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.targdir(:,peak) = behav.targdir(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.orient(:,peak) = behav.orient(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.deviation(:,peak) = behav.deviation(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);
                    behav.peaks.error(:,peak) = behav.error(pkidx(p)+(-opts.fs/2:opts.fs/2),trial);                    
                end
            end
            
           
            
        end
    end
     
    directory = [opts.dir '/mat/'];
    if ~exist(directory,'dir')
        mkdir(directory);
    end
    matfile = sprintf('%s/%s_sm%d_behav.mat',directory,subject,opts.shwin);
    
    disp([' -- Saving to ' matfile]);
    save(matfile,'behav');
    fprintf(' ** %d trials, %d sub-movements\n',trial,peak);

end


