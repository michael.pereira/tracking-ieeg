clear
addpath config
addpath model
addpath helper

subjects = {'IR51','IR54','IR57'};
%subjects = {'IR54'};

conf;
configplots;

cond = [1];

cfg.fs = 500;
cfg.tlength = 10;

for subj=1:length(subjects)
    
    %%
    fprintf('Loading data for subject %s ... \n',subjects{subj});
    bfile = [cfg.matdir '/' subjects{subj} '_behav.mat'];
    load(bfile,'ltrial','btrial','dtrial');
    
    %  trialidx1 = find(ltrial == 1);
    trialidx = find(ismember(ltrial,[1,3]));
    ntrials = length(trialidx);
    
    accel = squeeze(btrial(:,2,trialidx));
    speed = squeeze(btrial(:,1,trialidx));
    
    dev = squeeze(btrial(:,7,trialidx));
    err = squeeze(btrial(:,6,trialidx));
    trialtype = ltrial(trialidx);
    trialdif = dtrial(trialidx);
    %% EPOCHS
    peak = 0;
    peaks = struct();
    for trial = 1:size(accel,2);
        [pkamp,pkidx] = findpeaks(accel(:,trial));
        for p = 1:length(pkidx)
            if pkidx(p)-cfg.fs/2 > 0 && pkidx(p)+cfg.fs/2 < cfg.tlength*cfg.fs% && pkamp(p) > std(accel(:))
                peak = peak+1; 
                peaks.trial(peak) = trialidx(trial);
                peaks.type(peak) = trialtype(trial);
                peaks.difficulty(peak) = trialdif(trial);
                
                peaks.idx(:,peak) = pkidx(p)+(-cfg.fs/2:cfg.fs/2);
                peaks.accel(:,peak) = accel(pkidx(p)+(-cfg.fs/2:cfg.fs/2),trial);
                peaks.speed(:,peak) = speed(pkidx(p)+(-cfg.fs/2:cfg.fs/2),trial);
                peaks.dev(:,peak) = dev(pkidx(p)+(-cfg.fs/2:cfg.fs/2),trial);
                peaks.error(:,peak) = err(pkidx(p)+(-cfg.fs/2:cfg.fs/2),trial);
            end
        end
    end
    
    %% FIT MODEL
    
    midt = cfg.fs/2+1;
    tlist = round(linspace(1,cfg.fs/2+1,51));
    %tlist(end) = [];
    tlist(end+(-2:0)) = [];
    track = find(peaks.type==1);
    trainidx = track(1:floor(length(track)/2));
    testidx = track((floor(length(track)/2)+1):length(track));
    fields = {'aic','bic','coeffs','coeffs_p','coeffs_se','r2'};
    model1 = struct();
    model2 = struct();
    n1 = [1,1,2,2,2,1,1];
    n2 = [1,1,3,3,3,1,1];
    i = 0;
    for f=fields
        i = i+1;
        model1.(cell2mat(f)) = zeros([length(tlist),n1(i)]);
        model2.(cell2mat(f)) = zeros([length(tlist),length(tlist),n2(i)]);
    end
    model1.model = cell(length(tlist),1);
    model2.model = cell(length(tlist),length(tlist));
    
    for t1=1:length(tlist)
        fprintf('=');
        X = real(peaks.dev(tlist(t1),trainidx)).';
        y = peaks.accel(midt,trainidx).';
        bad = y <= 0;
        X(bad,:) = [];
        y(bad,:) = [];
        
        model1.model{t1} = fitlm(X,y);
        model1.aic(t1) = 2*4 - 2*model1.model{t1}.LogLikelihood;
        model1.coeffs(t1,:) = model1.model{t1}.Coefficients.Estimate;
        model1.coeffs_p(t1,:) = model1.model{t1}.Coefficients.pValue;
        model1.coeffs_se(t1,:) = model1.model{t1}.Coefficients.SE;
        model1.r2(t1) = model1.model{t1}.Rsquared.Adjusted;
        Xtest = real(peaks.dev(tlist(t1),testidx).');
        ytest = peaks.accel(midt,testidx).';
        pred = model1.model{t1}.predict(Xtest);
        model1.predr2(t1) = corr(ytest,pred).^2;
        
        for t2 = 1:(t1-1)
            X = [real(peaks.dev(tlist(t1),trainidx)).' real(peaks.dev(tlist(t2),trainidx)).'];
            y = peaks.accel(midt,trainidx).';
            bad = y <= 0;
            X(bad,:) = [];
            y(bad,:) = [];
            
            model2.model{t1,t2} = fitlm(X,y);
            model2.aic(t1,t2) = 2*5 - 2*model2.model{t1,t2}.LogLikelihood;
            model2.coeffs(t1,t2,:) = model2.model{t1,t2}.Coefficients.Estimate;
            model2.coeffs_p(t1,t2,:) = model2.model{t1,t2}.Coefficients.pValue;
            model2.coeffs_se(t1,t2,:) = model2.model{t1,t2}.Coefficients.SE;
            model2.r2(t1,t2) = model2.model{t1,t2}.Rsquared.Adjusted;
            Xtest = real([peaks.dev(tlist(t1),testidx).' peaks.dev(tlist(t2),testidx).']);
            ytest = peaks.accel(midt,testidx).';
            pred = model2.model{t1,t2}.predict(Xtest);
            model2.predr2(t1,t2) = corr(ytest,pred).^2;
            
        end
    end
    fprintf('\n');
    %% EVALUATE MODEL
    figure(100+subj); clf; 
    model2.r2(model2.r2(:)==0) = NaN;
    
    
    r2 = max(model1.predr2);
    %relr2 = model2.predr2;
    relr2 = (model2.predr2 - r2)./r2;
    
    [model2_best,i] = nanmax(model2.predr2(:));
    [x1,x2] = ind2sub(size(relr2),i);
    bestidx = [tlist(x1),tlist(x2)];
    
    t = tlist./cfg.fs-0.5;
    imagesc(t,t,model2.predr2.'); hold on;
    plot(t(x1),t(x2),'ro','LineWidth',2);
    colorbar;
    axis xy; 
    caxis([0,0.2])
    %caxis([model2_best,nanmax(model2.aic(:))]);
    colormap('hot');
    c = model2.coeffs(x1,x2,:);
    title(sprintf('%s, max r2=%.2f t1=%.2f (c1=%.2f), t2=%.2f (c1=%.2f)',subjects{subj},model2_best,t(x1),c(2),t(x2),c(3)));
    pause(0.1); 
    
    X = [real(peaks.dev(tlist(x1),:)).' real(peaks.dev(tlist(x2),:)).'];
    y = peaks.accel(midt,:).';
    
    peaks.pred = model2.model{x1,x2}.predict(X).';
    peaks.fberr = real(peaks.dev(x2,:));
    peaks.prederr = real(peaks.dev(x1,:));
    
    checkbehav.r2(subj) = model2_best;
    checkbehav.speed(subj) = mean(mean(abs(diff(btrial(:,5,ltrial==1))),1),3);
    checkbehav.error(subj) = mean(mean(btrial(:,6,ltrial==1),1),3);
    checkbehav.terror(subj) = mean(mean(btrial(:,6,ltrial==1) > 10,1),3);
    checkbehav.nsub(subj) = sum(peaks.type == 1);
    
    bfile = [cfg.matdir '/' subjects{subj} '_behavepochs.mat'];
    
    save(bfile,'peaks','bestidx','model1','model2');
end