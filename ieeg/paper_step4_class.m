clear
addpath config
addpath helper
addpath /Users/michaelpereira/Toolboxes/michael/
conf;
configplots;
anat = {'acc_ipsi','acc_contra'};
%anat = {'sfg_ipsi','sfg_contra'}; %side = 'ipsi';
%anat = {'mfg_ipsi','mfg_contra','ifg_contra'};%,'sfg_ipsi','sfg_contra'}; side = 'ipsi';

%anat = {'acc_ipsi','acc_contra','sfg_ipsi','sfg_contra','mfg_ipsi','mfg_contra','ifg_contra'};
%anat = {'ofc_ipsi','ofc_contra'};

%anat = {'hc_ipsi','hc_contra'}; side = 'ipsi';
%anat = {'ins_ipsi','ins_contra'}; side = 'ipsi';
%anat = {'frontal'};
cond = [1];

chelec = {'MF9','','SMA4','LAC2','lSMA4'};
%chelec = {'MF11','','RAC4','LAC2','RAC3'};

band = 'theta';

for subj=[5,1]
    if subj == 1
        subject = 'IR48'; depth = 0; cfg = struct(); conf; conf_IR48;
    elseif subj == 2
        subject = 'IR49'; depth = 0; cfg = struct(); conf; conf_IR49;
    elseif subj == 3
        subject = 'IR51'; depth = 1; cfg = struct(); conf; conf_IR51;
    elseif subj == 4
        subject = 'IR54'; depth = 1; cfg = struct(); conf; conf_IR54;
    elseif subj == 5
        subject = 'IR57'; depth = 1; cfg = struct(); conf; conf_IR57;
    end
    
    
    %%
    fprintf('Loading data for subject %s ... \n',subject);
    bfile = [cfg.matdir '/' subject '_behav.mat'];
    load(bfile,'ltrial');
    
    pfile = [cfg.matdir '/' subject '_behavepochs.mat'];
    load(pfile,'peaks');
    
    %efile = [cfg.matdir '/' subject '_elphys_bp.mat'];
    %d1 = load(efile,'trials');
    
    %[data,elecs,regions] = select_electrodes(d1.trials,cfg,anat);
    
    efile = [cfg.matdir '/' subject '_elphys_' band '_hilb.mat'];
    d2 = load(efile,'trials');
    
    [dataspecpow,elecs,regions] = select_electrodes(d2.trials,cfg,anat);
    if isempty(elecs.elecpos) 
        continue;
    end
    trialidx1 = find(ismember(ltrial,[1,2,3]));
    ntrials1 = length(trialidx1);
   
    
    %% GET EEG EPOCHS
    ne = length(elecs.labels);
    np = length(peaks.idx);
    tr = unique(peaks.trial);
    ntr = length(tr);
    %plfp = zeros(np,ne,cfg.fs+1);
    pspecpow = zeros(np,ne,cfg.fs+1);
    
    for e=1:ne    
       % lfp = data((cfg.fs*3+1):end,trialidx1,e);
        specpow = (abs(dataspecpow((cfg.fs*3+1):end,trialidx1,e)));
        base = (abs(dataspecpow(cfg.fs:cfg.fs*3,trialidx1,e)));
        specpow = bsxfun(@rdivide,bsxfun(@minus,specpow,mean(base,1)),std(base,1));
        for p=1:np
           % plfp(p,e,:) = lfp(peaks.idx(:,p));
            pspecpow(p,e,:) = specpow(peaks.idx(:,p),peaks.trial(p));
        end
    end
    
    %% 
    tlist = 1:10:cfg.fs+1;
    ypred1 = zeros(length(tlist),np);
    ypred2 = zeros(length(tlist),np);
    
    thr1 = median(peaks.accel(cfg.fs/2+1,:));
    thr2 = -10; % median(peaks.prederr)
    ygt1 = (peaks.accel(cfg.fs/2+1,:) < thr1).';
    ygt2 = (peaks.prederr < thr2).';
    
    for trial = 1:ntr
        fprintf('=');
        testidx = peaks.trial == tr(trial);
        trainidx = ~(testidx);
        
        y1 = ygt1(trainidx).';
        y2 = ygt2(trainidx).';
     
        for t=1:length(tlist)    
            models = struct();
            %X_lfp = [plfp(:,:,t) y1*0+1];
            X_specpow = pspecpow(trainidx,:,tlist(t));
            X_specpow_test = pspecpow(testidx,:,tlist(t));
           
            [~,~,post1] = classify(X_specpow_test,X_specpow,y1);
            [~,~,post2] = classify(X_specpow_test,X_specpow,y2);
                ypred1(t,testidx) = post1(:,2);
                ypred2(t,testidx) = post1(:,2);
        end
    end
            fprintf('\n');
    %%

    
    figure(subj+200); clf; 
    hold on;
    for t = 1:length(tlist)    
        [~,~,~,auc1(t)] = perfcurve(ygt1.',ypred1(t,:).',1);
        [~,~,~,auc2(t)] = perfcurve(ygt2.',ypred2(t,:).',1);
    end
    
   % [c1,p1] = corr(ypred1',ygt1,'type','Spearman');
   % [c2,p2] = corr(ypred2',ygt2,'type','Spearman');
        t = tlist./cfg.fs-0.5;

    plot(t,auc1.','b','LineWidth',2)
    plot(t,auc2.','r','LineWidth',2)
   % c1(p1 > 0.01) = NaN;
   % c2(p2 > 0.01) = NaN;
   % c3(p3 > 0.01) = NaN;
    
   % plot(t,c1.','k','LineWidth',4)
   % plot(t,c2.','k','LineWidth',4)
    
    title(subject);
    legend({'Acceleration','Deviation'});
    pause(0.1);
end