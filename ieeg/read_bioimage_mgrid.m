function [elec] = read_bioimage_mgrid(mgridfile, brainfile, coord)

% --------------------------------------------------------
% READ_BIOIMAGE_MGRID reads BioImage Suite .mgrid files and converts them 
% into a FieldTrip-compatible elec datatype structure and converts electrode
% positions from BioImage Suite mgrid coordinates to standard voxel
% coordinates
%
% Use as:
%   elec = read_bioimage_mgrid(mgridfile)
%   where 
%        mgridfile has an .mgrid file extension
%        brainfile is the MR or CT that corresponds to the mgrid file and is
%                  in a format that can be read by ft_read_mri (e.g., .nii)
%   
%   optional third argument: coord
%       if coord = 'tal' then an additional transformation from voxel
%       coordinates to Talairach coordinates is applied
%       default = no additional transformation; electrode positions are
%       left in voxel coordinates
%   
%
% Copyright (C) 2016, Arjen Stolk & Sandon Griffin
% --------------------------------------------------------


% define output
elec.label = {}; % N x 1 cell array
elec.elecpos = []; % N x 3 matrix

% conditional variables
WaitForPos = 0;
WaitForDescript = 0;
WaitForDim = 0;

% open and read ascii-file line by line
fileline = 0;
fid = fopen(mgridfile,'r'); % open ascii-file
while fileline >= 0 % read line by line
  
  fileline = fgets(fid); % read a line
  if fileline > 0
    
    % grid number
    if ~isempty(findstr(fileline,'# Electrode Grid '))
     GridNr = sscanf(fileline(findstr(fileline, 'Grid '):end),'Grid %i');
     WaitForDescript = 1;
     WaitForDim = 1;
    end
    
    % grid description
    if ~isempty(findstr(fileline,'#Description')) && WaitForDescript 
       nextfileline = fgets(fid);
       GridDescript = sscanf(nextfileline,'%s');
       WaitForDescript = 0;
    end
    
    % grid dimension
    if ~isempty(findstr(fileline,'#Dimensions')) && WaitForDim 
       nextfileline = fgets(fid);
       GridDim = sscanf(nextfileline,'%i %i')'; % row & column
       WaitForDim = 0;
    end
    
    % electrode number
    if ~isempty(findstr(fileline,'# Electrode '))
      type = sscanf(fileline(findstr(fileline,'Electrode '):end),'Electrode %s');
      if ~strcmp(type, 'Grid')
        ElecNr = sscanf(fileline(findstr(fileline,'Electrode '):end),'Electrode %i %i')';
        WaitForPos = 1;
      end      
    end
    
    % electrode position
    if ~isempty(findstr(fileline,'#Position')) && WaitForPos 
       nextfileline = fgets(fid);
       ElecPos = sscanf(nextfileline,'%f %f %f')'; % x, y, and z
       WaitForPos = 0;
    end
    
    % store
    if ~isempty(findstr(fileline,'#Value'))  
      elec.label{end+1,1} = [GridDescript num2str(GridDim(2) - ElecNr(2) + GridDim(2)*ElecNr(1))];
      elec.elecpos(end+1,:) = ElecPos;
    end
    
  end % if fileline
end % end of while loop
fclose(fid);

% adjust mgrid coordinates to voxel coordinates
scan = ft_read_mri(brainfile);
elec.elecpos(:, 1) = elec.elecpos(:, 1)/scan.hdr.volres(2);
elec.elecpos(:, 2) = elec.elecpos(:, 2)/scan.hdr.volres(1);
elec.elecpos(:, 3) = elec.elecpos(:, 3)/scan.hdr.volres(3);

% adjust voxel coordinates to Talairach coordinates
if strcmp(coord, 'tal') == 1
    elec.elecpos = ft_warp_apply(scan.transform, elec.elecpos);
end

% add other fields to output
elec.chanpos = elec.elecpos;
elec.tra = eye(size(elec.elecpos,1));

