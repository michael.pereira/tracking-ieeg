function [ behav ] = loadbehav( file, fs,fstarg, version)
%BEHAV Summary of this function goes here
%   Detailed explanation goes here
if nargin < 4
    version = 2;
end
if nargin < 3
    fstarg = 256;
end
if nargin < 2
    fs = 125;
end

if version == 1
    mousidx = [9,10];
    targidx = [7,8];
else
    mousidx = [6,7];
    targidx = [4,5];    
end
twin = 0.106;
halfwin =  round(twin*fstarg/2);
sgolwin = halfwin*2+1;
behav = struct;
[~,sgol] = sgolay(2,sgolwin);



% read mouse position file
datamous = [zeros(1,size(file,2)) ; file];

% find trial separation
sepmous = find(all(datamous(:,1:4)==0,2));
d = diff(sepmous);
sepmous(d==1) = [];

ntrials = length(sepmous) - 1;
%disp('-- computing behav. measures');
s = 0;
for tr = 1:ntrials
    
    % extract trial
    mous_ = datamous(sepmous(tr)+1:sepmous(tr+1)-1,:);
    
    % extract triggers
    trigidx = find(all(mous_(:,1:4)==0,2));
    
    
    s = s+1;
    
    tlength = round(size(mous_,1)./fs);
    
    % resample
    mous = zeros(round(fstarg*tlength),size(mous_,2));
    
    for ch=1:size(mous_,2)
        mous__ = sgolayfilt(mous_(:,ch),2,7);
        %mous__ = mous_(:,ch);
        
        mous(:,ch) = interp1(linspace(0,1,size(mous_,1)),mous__,linspace(0,1,round(fstarg*tlength)),'pchip');
    end
    %behav.flash{s} = mous(:,3);
    % perceived mouse position in complex numbers
    m = mous(:,mousidx(1)) + 1i*mous(:,mousidx(2));
    behav.mouse{s} = sgolayfilt(m,2,sgolwin);
    
    % target position in complex numbers
    t = mous(:,targidx(1)) + 1i*mous(:,targidx(2));
    behav.target{s} = sgolayfilt(t,2,sgolwin);
    
    pad = zeros(fstarg,1);
    pad1 = m(fstarg:-1:1);
    pad2 = m(end:-1:end-fstarg+1);
    behav.speed{s} = fstarg*abs(filter(sgol(:,2),1,[pad1 ; m ; pad2]));
    behav.speed{s}([1:(fstarg+halfwin) end-fstarg+halfwin+1:end]) = [];
    behav.accel{s} = -filter(sgol(:,2),1,[pad ; behav.speed{s} ; pad]);
    behav.accel{s}([1:(fstarg+halfwin) end-fstarg+halfwin+1:end]) = [];
    
    
    pad1 = t(fstarg:-1:1);
    pad2 = t(end:-1:end-fstarg+1);
    behav.targspeed{s} = fstarg*abs(filter(sgol(:,2),1,[pad1 ; t; pad2]));
    behav.targspeed{s}([1:(fstarg+halfwin) end-fstarg+halfwin+1:end]) = [];
    behav.targaccel{s} = -filter(sgol(:,2),1,[pad ; behav.targspeed{s} ; pad]);
    behav.targaccel{s}([1:(fstarg+halfwin) end-fstarg+halfwin+1:end]) = [];
    
    behav.velocity{s} = -fstarg*filter(sgol(:,2),1,[m(halfwin:-1:1) ; m ; m(end+(-halfwin+1:0))]);
    behav.velocity{s}(1:2*halfwin) = []; 
    
    for p=1:length(behav.mouse{s})
        behav.trajdist{s} = min(abs(behav.mouse{s}(p) - behav.target{s}));
    end
    
    % perceived instantaneous error (IE) between the target and the mouse position
    behav.err{s} = abs(behav.target{s}-behav.mouse{s});
    
    
    tdir = sgolayfilt([diff(behav.target{s}) ; 0],2,sgolwin);
    err = sgolayfilt(behav.mouse{s} - behav.target{s},2,sgolwin);
    ang = angle(exp(1j*(angle(err) - angle(tdir))));
    behav.serr{s} = abs(behav.target{s}-behav.mouse{s}).*exp(1i*ang);
    behav.targetdir{s} = angle(tdir);
    
    dir = sgolayfilt([diff(behav.target{s}) ; 0],2,sgolwin);
    ang2 = angle(exp(1j*(angle(err) - angle(dir))));
    behav.serr2{s} = abs(behav.target{s}-behav.mouse{s}).*exp(1i*ang2);
    behav.mousedir{s} = angle(dir);

    
    
    
end
end