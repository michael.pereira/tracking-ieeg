import pygame as pg
crosscolor = (0,255,255)
white = (0,0,0)

class Cursor(pg.sprite.Sprite):
	def __init__(self,color,radius):
        # call DirtySprite initializer
		pg.sprite.Sprite.__init__(self)
		self.radius = radius
		print " [cu] Cursor radius: " + repr(self.radius)
		self.image = pg.Surface((self.radius*2,self.radius*2))
		self.image.fill(white)
		self.image.set_colorkey(white)
		self.color = color
		self.visible = 1
		#self.image = pointerimg
		self.rect = self.image.get_rect()
		self.screen = [10000,10000]
		self.factor = 1

	def setscreensize(self,screen,factor):
		self.screen = screen
		self.factor = factor

	def update(self):
		self.dirty = 1
		#print "update: setvisible: " + repr(self.visible)
		if self.visible:
			pg.draw.circle(self.image,self.color,(self.radius,self.radius),self.radius,0)
		else:
			pg.draw.circle(self.image,(0,0,0),(self.radius,self.radius),self.radius,0)

		#pg.draw.line(self.image,crosscolor,(self.radius/2,3),(self.radius/2,self.radius-3),3)
		#pg.draw.line(self.image,crosscolor,(3,self.radius/2),(self.radius-3,self.radius/2),3)
	def setpos(self,position):
		adjpos = [position[0]-self.radius,position[1]-self.radius]
		
		self.rect.topleft = adjpos
		self.update()

	def getpos(self):
		pos = self.rect.topleft
		return [pos[0]+self.radius,pos[1]+self.radius]

	def setvisible(self,visible):
		self.visible = visible