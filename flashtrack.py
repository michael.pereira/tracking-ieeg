#!/usr/bin/python
import math
import os
import sys
import time
import Tkinter

import pygame as pg
from numpy import loadtxt, subtract, abs, mean, power, array
from numpy.linalg import norm
from random import shuffle,gauss,randint

from configflash import Config
from interface import Interface
from cursor import Cursor
from target import Target
from syncsquare import Syncsquare

SCREEN_SIZE = (0, 0)
#SCREEN_SIZE = (1024,768)

STATE_FIX = 21
STATE_TRACK = 22
STATE_DRIFTCORR = 23
STATE_REST = 24

class FlashTrack(object):
	def __init__(self):
		self.init = 0
		# we start with the GUI
		self.startGUI()

	def startGUI(self):
		# draw a GUI to select the parameters before starting
		# the experiment	

		self.gui = Tkinter.Tk()
		w = 250 # width for the Tk root
		h = 350 # height for the Tk root

		ws = self.gui.winfo_screenwidth() # width of the screen
		hs = self.gui.winfo_screenheight() # height of the screen

		# calculate x and y coordinates for the Tk root window
		x = (ws/2) - (w/2)
		y = (hs/2) - (h/2)
		self.gui.geometry('%dx%d+%d+%d' % (w, h, x, y))

		# init variables
		stick = Tkinter.W+Tkinter.E
		subj = Tkinter.StringVar()
		#sess = Tkinter.StringVar()
		demo = Tkinter.IntVar()
		runsp = Tkinter.DoubleVar()
		nblocks = Tkinter.IntVar()
		row=0
		Tkinter.Label(self.gui, text='Subject ID',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		Tkinter.Entry(self.gui, textvariable=subj,width=15).grid(row=row,column=1,sticky=stick)
		row+=1
		#Tkinter.Label(self.gui, text='Session name',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		#Tkinter.Entry(self.gui, textvariable=sess,width=15).grid(row=row,column=1,sticky=stick)
		#row+=1
		Tkinter.Label(self.gui, text='Task type',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		tasktype = Tkinter.Listbox(self.gui, height=2)
		tasktype.insert(Tkinter.END,"Tracking")
		tasktype.insert(Tkinter.END,"Drawing")
		tasktype.selection_set(0)
		tasktype.grid(row=row,column=1,sticky=stick)
		row+=1
		Tkinter.Label(self.gui, text='DEMO MODE',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		Tkinter.Checkbutton(self.gui,variable=demo).grid(row=row,column=1,sticky=stick)
		row+=1
		Tkinter.Label(self.gui, text='Number of blocks',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		Tkinter.Scale(self.gui,variable=nblocks,from_=1,to=4,resolution=1,orient=Tkinter.HORIZONTAL).grid(row=row,column=1,sticky=stick)
		row+=1
		Tkinter.Label(self.gui, text='Target speed factor',borderwidth=1 ).grid(row=row,column=0,sticky=stick)
		Tkinter.Scale(self.gui,variable=runsp,from_=0.1,to=1.5,resolution=0.1,orient=Tkinter.HORIZONTAL).grid(row=row,column=1,sticky=stick)
		row+=1
		Tkinter.Button(self.gui,text="Start", command=lambda:self.start_protocol(\
			subj.get(),demo.get(),runsp.get(),\
			nblocks.get(),\
			tasktype.curselection()\
			)).grid(row=row,column=1,sticky=stick)
			#subj.get(),sess.get(),\
		# default variables
		demo.set(0)
		runsp.set(1)
		nblocks.set(2)
		self.gui.mainloop()

	def start_protocol(self,subj,demo,runsp,nblocks,tasktype):
		# remove GUI
		self.gui.quit()
		# get configurations
		self.config = Config()
		# should be defined in the GUI but just to be sure
		if not tasktype:
			tasktype = [0]
		self.tasktype = tasktype
		
		self.speedfactor = runsp
		self.nrep = nblocks

		if demo:
			self.config.ntr = 2
			self.nrep = 1
		# load interface (files, triggers, etc...)
		#self.iface = Interface(subj + "_" + sess,self.config.triggers,demo)
		#self.iface.save_config(subj,sess,demo,runsp,tasktype)

		self.iface = Interface(subj,self.config.triggers,demo)
		self.iface.save_config(subj,demo,runsp)#,tasktype)
		# the number of frames (i.e. screen refreshes) of a run
		self.nframe =  int(math.ceil(self.config.ttrial / 1000.0 * self.config.frate))
		self.nframefix = int(math.ceil(self.config.tfix / 1000.0 *  self.config.frate))
		self.nframerest = int(math.ceil(self.config.trest / 1000.0 *  self.config.frate))
		
		self.maketrials()
		
		trialtxt = "trials: "
		for i in range(0,self.nruns):
			trialtxt += "[" + repr(self.shufflecond[i]) + "," + repr(self.shuffletraj[i]) + "], "
		print " -- starting session for subject " + repr(subj)
		print " -- (runs at " + repr(runsp) + " speed for " + repr(self.nruns) + " runs)"
		print " -- trial order looks like: "
		print trialtxt
		print " ++ frames: " + repr(self.nframe) + " @ " + repr(self.config.frate)
		
		if not os.path.isdir('traj'):
			print "ERROR: no *traj* directory containing trajectory files, exiting"
			sys.exit()
		self.ntrajs = 0
		for t in range(0,self.config.ntr):
			if os.path.exists(os.path.join('traj',"traj" + repr(t) + ".txt")):
				self.ntrajs += 1
			else:
				break

		# start pygame engine
		pg.init()

		# init variables
		self.itrial = 0
		self.itraj = 0
		self.iframe = 0
		self.iframetraj = 0
		self.iframefix = 0
		self.iframerest = 0
		self.ifr = 0
		self.icond = 0
		self.icondlast = -1
		self.itraj = 0
		self.elaps = 0
		self.loop = 0
		self.block = 0
		self.pause = 0
		self.state = STATE_REST
		self.trajon = 1
		self.targeton = 1
		self.factor = 0.9
		self.beherror = [0.0]*self.nframe
		self.fullscreen = self.config.fullscreen
		self.screen = pg.display.set_mode(SCREEN_SIZE,pg.FULLSCREEN*(self.fullscreen) + pg.HWSURFACE*0)
		self.screen_w = self.screen.get_width()
		self.screen_h = self.screen.get_height()
		self.ntrpause = self.config.npause
		self.savetraj = []
		self.screen_center = [self.screen_w/2,self.screen_h/2] # Center of the screen.
		self.font = pg.font.SysFont('dejavusans', 14)
 		self.fontbig = pg.font.SysFont('dejavusans', 48)
 		self.fontsmall = pg.font.SysFont('dejavusans', 24)
 		self.txt = self.fontbig.render("",1,(200,200,200))
 		self.txt2 = self.fontbig.render("",1,(200,200,200))
		self.pos = pg.mouse.get_pos()
 		# cursor sprite
		self.cursor = Cursor(self.config.cursorcolor,self.config.cursorsize)
		# target sprite
		self.target = Target(self.config.targetcolor,self.config.targetcolorprep,self.config.targetcolorrest,self.config.targetsize)
		# sync sprite (fakes cursor behaviour for syncing with light sensor)
		# adaptation of Colin Hoy's code: 
		scaleFactor = self.screen_h / 720.0
		recSize = self.config.syncradius*scaleFactor
		print " ++ Sync rectangle size: " + repr(recSize)
		self.sync = Syncsquare(recSize)
		self.sync.setpos((0,self.screen_h-recSize))
			
		# for text
		self.txt = self.fontsmall.render(self.config.placetext, 1, (255,255,0))

		self.clock = pg.time.Clock()
		# hide (or show) mouse
		pg.mouse.set_visible(self.config.showmouse)
		# Put cursor, target and syncbox as active sprites
		self.active_sprites = pg.sprite.LayeredUpdates(self.target,self.cursor,self.sync)
		self.init = 1

	def main_loop(self):
		# Loop through trajectories and conditions
		if self.init == 0:
			pg.quit()
			sys.exit()

		self.elaps = 0
		# always start with target in the center
		self.target.setpos((self.screen_w/2,self.screen_h/2))
		self.cursor.setscreensize([self.screen_w,self.screen_h],0.9)

		self.waitinstructions(self.config.instr_init,0)

		self.iface.sendtrigger(8)
		self.state = STATE_REST
		self.sync.setvisible(0.5)
		while(self.itrial <= self.nruns):						
			# select condition and trajectory
			self.icond = self.shufflecond[self.itrial]
			print " ++  Starting condition " + repr(self.icond) 
			self.itraj = self.shuffletraj[self.itrial] % self.ntrajs
			print " ++  Starting trajectory " + repr(self.itraj)
			self.trajfile = os.path.join('traj',"traj" + repr(self.itraj) + ".txt")
			self.currenttraj = loadtxt(self.trajfile,delimiter=",") +  self.screen_center

			if randint(0,1) == 0:
				for i in range(0,len(self.currenttraj)):
					self.currenttraj[i][0] = self.screen_w - self.currenttraj[i][0]
			if randint(0,1) == 0:
				for i in range(0,len(self.currenttraj)):
					self.currenttraj[i][1] = self.screen_h - self.currenttraj[i][1]
			
			if self.icond == 0 and self.icondlast != self.icond:
				# draw
				self.waitinstructions(self.config.instr_draw_init1,0)
				#self.waitinstructions(self.config.instr_draw_init2)
				self.targeton = 0
				self.trajon = self.config.trajon
				self.txt2 = self.fontsmall.render("Drawing", 1, (255,255,0))
			elif self.icond == 3 and self.icondlast != self.icond:
				# watch
				self.block+=1
				self.waitinstructions(["Block " + repr(self.block) + ": Drawing"] + self.config.instr_watch_init1,0)
				self.waitinstructions(self.config.instr_watch_init2,0)
				self.targeton = 0
				self.trajon = 0
				self.txt2 = self.fontsmall.render("Watching - do not move", 1, (255,255,0))
			elif self.icond == 1 and self.icondlast != self.icond:
				# track
				self.block+=1
				self.waitinstructions(["Block " + repr(self.block) + ": Tracking"] + self.config.instr_track_init1,0)
				self.waitinstructions(self.config.instr_track_init2,0)
				self.targeton = 1
				self.trajon = 1
				self.txt2 = self.fontsmall.render("Tracking", 1, (255,255,0))
			
			self.active_sprites.add(self.target,self.cursor)			
			self.drawtraj(self.trajon,self.config.trajcolor1)
			self.txt = self.fontsmall.render(self.config.placetext, 1, (255,255,0))
			self.target.setpos(map(int,self.currenttraj[0]))
			self.loop = 1
			
			self.drawtraj(self.icond,self.config.trajcolor1)
			# for each frame
			while(self.loop):
	
				# keep in sync
				self.elaps += self.clock.tick_busy_loop(self.config.frate)
				self.fps = self.clock.get_fps()
				if (self.state == STATE_TRACK) and (abs(self.fps - self.config.frate) > 1):
					print "fps error: " + repr(self.fps) + " (state=" + repr(self.state)  + ")"
				self.ifr += 1

				self.newpos = pg.mouse.get_rel()
				self.targpos = self.target.getpos()

				# update state machine
				self.statemachine()

				# draw 
				self.drawsprite()

				# check for hardware events (key presses, ...)
				self.getevents()
			self.icondlast = self.icond
			self.itrial += 1
			if self.itrial == (self.nruns):
				self.exitprotocol()
		print "exiting in 10 seconds"
		self.exitprotocol()
				
	def statemachine(self):
		# this functions switches between states
		# STATE_REST
		# STATE_FIX
		# STATE_TRACK

		if (self.state == STATE_FIX):
			# STATE_FIX
			if (self.iframefix < self.nframefix):
				# increase fixation counter
				self.iframefix += 1
			else:
				# goto STATE_TRACK
				print "    --> switch to track (" + repr(self.icond) + ")"
				#if self.targeton == 0:
				#	self.active_sprites.remove(self.target)
				self.target.switchon()
				self.sync.setvisible(1)
				self.state = STATE_TRACK
				self.iface.sendtrigger(2)

				self.txt = self.fontbig.render("",1,(200,200,200))
				
				self.drawtraj(self.trajon,self.config.trajcolor1)
				self.iframe = 0
				self.iframetraj = 0
				self.ifr = 0
				self.loop = 1 
				self.elaps = 0
				
				if self.icond != 1:
					self.active_sprites.remove(self.target)
		elif (self.state == STATE_TRACK):
			# STATE_TRACK
			#print "iframe: " + repr(self.iframe) + " / " + repr(self.nframe)
			if (self.iframe < self.nframe):			
				# record behavioural data
				self.record()
				# increment counters
				self.iframe += 1
				self.iframetraj += self.speedfactor
			else:
				# goto STATE_REST
				self.loop = 0
				self.state = STATE_REST
				self.elaps = 0
				self.iface.sendtrigger(3)
				#set cursor to visible again
				#self.cursor.setvisible(1)
				#switch off the sync rectangle
				self.sync.setvisible(0)
				
				pg.mouse.set_pos([self.screen_w/2,200])
				#self.target.setpos((self.screen_w/2,self.screen_h/2))
				self.drawsprite()
				self.clock.tick_busy_loop(self.config.frate)
				self.target.switchoff()
				self.active_sprites.remove(self.target,self.cursor)
				self.behmeanerror = mean(array(self.beherror)>self.config.targetsize)
				if self.icond == 0:
					self.behmeanerror = -1
				if self.icond == 1:
					sc = int((1-self.behmeanerror)*100)
					self.waitinstructions(["Trial " + repr(self.itrial+1) + ") score:" + repr(sc)],1)
					#self.txt = self.fontbig.render("Score: " + repr(sc),1,(200,200,200))
					print "    *** Mean error: " + repr(self.behmeanerror) + " -> score: " + repr(sc)
				else:
					sc = -1

				# goto STATE_REST
				print "    --> switch to rest"
				#self.drawtraj(self.icond,self.config.trajcolor1)
				
				self.savetraj = self.iface.getbehav()
				self.iface.dump_file([float(self.itrial),float(self.icond),float(self.itraj),float(time.time()),float(sc)])
				self.iframerest = 0

		elif (self.state == STATE_REST):
			# STATE OF REST
			if norm(subtract(self.cursor.getpos(),self.target.getpos()),2) < self.config.targetsize:
				self.iframerest += 1
			else:
				self.iframerest = 0

			#self.txt = self.fontbig.render(self.config.placetext, 1, (50,200,50))
			if self.pause: 
				self.txt = self.fontbig.render("Paused",1,(200,200,200))
				self.drawtraj(self.icond,self.config.trajcolor1)
			if (self.iframerest > self.nframerest):
				# goto STATE_FIX
				print "    --> switch to fix"
				self.state = STATE_FIX
				if self.icond == 0:
					self.txt = self.fontsmall.render(self.config.doodletext, 1, (50,200,50))
				elif self.icond == 1:
					self.txt = self.fontsmall.render(self.config.tracktext, 1, (200,50,50))
				elif self.icond == 2:
					self.txt = self.fontsmall.render(self.config.watchtext, 1, (50,50,200))
				elif self.icond == 3:
					self.txt = self.fontsmall.render(self.config.watchbeforedoodletext, 1, (50,50,200))
				self.sync.setvisible(0.5)
				self.iframefix = 0
				self.iframerest = 0
				if self.icond == 0:
					trajcolor = self.config.trajcolor2
				else:
					trajcolor = self.config.trajcolor1
				self.drawtraj(self.icond,trajcolor)

				self.target.switchprep()
				
				if self.itrial % self.ntrpause == self.ntrpause - 1:
					self.pause = 1
					

	def getevents(self):
		for event in pg.event.get():
			if event.type == pg.QUIT:
				# quit event
				self.exitprotocol()
			elif event.type == pg.MOUSEBUTTONDOWN:
				self.wait = 0
			elif event.type == pg.KEYDOWN:
				if event.key == pg.K_ESCAPE:
					# ESC keypress (QUIT)
					self.exitprotocol()
				elif event.key == pg.K_f:
					# F keypress (FULLSCREEN TOGGLE)
					self.fullscreen = (self.fullscreen + 1) % 2
					pg.display.set_mode((0,0),self.fullscreen*pg.FULLSCREEN+pg.HWSURFACE*0)
					self.drawtraj(self.icond,self.config.trajcolor1)	
					self.sync.setpos((0,self.screen_h-self.config.syncradius*2))	
					print "fullscreen switch, realigning"

	def getpos(self,traj,iframe):
		# interpolate position
		ifr = int(math.floor(iframe))
		a = iframe - ifr
		pos = (1-a,1-a) * traj[(ifr) % len(traj)] + (a,a) * traj[(ifr+1) % len(traj)]
		return pos

	def record(self):
		# record error
		insterror = float(norm(subtract(self.cursor.getpos(),self.target.getpos()),2))
		#print "iframe: " + repr(self.iframe) + " - len: " + repr(len(self.beherror))
		self.beherror[self.iframe] = insterror

		# get target and cursor position and store them
		self.iface.append_file([float(self.iframe),float(self.elaps),float(insterror),float(self.targpos[0]),float(self.targpos[1]),float(self.pos[0]),float(self.pos[1])])
	
	def waitinstructions(self,lines,bigfont):
		# background
		self.bg = pg.Surface(self.screen.get_size()).convert()
		self.bg.fill(self.config.bgcolor)
		self.screen.blit(self.bg,(0,0))

		# wait during baseline ...
		cx = self.screen_center[0]
		cy = self.screen_center[1]
		self.wait = 1
		while(self.wait == 1):
			# set background behind sprites
			self.active_sprites.clear(self.screen,self.bg)
			l = 0 
			for line in lines:
				if bigfont:
					self.txt = self.fontbig.render(line,1,(200,200,200))
				else:
					self.txt = self.fontsmall.render(line,1,(200,200,200))

				#self.bg.blit(self.txt,(self.screen_w/2-self.txt.get_width()/2,cy-self.config.border[1]/2+(l-1)*80))
				self.bg.blit(self.txt,(self.screen_w/4,cy-self.config.border[1]/2+(l-1)*80))
				
				l = l + 1

			self.screen.blit(self.bg,(0,0))
			# find region to update
			#region = self.active_sprites.draw(self.screen)
			# update region
			#pg.display.update(region)
			# check for hardware events (key presses, ...)
			self.getevents()
			pg.display.flip()
			self.clock.tick(self.config.frate)
		# background
		#self.bg = pg.Surface(self.screen.get_size()).convert()
		#self.bg.fill(self.config.bgcolor)
		#self.screen.blit(self.bg,(0,0))
		#pg.display.flip()

	def maketrials(self):
		# shuffle conditions
		self.shufflecond = []
		self.shuffletraj = []
		if self.tasktype[0]==0:
			order = [1,0]
		else:
			order = [0,1]

		for rep in range(0,self.nrep):
				
			shufflecond = []
			shuffletraj = []
			if self.tasktype[0]==1:
				# add one watch trial
				self.shufflecond += [3]
				self.shuffletraj += [self.config.watchtrajid]
			
			# add ntr trials for tracking	
			shufflecond += [order[0]]*self.config.ntr
			for t in range(self.config.itr,self.config.itr+self.config.ntr):
				shuffletraj += [t]

			# shuffle trials for tracking and add
			combined = zip(shuffletraj, shufflecond)
			shuffle(combined)
			shuffletraj[:], shufflecond[:] = zip(*combined)
			self.shufflecond += shufflecond
			self.shuffletraj += shuffletraj

			if self.tasktype[0]==0:
				# add one watch trial
				self.shufflecond += [3]
				self.shuffletraj += [self.config.watchtrajid]

			# add ntr trials for drawing
			shufflecond = []
			shuffletraj = []
			shufflecond += [order[1]]*self.config.ntr
			for t in range(self.config.itr,self.config.itr+self.config.ntr):
				shuffletraj += [t]

			# shuffle trials for drawing and add
			combined = zip(shuffletraj, shufflecond)
			shuffle(combined)
			shuffletraj[:], shufflecond[:] = zip(*combined)
			self.shufflecond += shufflecond
			self.shuffletraj += shuffletraj
		

		self.nruns = len(self.shufflecond)

	def drawtraj(self,visible,trajcolor):
		
		self.bg = pg.Surface(self.screen.get_size()).convert()
		self.bg.fill(self.config.bgcolor)

		cx = self.screen_center[0]
		cy = self.screen_center[1]
		self.border = pg.Rect(cx-self.config.border[0]/2,cy-self.config.border[1]/2,self.config.border[0],self.config.border[1])
		pg.draw.rect(self.bg,self.config.bordercolor,self.border,4)
		#print " ++ loading traj%d\n" %(self.itraj)

		if visible == 1:

			x1 = self.currenttraj[0]
			for x2 in self.currenttraj:
				pg.draw.line(self.bg,trajcolor,x1,x2,4)
				x1 = x2
			x1 = self.currenttraj[0]
			i = 0
			for x2 in self.currenttraj:
				pg.draw.line(self.bg,(200,0,0),x1,x2,4)
				i += 1
				if i==25:
					x1_save = x1
				if i > 30:
					# draw the arrow
					angle =  math.pi/2
					# use a longer vector than one segment
					x_vec = [-(x1_save[0] - x2[0]),-(x1_save[1] - x2[1])]
					# one branch:
					# rotate
					x1_rot = [math.cos(angle)*(x_vec[0])-math.sin(angle)*(x_vec[1]),math.sin(angle)*(x_vec[0])+math.cos(angle)*(x_vec[1])]
					# add
					x1_final = [x1_save[0] + x1_rot[0],x1_save[1] + x1_rot[1]]
					pg.draw.line(self.bg,(200,0,0),x1_final,x2,4)
					# other branch
					x1_rot = [math.cos(-angle)*(x_vec[0])-math.sin(-angle)*(x_vec[1]),math.sin(-angle)*(x_vec[0])+math.cos(-angle)*(x_vec[1])]
					x1_final = [x1_save[0] + x1_rot[0],x1_save[1] + x1_rot[1]]
					pg.draw.line(self.bg,(200,0,0),x1_final,x2,4)
					break
				x1 = x2
		self.bg.blit(self.txt,(self.screen_w/2-self.txt.get_width()/2,cy-self.config.border[1]/2-50))
		self.bg.blit(self.txt2,(self.screen_w/2-self.txt2.get_width()/2,cy-self.config.border[1]/2-100))
		
		self.screen.blit(self.bg,(0,0))
		#region = self.active_sprites.draw(self.screen)
		pg.display.flip()

	def drawsprite(self):
		newpos = [self.newpos[0],self.newpos[1]]
		if self.pos[0]+self.newpos[0] < self.screen_w*(1-self.factor):
			newpos[0] = 0
		elif self.pos[0]+self.newpos[0] > self.screen_w*(self.factor):
			newpos[0] = 0
		if self.pos[1]+self.newpos[1] < self.screen_h*(1-self.factor):
			newpos[1] = 0
		elif self.pos[1]+self.newpos[1] > self.screen_h*(self.factor):
			newpos[1] = 0

		self.pos = (self.pos[0]+newpos[0],self.pos[1]+newpos[1])

				
		# set cursor to new position
		if (self.icond == 2) and (self.state == STATE_TRACK):
			cpos = [self.savetraj[self.iframe-1][8],self.savetraj[self.iframe-1][9]]
		elif (self.icond == 3) and (self.state == STATE_TRACK):
			pos = self.getpos(self.currenttraj,self.iframetraj-self.speedfactor)
			cpos = map(int,pos)
			#cpos = self.currenttraj[self.iframe-1]
		else:
			cpos = self.pos

		self.cursor.setpos(cpos)

		# set target position
		if self.state == STATE_TRACK:
			pos = self.getpos(self.currenttraj,self.iframetraj-self.speedfactor)
			self.target.setpos(map(int,pos))
		
		# set background behind sprites
		self.active_sprites.clear(self.screen,self.bg)
		# general call to update function
		self.active_sprites.update()
		# find region to update
		region = self.active_sprites.draw(self.screen)

		# update region
		pg.display.update(region)


	def exitprotocol(self):
		print "exiting"
		#self.cursor.setvisible(0)
		self.txt = self.fontbig.render(self.config.exitingtext, 1, (200,200,200))
		self.sync.setvisible(0.5)
			
		self.drawtraj(0,(0,0,0))

		self.iface.sendtrigger(8)
		self.elaps = 0
		while(self.elaps < 5000):
			self.elaps += self.clock.tick(self.config.frate)
			# set background behind sprites
			self.active_sprites.clear(self.screen,self.bg)
			self.newpos = pg.mouse.get_rel()
			self.drawsprite()
			# find region to update
			region = self.active_sprites.draw(self.screen)
			# update region
			pg.display.update(region)
			# check for hardware events (key presses, ...)
			self.getevents()
		self.iface.sendtrigger(8)
		self.sync.setvisible(0)
		# close interfaces
		self.iface.exit()
		# exit pygame
		pg.quit()
		sys.exit()

if __name__ == "__main__":
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pg.init()
    pg.display.set_mode(SCREEN_SIZE)
    run_it = FlashTrack()
    run_it.main_loop()
