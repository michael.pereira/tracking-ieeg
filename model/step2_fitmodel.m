clear

%% PARAMETERS
% list of subjects to process
opts.subjects = {'k7','o7','p4','p5','w1','w2','w3','w4','w5','w6','w7','w8','w9','x1'};


% target sampling rate (after downsampling)
opts.fs = 50;
opts.tlength = 16;
opts.shwin = 3;
opts.dir = '/Users/michaelpereira/data/tracking/';
opts.fitmodel ='glm';
opts.distr = 'normal';
opts.link = 'log';
s = 0;
for subj = opts.subjects;
    
    s = s+1;
    subject = subj{:};
    directory = [opts.dir '/mat/'];
    matfile = sprintf('%s/%s_sm%d_behav.mat',directory,subject,opts.shwin);
    fprintf('Processing subject %s: %s\n',subject,matfile);
    load(matfile,'behav');
    
    nsm = length(behav.peaks.idx);
    [c,p] = corr(behav.peaks.accel(26,:).',behav.peaks.deviation.');
    results.corr(:,s) = c;
    [results.bestcorr(s),results.idxbestcorr(s)] = min(c(1:26));
    
    %plot(behav.peaks.deviation(results.idxbestcorr(s),:),behav.peaks.accel(26,:),'.');
    
    trainidx = 1:floor(nsm/2);
    testidx = (floor(nsm/2)+1):nsm;
    
    for t1=1:25
        X = behav.peaks.deviation(t1,trainidx).';
        y = behav.peaks.accel(26,trainidx).';
        bad = y <= 0;
        X(bad,:) = [];
        y(bad,:) = [];
        
        if strcmp(opts.fitmodel,'lm')
            model1{s}.model{t1} = fitlm(X,y);
        elseif strcmp(opts.fitmodel,'glm')
            model1{s}.model{t1} = fitglm(X,y,'Distribution',opts.distr,'link',opts.link);
        elseif strcmp(opts.fitmodel,'loglik');
            params = [0,0.2,0.5]
        end
        model1{s}.aic(t1) = model1{s}.model{t1}.ModelCriterion.AIC;
        model1{s}.bic(t1) = model1{s}.model{t1}.ModelCriterion.BIC;
        model1{s}.coeffs(:,t1) = model1{s}.model{t1}.Coefficients.Estimate;
        model1{s}.coeffs_p(:,t1) = model1{s}.model{t1}.Coefficients.pValue;
        model1{s}.coeffs_se(:,t1) = model1{s}.model{t1}.Coefficients.SE;
        model1{s}.r2(t1) = model1{s}.model{t1}.Rsquared.Adjusted;
        model1{s}.Xtest(:,t1) = behav.peaks.deviation(t1,testidx).';
        model1{s}.ytest(:,t1) = behav.peaks.accel(26,testidx).';
        model1{s}.pred(:,t1) = model1{s}.model{t1}.predict(model1{s}.Xtest(:,t1));
        model1{s}.predr2(t1) = corr(model1{s}.ytest(:,t1),model1{s}.pred(:,t1)).^2;
        
        for t2 = 1:(t1-1)
            X = [behav.peaks.deviation(t1,trainidx).' behav.peaks.deviation(t2,trainidx).'];
            y = behav.peaks.accel(26,trainidx).';
            bad = y <= 0;
            X(bad,:) = [];
            y(bad,:) = [];
            
            if strcmp(opts.fitmodel,'lm')
                model2{s}.model{t1,t2} = fitlm(X,y);
            elseif strcmp(opts.fitmodel,'glm')
                model2{s}.model{t1,t2} = fitglm(X,y,'Distribution',opts.distr,'link',opts.link);
            elseif strcmp(opts.fitmodel,'loglik');
                %%
                init = [0.05,0.12,-2,1,1,2];
                X = behav.peaks.deviation(:,trainidx).';
                y = behav.peaks.accel(26,trainidx).';
                bad = y <= 0;
                X(bad,:) = [];
                y(bad,:) = [];
                
                [ params,stat ] = fit(X,y,init, opts,1);
            end
            model2{s}.aic(t1,t2) = model2{s}.model{t1,t2}.ModelCriterion.AIC;
            model2{s}.bic(t1,t2) = model2{s}.model{t1,t2}.ModelCriterion.BIC;
            model2{s}.coeffs(:,t1,t2) = model2{s}.model{t1,t2}.Coefficients.Estimate;
            model2{s}.coeffs_p(:,t1,t2) = model2{s}.model{t1,t2}.Coefficients.pValue;
            model2{s}.coeffs_se(:,t1,t2) = model2{s}.model{t1,t2}.Coefficients.SE;
            model2{s}.r2(t1,t2) = model2{s}.model{t1,t2}.Rsquared.Adjusted;
            model2{s}.Xtest(:,:,t1,t2) = [behav.peaks.deviation(t1,testidx).' behav.peaks.deviation(t2,testidx).'];
            model2{s}.ytest(:,t1,t2) = behav.peaks.accel(26,testidx).';
            model2{s}.pred(:,t1,t2) = model2{s}.model{t1,t2}.predict(model2{s}.Xtest(:,:,t1,t2));
            model2{s}.predr2(t1,t2) = corr(model2{s}.ytest(:,t1,t2),model2{s}.pred(:,t1,t2)).^2;
            
        end
    end
    max(model2{s}.predr2(:))
end

%%
if ~exist([opts.dir '/model/'])
    mkdir([opts.dir '/model/']);
end
matfile = [opts.dir '/model/model_sm' num2str(opts.shwin) '_' num2str(opts.fitmodel) '.mat'];
fprintf(' ** saving %s\n',matfile);
save(matfile,'model1','model2');

%%
ns = length(opts.subjects);
r2_a = 0;
r2_b = 0;
for s=1:ns
    r2_a = r2_a + model1{s}.predr2;
    r2_b = r2_b + model2{s}.predr2;
end
r2_a = r2_a./ns;
r2_b = r2_b./ns;
%%
figure();
t = linspace(-0.5,0,opts.fs/2);
imagesc(t,t(1:end-1),r2_b.'); axis xy
set(gca,'FontSize',16)
xlabel('t1 [s]');
xlabel('t1 [s]');
