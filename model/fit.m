function [ params,stat ] = fit(X,y, init, opts, justplot)
%UNTITLED10 Summary of this function goes here
%   Detailed explanation goes here
if nargin < 5 
    justplot = 0;
end
params = init;
init(1:2) = log(init(1:2));

if justplot==0
    options = optimset('Display','Iter','FinDiffRelStep',1e-2);
    
    [p, stat] = fminsearch(@fitfunc,init,options);
    params = p;
    params(1:2) = exp(params(1:2));
else
    stat = fitfunc(init);
    
end
    function stat = fitfunc(p)
    
        p(1:2) = exp(p(1:2));
        
        % Reset random number generator
        %rng('default');
        [pred,reg] = predict2(X,p,opts);
        
        chi2 = mean((pred - y).^2./y);
        fprintf('chi2: %.2f\n',chi2);
        stat=chi2;
        %[~,stat] = kstest2(pred,y);
        %stat = -log(stat);
        figure(1); clf;
        subplot(131); hold on;
        [k,s] = ksdensity(pred,-10:50);%,'support','positive'); 
        plot(s,k,'k--','LineWidth',2);
        [k,s] = ksdensity(y,-10:50);%,'support','positive');
        plot(s,k,'k','LineWidth',2);
        title(sprintf('t1=%.2f, t2=%.2f, a=%.2f, b=%.2f, c=%.2f, w=%.2f\n',p));
        subplot(132); hold on;
        [k,s] = ksdensity(reg); 
        plot(s,k,'k--','LineWidth',2);
        subplot(133); hold on;
        plot(y,pred,'.');
        plot(-10:50,-10:50,'r','LineWidth',2);
        axis([-10,50,-10,50]);
        title(sprintf('Corr: %.2f',corr(y,pred)));
        pause(0.01);
    end
end

