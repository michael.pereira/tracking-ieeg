function [ y,reg] = predict2( X,params, opts )
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

x1 = (26-params(1)*opts.fs);
x2 = (26-params(2)*opts.fs);

x1 = min(max(x1,1),25);
x2 = min(max(x2,1),25);

w1 = x1 - floor(x1);
w2 = x2 - floor(x2);

x1 = floor(x1);
x2 = floor(x2);


W = zeros(opts.fs+1,1);
W(x1) = params(3)*w1;
W(x1+1) = params(3)*(1-w1);
W(x2) = params(4)*w1;
W(x2+1) = params(4)*(1-w2);

reg = X*W + params(5);

%y = params(6)*exp(reg)./(1+exp(reg));
%y = params(6)*reg.^2;
%y = params(6)*exp(-reg.^2);
y= params(6)*reg;
end

