function [ simconf ] = sigmoid( dv, a,b, w)
%CONF_PREDICT_DISTR Summary of this function goes here
%   Detailed explanation goes here

if nargin < 4 || isempty(w)
    w = ones(size(dv));
end
reg = dv*a + b;

simconf = exp(w.*reg)./(1+exp(w.*reg));

end

