
%% PARAMETERS
% list of subjects to process
opts.subjects = {'k7','o7','p4','p5','w1','w2','w3','w4','w5','w6','w7','w8','w9','x1'};


% target sampling rate (after downsampling)
opts.fs = 50;
opts.tlength = 16;
opts.shwin = 3;
opts.dir = '/Users/michaelpereira/data/tracking/';
opts.fitmodel ='lm';
opts.distr = 'normal';
opts.link = 'logit';
s = 0;
for subj = opts.subjects;
    
    s = s+1;
    subject = subj{:};
    
    directory = [opts.dir '/mat/'];
    matfile = sprintf('%s/%s_sm%d_behav.mat',directory,subject,opts.shwin);
    fprintf('Processing subject %s: %s\n',subject,matfile);
    load(matfile,'behav');
    
    
    [model1_bestr2(s),imodel1_bestr2(s)] = max(model1{s}.predr2(:));
    [model2_bestr2(s),i] = max(model2{s}.predr2(:));
    [x1,x2] = ind2sub(size(model2{s}.predr2),i);
    imodel2_bestr2(:,s) = [x1,x2];
    
    tmodel1_bestr2(s) = (opts.fs/2+1-imodel1_bestr2(s))./opts.fs;
    tmodel2_bestr2(:,s) = (opts.fs/2+1-imodel2_bestr2(:,s))./opts.fs;
    
    model1_coeff(:,s) = model1{s}.coeffs(:,imodel1_bestr2(s));
    model2_coeff(:,s) = model2{s}.coeffs(:,x1,x2);
    
    model1_modeldistr(:,s) = ksdensity(model1{s}.pred(:,x1),-5:0.1:30);
    model1_datadistr(:,s) = ksdensity(model1{s}.ytest(:,x1),-5:0.1:30);
    
    model2_modeldistr(:,s) = ksdensity(model2{s}.pred(:,x1,x2),-5:0.1:30);
    model2_datadistr(:,s) = ksdensity(model2{s}.ytest(:,x1,x2),-5:0.1:30);
    
    
    
    
    
end