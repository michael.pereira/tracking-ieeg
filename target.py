import pygame as pg
from scipy import add,subtract

white = (0,0,0)
crosscolor = (100,100,100)
class Target(pg.sprite.Sprite):
	def __init__(self,coloron,colorprep,coloroff,radius):
        # call DirtySprite initializer
		pg.sprite.Sprite.__init__(self)
		self.image = pg.Surface((radius*2,radius*2))
		self.image.fill(white)
		self.image.set_colorkey(white)
		self.rect = self.image.get_rect()
		self.coloron = coloron
		self.coloroff = coloroff
		self.colorprep = colorprep
		self.switchoff()
		self.radius = radius
		self.visib = 1
		self.setpos((100,100))
		self.update()
        
	def update(self):
		
		pg.draw.circle(self.image,self.color,(self.radius,self.radius),self.radius,0)
		#pg.draw.line(self.image,crosscolor,(self.radius/2,3),(self.radius/2,self.radius-3),3)
		#pg.draw.line(self.image,crosscolor,(3,self.radius/2),(self.radius-3,self.radius/2),3)
	def setpos(self,position):
		self.rect.topleft = subtract(position,(self.radius,self.radius))
		#print "posx = " + repr(position[0]) + ", posy = " + repr(position[1])
		
		self.update()
	def getpos(self):
		return add(self.rect.topleft,(self.radius,self.radius))

	def switchon(self):
		if self.visib:
			self.color = self.coloron
	def switchprep(self):
		self.color = self.colorprep
	def switchoff(self):
		self.color = self.coloroff
	def setvisible(self,visible):
		self.visible = visible

