import pygame as pg
white = (0,0,0)

class Syncsquare(pg.sprite.Sprite):
	def __init__(self,size):
        # call DirtySprite initializer
		pg.sprite.Sprite.__init__(self)
		self.image = pg.Surface((size,size))
		self.image.fill(white)
		self.rect = self.image.get_rect()
		self.size = size
		self.visible = 1
		self.shape = 2
	def update(self):
		#print "update: setvisible: " + repr(self.visible)
		
		color = (self.visible*255,self.visible*255,self.visible*255)
		if self.shape == 1:
			pg.draw.circle(self.image,color,(self.size/2,self.size/2),self.size/2,0)
		else:
			pg.draw.rect(self.image,color,(0,0,self.size,self.size),0)
		

	def setpos(self,position):
		self.rect.topleft = position
		self.update()
	def getpos(self):
		return self.rect.topleft
	def setvisible(self,visible):
		self.visible = visible