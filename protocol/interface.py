'''
Created on Mar 14, 2014

@author: mpereira
'''
import os, pygame, time
from pygame.locals import *
import pygame as pg

from numpy import savetxt, loadtxt
from datetime import date

from ctypes import *


class Interface():
	def __init__(self,subjectid,trigger,demo=0):
		
		self.files = list()
		self.sc = list()
		self.beh = list()
		self.subjectid = subjectid
		self.directory = 'data'
		if not os.path.isdir('data'):
			os.makedirs('data')

		self.parport = trigger
		self.d = date.today()
		self.t = time.strftime("%H%M%S")
		self.demo = demo
		self.setup_file()
		self.init_parport()
		print " [if] setting up interface for subject " + self.subjectid + ", trigger=" + repr(self.parport)

	def exit(self):
		self.close_parport()
		self.close_file()

	def setup_file(self):

		fsc = self.open_file("score")
		self.files.append(fsc)

		fbh = self.open_file("behav")
		self.files.append(fbh)

	def save_config(self,subj,demo,runsp):#,tasktype):
		fid = self.open_file("config")
		fid.write("subject = " + repr(subj) + "\n")
		#fid.write("sess = " + repr(sess) + "\n")
		fid.write("demo = " + repr(demo) + "\n")
		fid.write("runsp = " + repr(runsp) + "\n")
		#fid.write("tasktype = " + repr(tasktype) + "\n")
		fid.close()

	def append_file(self,data):
		self.beh.append(data)
    
	def getbehav(self):
		return self.beh

	def dump_file(self,data): #itrial,itraj,frate,err):
		
		self.sc.append(data)
		#print "sc: " + repr(self.sc)
		savetxt(self.files[0], self.sc, fmt='%f', delimiter=',')
		self.sc = list()
		self.files[0].flush()
	
		self.beh.append([0.0,0.0,0.0,0.0,0.0,0.0,0.0])
		#print "beh: " + repr(self.beh)
		savetxt(self.files[1], self.beh, fmt='%f', delimiter=',')
		self.beh = list()
		self.files[1].flush()
        

	def open_file(self,txt): # INTERNAL
		f_orig = os.path.join(self.directory,self.subjectid +'_' + txt + '_' + repr(self.d.day) + repr(self.d.month) + repr(self.d.year) + "_" + self.t)
		if self.demo:
			f_orig += "_demo"
		print " [if] opening file: " + f_orig
		return file(f_orig + '.txt', 'a')
	
	def close_file(self):
		self.files[0].close()
		self.files[1].close()

	def init_parport(self):
		if self.parport==1:
			self.lpt = pylpttrigger
			self.lpt.open(0,10,0)
		elif self.parport == 2:
			self.lpt = windll.inpout32
			self.lptaddress = 0xD020
		
	def close_parport(self):
		if self.parport==1:
			self.lpt.close()

	def sendtrigger(self,level):
		if self.parport==1:
			self.lpt.signal(level) 
		elif self.parport ==2:
			self.lpt.Out32(self.lptaddress, int(level)) # sets corresponding pins to on
		#else:
		#	print " [if] NO HARDWARE TRIGGERS!!!"
	
	#print "sent trigger " + repr(level)

	