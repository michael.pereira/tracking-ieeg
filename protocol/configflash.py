from random import randrange
class Config:
    def __init__(self):
    	
		# GENERAL
		self.frate = 50	# the frame rate: 50 frames per second	
		self.trajon = 0 # whether we show the trajectory during doodling
		
		# set to 1 for fullscreen mode, if not, the window size is set 
		# on line 18 of the flashtrack.py script (SCREEN_SIZE)
		self.fullscreen = 1
		self.triggers = 0 # 0 to use only photodiode, 1 for Linux, 2 for Windows
		self.npause = -1 # 

		self.cursorsize = 4 # the diameter in pixels of the cursor
		self.targetsize = 10 # the diameter in pixels of the target
		self.watchtrajid = 5 # the id of the trajectory used for the watch condition
		self.showmouse = 0 # show mouse cursor for debugging
		self.syncradius = 90 # size of the square used for synchronization with the photodiode
		self.itr = 0 # start with trajectory 0
		self.ntr = 10 # number of trajectories to load for each block
		self.trajonbetween = 1 # whether to draw the trajectory during rest and prepare phase
		self.border = (500,400)

		# TIME
		self.ttrial = 10000 # the duration of the trial in ms		
		self.tfix = 1000 # preparation time before tracking starts
		self.trest = 1000 # time to stay on target before triggering preparation state and eventually tracking
		
		# COLORS
		self.bgcolor = (0,0,0)
		self.trajcolor1 = (120,120,120)
		self.trajcolor2 = (60,60,60)
		self.bordercolor = (50,50,50)
		self.cursorcolor = (255,255,255) # RGB color of the cursor		
		self.targetcolor = (0,150,0) # RGB color of the target during tracking
		self.targetcolorprep = (150,100,0) # RGB color of the target during preparation
		self.targetcolorrest = (150,0,0) # RGB color of the target during rest

		# TEXT
		self.instr_init = [\
		"Hello and thanks for participating in this experiment", \
		"which will provide us with valuable data", \
		"about how the brain monitors continuous movements.",\
		"(click to advance)"]
		self.instr_track_init1 = [\
		"In this block, you will have to follow the colored circle with the computer mouse.",\
		"(click to advance)"]
		self.instr_track_init2 = [\
		"Tracking rules:", \
		"- Move the mouse to track the colored circle as it moves along the white shape.",\
		"- To start, move the mouse onto the red circle. It will turn orange for 2 seconds, so get ready!",\
		"- When the circle turns green and starts moving, follow it with your mouse. ",\
		"- The longer you're on top of the green circle, the higher your score!",\
		"(click to advance)"]
		self.instr_watch_init1 = [\
		"In this block, you will be free to draw whatever shapes you like.", \
		"However, you should draw with slow, smooth movements like when you were tracking the circle.",\
		"You will start by watching an example."
		"(click to advance)"]
		self.instr_watch_init2 = [\
		"Watching rules:", \
		"- Do not move the computer mouse.",\
        "- Watch the white circle draw an example shape.",\
        "- Pay attention to how slow and smooth it moves.",\
        "(click to advance)"]
		self.instr_draw_init1 = [\
		"Drawing rules:", \
		"- Move the mouse to draw your own shapes, like the one you just saw.",\
        "- Move slowly and smoothly, like the example you watched.",\
        "- Please stay inside the gray box!",\
        "- To start, move the mouse onto the red circle. It will turn orange for 2 seconds, so get ready!",\
        "- Draw slowly and smoothly!",\
        "(click to advance)"]

		
		#self.instr_track_init1 = [\
		#"In the next block, you will have to use the computer mouse", \
		#"to track the colored circle that will move along the white trajectory."]
		#self.instr_track_init2 = [\
		#"To start, place the mouse on the red circle.", \
		#"The circle will turn orange for 1 second then green and start moving."]
		#self.instr_watch_init1 = [\
		#"In the next trial, do not use the computer mouse.", \
		#"You will see an example trajectory drawn.", \
		#"Please pay attention to the pace at which the cursor moves"]
		#self.instr_watch_init2 = [\
		#"To start, place the mouse on the red circle.", \
		#"The circle will turn orange for 1 second then start moving.",
		#"Remember you do not have to move the mouse!"]
		#self.instr_draw_init1 = [\
		#"In the next block, you will have to use the computer mouse", \
		#"to draw a trajectory similar to the one below.", \
		#"Please stay inside the grey square!"]
		#self.instr_draw_init2 = [\
		#"To start, place the mouse on the red circle.", \
		#"The circle will turn orange for 1 second then disapear and you can start.",
		#"Please remember to draw smoothly and slowly, as in the example"]

		self.watchbeforedoodletext = ""#Watch an example trajectory"
		self.doodletext = ""#"Draw your own trajectory, smooth and slow!"
		self.tracktext = ""#"Track the target along the trajectory"
		self.watchtext = ""#"Watch your performance without moving"
		self.placetext = "Place the mouse on the red circle to start"
		self.exitingtext = "Great job! You've finished this session"

		
    def getwaittime(self, t):
		if t[1] == 0:
			return t[0]
		else:
			return randrange(t[0],t[1])
