
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
addpath('functions');
ft_defaults;
nsub = length(conf.subjects);


for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 2 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 2 ] Processing %s\n',subj);
    end
    
    f = sprintf('%s/%s/ses-001/behav/%s_ses-001_run-*_beh.tsv',conf.dir.bids,subj,subj);
    files = dir(f);
    nrun = length(files);
    stage1 = cell(1,length(conf.bands));
    trial = 0;
    % for every file (i.e. run)
    beh = [];
    for r = 1:nrun
        fprintf(' [ step 2 ] %s - run-0%d\n',subj,r);
        
        
        traj = readtable([files(r).folder '/' files(r).name],'Delimiter','\t','FileType','text');
        if size(traj,2) == 12
            traj.Properties.VariableNames = {'id','t','trig1','trig2','trig3','err','trajx','trajy','curx','cury','mousex','mousey'};
        else
            traj.Properties.VariableNames = {'id','t','err','trajx','trajy','curx','cury'};
        end
        traj.t = traj.t*1e-3;
        
        
        score = readtable([files(r).folder(1:end-5) '/ieeg/' files(r).name(1:end-8) '_events.tsv'],'Delimiter','\t','FileType','text');
        
        
        sep = find(traj.id==0);
        sep(diff(sep)==1) = [];
        ntr = length(sep)-1;
        
        h1 = figure(s*1000+r*10); hold on;
        %h2 = figure(s*1000+r*10+1); hold on;
        %h3 = figure(s*1000+r*10+2); hold on;
        
        x = strsplit(subj,'-');
        
        for tr = 1:ntr
            selrow = sep(tr):(sep(tr+1)-2);
            trial = trial+1;
            fprintf('.');
            behav = computebehav(traj,selrow,conf.behav.gsg,conf);
            for f=fieldnames(behav).'
                beh(trial).(cell2mat(f)) = behav.(cell2mat(f));
            end
            beh(trial).score = score.score(tr);
            beh(trial).trajid = score.traj_id(tr);
            beh(trial).type = score.trial_type(tr);
            beh(trial).onset = score.onset(tr);
            
            if tr<=50
                %fprintf(' [ stage 2 ] plotting\n');
                subplot(10,5,tr);hold on
                if score.trial_type(tr)==1
                    plot(beh(trial).targ,'k');
                    plot(beh(trial).curs,'r');
                    title(sprintf('%s-%d: %.2f (%.0f)',x{2},r,beh(trial).trajcor,beh(trial).maxdist));
                elseif score.trial_type(tr)==3
                    plot(beh(trial).targ,'c');
                    title(sprintf('%s-%d: (view)',x{2},r));
                elseif score.trial_type(tr)==0
                    plot(beh(trial).curs,'g');
                    title(sprintf('%s-%d (draw)',x{2},r));
                end
                pause(0.1);
            end
            
            
            
        end
        fprintf('\n');
        
        set(gcf,'Position',[-600,1000,600,1200])
        
        saveas(h1,['figs/' subj '_run-0' num2str(r) '_trajectories.png']);
    end
%     %%
%         params = [0.1,0.01,2];%,0.1];
%     conf.fitnrep = 10;
%     conf.fitmaxiter=100;
%     [ param,stat] = fitmodel(beh([beh.type]==1 & [beh.maxdist] < 100), params,conf);
%     params = [0.01,0.01,30,1];
%     kfac = [0.1,0.01,0.001];
%     w = [1,5,10];
%     for i=1:3
%         for j=1:3
%             for k=1:3
%                 params = [kfac(i),kfac(j),w(k)];%,0.0001];
%                 conf.fitmaxiter = 10;
%                 conf.fitnrep = 1;
%                 rng('default');
%                 [ param(i,j,k,:),stat(i,j,k)] = fitmodel(beh([beh.type]==1), params,conf);
%             end
%         end
%     end
%     
%     
    
    %%
    %if 0
    %%
    %        tr = 5;
    %        playtraj(beh{tr}.targ(1:10:end),simcurs1(1:10:end),1/100)
    

stage2_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/behav/'];
if ~exist(stage2_dir,'dir')
    system(sprintf('mkdir -p %s',stage2_dir));
end
stage2_filename = [stage2_dir '/' subj '_ses-001_behav.mat'];
fprintf(' [ step 2 ] Saving behav of %s in %s\n',subj,stage2_filename);
save(stage2_filename,'beh','-v7.3');%,'para','stat','-v7.3');
end