
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 4 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 4 ] Processing %s\n',subj);
    end
    
    stage1_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    
    band = 'hg*';
    stage1_filename = [stage1_dir '/' subj '_ses-001_' band '.mat'];
    fprintf('Loading %s band of %s in %s\n',band,subj,stage1_filename);
    d = dir(stage1_filename);
    %%
    hga = cell(1);
    for b = 1:length(d)
        fprintf(' [  step 4 ] loading %s\n', d(b).name);
        load([d(b).folder '/' d(b).name],'data');
        fprintf(' [  step 4 ] processing %s\n', d(b).name);
        %tic;
        tsel = data.time{1} < 0;
        ntr = length(data.trial);
        [nel,nt] = size(data.trial{1});
        
        % get baseline
        sig = zeros(nel,nt,ntr);
        blmean = zeros(nel,1,ntr);
        blstd = zeros(nel,1,ntr);
        for tr = 1:ntr
            blmean(:,1,tr) = mean(data.trial{tr}(:,tsel),2);
            blstd(:,1,tr) = std(data.trial{tr}(:,tsel),[],2);
        end
        
        % add hg after normalization
        for tr = 1:ntr
            if b==1
                hga{tr} = (1/length(d))*bsxfun(@rdivide,bsxfun(@minus,data.trial{tr},mean(blmean,3)),mean(blstd,3));                 
            else
                hga{tr} = hga{tr} + (1/length(d))*bsxfun(@rdivide,bsxfun(@minus,data.trial{tr},mean(blmean,3)),mean(blstd,3));
            end
        end
       % toc
    end
    
    data.trials = hga; clear hga
    
    % smooth
    fprintf(' [  step 4 ] smoothing %s\n', d(b).name);    
    smhga = data.trials;
    for tr = 1:ntr
        for el=1:size(sig,1)
            smhga{tr}(el,:) = sgolayfilt(data.trials{tr}(el,:),2,(conf.sgwin/2*conf.fs)*2+1);
        end
    end
    
    %%
    stage4_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    if ~exist(stage4_dir,'dir')
        fprintf(' [ step 4 ] creating %s\n',stage4_dir);
        system(sprintf('mkdir -p %s',stage4_dir));
    end
    
    stage4_filename = [stage1_dir '/' subj '_ses-001_zhg.mat'];
    fprintf('Saving zhg for %s in %s\n',subj,stage4_filename);
    save(stage4_filename,'data','-v7.3');
    
    data.trials = smhga;
    stage4_filename = [stage1_dir '/' subj '_ses-001_smzhg.mat'];
    fprintf('Saving smzhg for %s in %s\n',subj,stage4_filename);
    save(stage4_filename,'data','-v7.3');
end



