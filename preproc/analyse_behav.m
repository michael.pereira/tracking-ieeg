
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
addpath('functions');
ft_defaults;
nsub = length(conf.subjects);


for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 2 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 2 ] Processing %s\n',subj);
    end
    
stage2_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/behav/'];
stage2_filename = [stage2_dir '/' subj '_ses-001_behav.mat'];
fprintf(' [ step 2 ] Saving behav of %s in %s\n',subj,stage2_filename);
load(stage2_filename,'beh');%,'para','stat','-v7.3');


tr = 5;
playtraj(beh(tr).targ,beh(tr).curs,1/100,10)
    

  %%  
trials = find([beh.type] == 1);
ntrial = length(trials);

err = nan(10000,ntrial);
magerr = nan(10000,ntrial);
angerr = nan(9900,ntrial);
derr = nan(9999,ntrial);
magderr = nan(9999,ntrial);
angderr = nan(9999,ntrial);

for tr=1:ntrial
    id = trials(tr);
    err(:,tr) = [beh(id).targ] - [beh(id).curs];
    
    magerr(:,tr) = abs(err(:,tr));
    angerr(:,tr) = angle(err(1:9900,tr).*exp(-1j*angle([beh(id).tvel].')));
    
    derr(:,tr) = 1/1000*diff(err(:,tr));
    magderr(:,tr) = abs(derr(:,tr));
    angderr(:,tr) = angle(derr(:,tr).*exp(-1j*angle(err(1:end-1,tr))));
    
end

end