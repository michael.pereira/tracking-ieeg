
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
addpath(conf.dir.jsonio);
ft_defaults;
nsub = length(conf.subjects);
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 2 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 2 ] Processing %s\n',subj);
    end
    
    %     stage1_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    %
    %     band = 'bb';
    %     stage1_filename = [stage1_dir '/' subj '_ses-001_' band '.mat'];
    %     fprintf('Loading %s band of %s in %s\n',band,subj,stage1_filename);
    %     d = dir(stage1_filename);
    %%
    
    b = 1;
    %   fprintf(' [  step 2 ] processing %s\n', d(b).name);
    %   load([d(b).folder '/' d(b).name],'data');
    
    rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_acpc_fr.mat'];
    if exist(rfile,'file')
        x = load(rfile);
        elec = x.elec_acpc_fr;
    else
        rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_acpc_r.mat'];
        if exist(rfile,'file')
            x = load(rfile);
            elec = x.elec_acpc_r;
        else
            rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_acpc_f.mat'];
            if exist(rfile,'file')
                x = load(rfile);
                elec = x.elec_acpc_f;
            else
                rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_acpc.mat'];
                if exist(rfile,'file')
                    x = load(rfile);
                    elec = x.elec_acpc;
                else
                    error('No ACPC recon');
                end
            end
        end
    end
    fprintf(' [ step 2 ] loading ACPC electrode positions from %s\n',rfile)
    %%
    elecfile = elecjson(conf.dir.bids,subj,'ACPC');
    
    l = length(elec.label);
    group = cell(l,1);
    for i=1:l
        for j=1:length(elec.label{i})
            if ~isletter(elec.label{i}(j))
                group{i} = elec.label{i}(1:j-1);
                break
            end
        end
    end
    [~,irsrt] = sort(elec.label);
    tbl = table(elec.label(irsrt),elec.elecpos(irsrt,1),elec.elecpos(irsrt,2),elec.elecpos(irsrt,3),ones(l,1),group(irsrt),...
        'VariableNames',{'name','x','y','z','size','group'});
    writetable(tbl,elecfile,'FileType','text','Delimiter','\t');
    
    %%
    
    rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_mni_v.mat'];
    if exist(rfile,'file')
        x = load(rfile);
        elec = x.elec_mni_v;
    else
        rfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_mni_frv.mat'];
        if exist(rfile,'file')
            x = load(rfile);
            elec = x.elec_mni_frv;
        else
            error('No MNI recon');
        end
    end
    fprintf(' [ step 2 ] loading MNI electrode positions from %s\n',rfile)
    
    elecfile = elecjson(conf.dir.bids,subj,'MNI');
    
    l = length(elec.label);
    group = cell(l,1);
    for i=1:l
        for j=1:length(elec.label{i})
            if ~isletter(elec.label{i}(j))
                group{i} = elec.label{i}(1:j-1);
                break
            end
        end
    end
    [~,irsrt] = sort(elec.label);
    tbl = table(elec.label(irsrt),elec.elecpos(irsrt,1),elec.elecpos(irsrt,2),elec.elecpos(irsrt,3),ones(l,1),group(irsrt),...
        'VariableNames',{'name','x','y','z','size','group'});
    writetable(tbl,elecfile,'FileType','text','Delimiter','\t');
    
    %%
    %     stage2_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    %     if ~exist(stage2_dir,'dir')
    %         fprintf(' [ step 2 ] creating %s\n',stage2_dir);
%         system(sprintf('mkdir -p %s',stage2_dir));
%     end
%     
%     stage2_filename = [stage2_dir '/' subj '_ses-001_zhg.mat'];
%     fprintf('Saving zhg for %s in %s\n',subj,stage2_filename);
%     save(stage2_filename,'data','-v7.3');
%     
%     
end



