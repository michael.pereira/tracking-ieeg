
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 4 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 4 ] Processing %s\n',subj);
    end
    
    stage1_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    
    band = 'low';
    stage1_filename = [stage1_dir '/' subj '_ses-001_' band '.mat'];
    fprintf('Loading %s band of %s in %s\n',band,subj,stage1_filename);
    d = dir(stage1_filename);
    %%
    b = 1;
    fprintf(' [  step 4 ] loading %s\n', d(b).name);
    load([d(b).folder '/' d(b).name],'data');
    
    %%
    [nel,nt] = size(data.trial{1});
    shift = ((1:nel).'*10);
    for tr=1:length(data.trial)
        %%plo
        figure(100); clf; hold on;
        t0 = data.sampleinfo(tr,1)/conf.fs;
        plot(t0+ data.time{tr},bsxfun(@plus,data.trial{tr},shift));
        plot(t0+[0 0],ylim(),'k--');
        plot(t0+[10 10],ylim(),'k--');
        xlim(t0+[-4,12])
        set(gca,'YTick',shift,'YTickLabel',data.label);
        title([subj ' tr ' num2str(tr)]);
        %%
        pause();
    end
end