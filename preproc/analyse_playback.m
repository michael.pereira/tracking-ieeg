
clear
addpath ../admin
addpath functions
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);


for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ analysis ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ analysis ] Processing %s\n',subj);
    end
    
    stage2_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/behav/'];
    stage2_filename = [stage2_dir '/' subj '_ses-001_behav.mat'];
    fprintf(' [ step 2 ] Loading behav of %s in %s\n',subj,stage2_filename);
    load(stage2_filename,'beh');
  %%
    tr = 1;
t = (0:conf.time.trial*conf.fs)./conf.fs;
t(end) = [];

Tstab = 0.2;
Tf = 0;%0.001;
wsigma = 2;
vsigma = 0;
%[sys] = pidmodel(Tstab,Tf,'PDF',[],1/conf.fs);
%simcurs = simcursor(sys,beh(tr).targ.',t,wsigma,vsigma);
k = [0.003,0,0.003];
[sys2] = pidmodel2(Tf,k,1/conf.fs);
rng('default');
simcurs1 = simcursor2(sys2,beh(tr).targ.',t,wsigma,vsigma);
%rng('default');
%simcurs2 = simcursor2_euclid(k,beh(tr).targ.',t,wsigma,vsigma);

v = filter(1000*[1 -1],1,beh(tr).curs.'-beh(tr).curs(1));

[simcurs,x,xsim] = regcursor2(k,beh(tr).targ.',beh(tr).curs.',t,wsigma,vsigma);

v2 = filter(1000*[1 -1],1,simcurs-simcurs(1));


figure(); 
clf;
subplot(121); hold on;
plot(abs(v))
plot(sqrt(xsim(2,:).^2 + xsim(4,:).^2))

subplot(122); hold on;
plot(abs(beh(tr).targ-beh(tr).curs))
plot(abs(beh(tr).targ-simcurs.'))
legend('Data','Sim');


%playtraj(beh(tr).targ,beh(tr).cusr,1/100,10,simcurs.')

%simcurs2 = simcursor2(sys2,0*beh{tr}.targ.',t,wsigma,vsigma);
%simcurs2 = simcurs2 + beh{tr}.targ.';

%%
simbeh = computesimbehav(simcurs1.',conf.behav.gsg,conf,beh(tr).targ.');



mean(abs(simbeh.curs-beh{tr}.targ))
playtraj(beh{tr}.targ(1:10:end),simcurs1(1:10:end),1/100)

end