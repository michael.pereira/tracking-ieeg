
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);

band = 'zhg';
tstat = [-0.2,0];
totel = 0;
allel = 0;
doplot = 0;
 mid = 501;
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 4 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 4 ] Processing %s\n',subj);
    end
    
    basedir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/'];
    
    datafile = [basedir '/ieeg/' subj '_ses-001_' band '.mat'];
    fprintf('Loading %s\n',datafile);
    load(datafile,'data');
    
    datafile = [basedir '/ieeg/' subj '_ses-001_theta.mat'];
    fprintf('Loading %s\n',datafile);
    theta = load(datafile,'data');
    
    behfile = [basedir '/behav/' subj '_ses-001_behav.mat'];
    fprintf('Loading %s\n',behfile);
    load(behfile);
    sel = find(elsel(data.label,conf.sub(s).anat.mpfc));
    ntr = length(data.trial);
    %%
    
    
    track = ([beh.type]==1);
    draw = ([beh.type]==0);
    clear avg
    for tr = 1:ntr
        for el=1:length(sel)
            avg(el,:,tr) = sgolayfilt(data.trial{tr}(sel(el),:,:),2,51);
            %avg(el,:,tr) = data.trial{tr}(sel(el),:,:);
        end
    end
    
    if 0
    figure();
    for el=1:length(sel)
        if el > 20, break; end
        subplot(5,4,el);hold on;
        cishade(squeeze(avg(el,:,track)).',0.4,'r',data.time{1},'LineWidth',1);
        cishade(squeeze(avg(el,:,draw)).',0.4,'c',data.time{1},'LineWidth',1);
        plot([0,0],ylim(),'k--');
        plot([10,10],ylim(),'k--');
        title([subj ': ' data.label{sel(el)}]);
        
        pause(0.01);
    end
    end
    %%
    
    
    t = linspace(-0.5,0.5,1001);
    clear  pkgamma pkerr pkprojerr pkperperr pksp pkacc pktype 
    for el=1:length(sel)
        pk = 0;
        allel = allel + 1;
        
        for tr = 1:ntr
            %locs = beh(tr).cpk.loc;
            sig = theta.data.trial{tr}(sel(el),:);
            [~,locs] = findpeaks(-sig,'MinPeakProminence',1,'MinPeakHeight',1); isthrough(allel) = 1;
            %[~,locs] = findpeaks(sig,'MinPeakProminence',1,'MinPeakHeight',1); isthrough(allel) = 0;
            
           %if mean(avg(el,locsth)) >= mean(avg(el,locspk))
           %     locs = locsth;
           %     isthrough(allel) = 1;
           % else
           %     locs = locspk;
               
           % end
            
            locsel = theta.data.time{tr}(locs) > 0.5 &  theta.data.time{tr}(locs)<9.5;
            locs = locs(locsel);
            
            npk = length(locs);
            acc = beh(tr).cacc;
            vel = beh(tr).cvel;
            err = abs(beh(tr).curs - beh(tr).targ);
            projerr = real(beh(tr).erroncurs);
            perperr = imag(beh(tr).erroncurs);
            
            for k = 1:npk
                if locs(k) > conf.fs/2 && locs(k)+conf.fs/2 < (conf.time.trial-0.2)*conf.fs
                    pk = pk+1;
                    pkgamma{el}(:,pk) = avg(el,locs(k)+(-conf.fs/2:conf.fs/2),tr);
                    pkerr{el}(:,pk) = err(locs(k)+(-conf.fs/2:conf.fs/2));
                    pkprojerr{el}(:,pk) = projerr(locs(k)+(-conf.fs/2:conf.fs/2));
                    pkperperr{el}(:,pk) = perperr(locs(k)+(-conf.fs/2:conf.fs/2));
                    pksp{el}(:,pk) = vel(locs(k)+(-conf.fs/2:conf.fs/2));
                    pkacc{el}(:,pk) = acc(locs(k)+(-conf.fs/2:conf.fs/2));
                    
                    pktype{el}(pk) = beh(tr).type;
                    
                    
                    
                end
            end
        end
        
        hga = mean(pkgamma{el}(t > 0 & t < 0.1,:)).';
        tblhga = hga;
        tblsuj = s*ones(size(tblhga));
        tblel = allel*ones(size(tblhga));
        tblacc = pkacc{el}(mid,:).';
        tblnextacc = mean(pkacc{el}(t > 0.1 & t < 0.3,:)).';
        
        tblsp = pksp{el}(mid,:).';
        tblerr = pkerr{el}(mid,:).';
        tblprojerr = pkprojerr{el}(mid,:).';
        tblperperr = pkperperr{el}(mid,:).';
        tbltype = pktype{el}.';
        tbl_ = table(tblsuj,tblel,tbltype,tblhga,tblerr,tblerr>10,tblprojerr,tblprojerr<-10,tblperperr,tblperperr>10,tblacc,tblnextacc,abs(tblsp),...
            'VariableNames',{'suj','el','type','hga','err','ierr','projerr','iprojerr','perperr','iperperr','acc','nextacc','sp'});
        
        if allel==1
            tbl = tbl_;
        else
            tbl = [tbl; tbl_];
        end
        
        iddraw = pktype{el}==3;
        idtrack = pktype{el}==1;
        idtrack_err = pktype{el}==1 & pkerr{el}(mid,:) > 10;%median(pkerr{el}(mid,pktype{el}==1));
        idtrack_noerr = pktype{el}==1 & pkerr{el}(mid,:) < 10;% median(pkerr{el}(mid,pktype{el}==1));
        idtrack_fwd = pktype{el}==1 & pkprojerr{el}(mid,:) > -10;
        idtrack_bwd = pktype{el}==1 & pkprojerr{el}(mid,:) < -10;
        
        allpkcorracc(:,allel) = corr(hga,pkacc{el}.');
        allpkcorrsp(:,allel) = corr(hga,abs(pksp{el}.'));
        allpkcorrerr(:,allel) = corr(hga,pkerr{el}.');
        allpkcorrprojerr(:,allel) = corr(hga,pkprojerr{el}.');
        allpkcorrperperr(:,allel) = corr(hga,pkperperr{el}.');
       
        allpkgamma_draw(:,allel) = mean(pkgamma{el}(:,iddraw),2);
        allpkgamma_track(:,allel) = mean(pkgamma{el}(:,idtrack),2);
        allpkgamma_track_err(:,allel) = mean(pkgamma{el}(:,idtrack_err),2);
        allpkgamma_track_noerr(:,allel) = mean(pkgamma{el}(:,idtrack_noerr),2);
        allpkgamma_track_fwd(:,allel) = mean(pkgamma{el}(:,idtrack_fwd),2);
        allpkgamma_track_bwd(:,allel) = mean(pkgamma{el}(:,idtrack_bwd),2);
        
        allpkerr_draw(:,allel) = mean(pkerr{el}(:,iddraw),2);
        allpkerr_track(:,allel) = mean(pkerr{el}(:,idtrack),2);
        allpkerr_track_err(:,allel) = mean(pkerr{el}(:,idtrack_err),2);
        allpkerr_track_noerr(:,allel) = mean(pkerr{el}(:,idtrack_noerr),2);
        allpkerr_track_fwd(:,allel) = mean(pkerr{el}(:,idtrack_fwd),2);
        allpkerr_track_bwd(:,allel) = mean(pkerr{el}(:,idtrack_bwd),2);
        
        allpkprojerr_draw(:,allel) = mean(pkprojerr{el}(:,iddraw),2);
        allpkprojerr_track(:,allel) = mean(pkprojerr{el}(:,idtrack),2);
        allpkprojerr_track_err(:,allel) = mean(pkprojerr{el}(:,idtrack_err),2);
        allpkprojerr_track_noerr(:,allel) = mean(pkprojerr{el}(:,idtrack_noerr),2);
        allpkprojerr_track_fwd(:,allel) = mean(pkprojerr{el}(:,idtrack_fwd),2);
        allpkprojerr_track_bwd(:,allel) = mean(pkprojerr{el}(:,idtrack_bwd),2);
        
        allpkacc_draw(:,allel) = mean(pkacc{el}(:,iddraw),2);
        allpkacc_track(:,allel) = mean(pkacc{el}(:,idtrack),2);
        allpkacc_track_err(:,allel) = mean(pkacc{el}(:,idtrack_err),2);
        allpkacc_track_noerr(:,allel) = mean(pkacc{el}(:,idtrack_noerr),2);
        allpkacc_track_fwd(:,allel) = mean(pkacc{el}(:,idtrack_fwd),2);
        allpkacc_track_bwd(:,allel) = mean(pkacc{el}(:,idtrack_bwd),2);
        
        tic
        rndsig = [pkgamma{el}(:,idtrack_noerr) pkgamma{el}(:,idtrack_err)];
        rndlab = [zeros(1,sum(idtrack_noerr)) ones(1,sum(idtrack_err))];
        rng('default');
        for i=1:1000
            rnd = randperm(length(rndlab));
            rndlab_ = rndlab(rnd);
            
            rndallpkgamma_track_err(:,allel,i) = mean(rndsig(:,rndlab_==1),2);
            rndallpkgamma_track_noerr(:,allel,i) = mean(rndsig(:,rndlab_==0),2);
        end
       
        
        dif(allel) = mean(allpkacc_track_err(t > 0 & t < 0.1,allel) - allpkacc_track_noerr(t > 0 & t < 0.1,allel));
        
        rnddif(allel,:) = squeeze(mean(rndallpkgamma_track_err(t > 0 & t < 0.1,allel,:) - rndallpkgamma_track_noerr(t > 0 & t < 0.1,allel,:),1));
        
        pval(allel) = mean(dif(allel) < rnddif(allel,:));
        fprintf(' * %s: through:%d, p = %f\n',data.label{sel(el)},isthrough(allel) ,pval(allel));
        
        toc
        
       
        if doplot
            %%
            figure(); 
            subplot(321); hold on; 
            cishade(pkacc{el}(:,pktype{el}==1).',0.4,'r',t,'LineWidth',2)
            title('Accel'); plot([0 0],ylim(),'k--'); plot(xlim(),[0,0],'k-'); 
            
            subplot(322); hold on;
            cishade(abs(pksp{el}(:,pktype{el}==1).'),0.4,'m',t,'LineWidth',2)
            title('Speed');plot([0 0],ylim(),'k--'); 
            
            subplot(323); hold on;
            cishade(pkerr{el}(:,pktype{el}==1).',0.4,'b',t,'LineWidth',2)
            title('Error');plot([0 0],ylim(),'k--'); 
            
            subplot(324); hold on;
            cishade(pkprojerr{el}(:,pktype{el}==1).',0.4,'c',t,'LineWidth',2)
            title('Proj. Error');plot([0 0],ylim(),'k--'); 
            
            subplot(325); hold on;
            cishade(pkgamma{el}(:,idtrack).',0.4,'r',t,'LineWidth',2)
            cishade(pkgamma{el}(:,iddraw).',0.4,'c',t,'LineWidth',2)
            title(sprintf('%s',data.label{sel(el)}));plot([0 0],ylim(),'k--'); 
          %  [~,p] = ttest2(pkgamma{el}(:,idtrack).',pkgamma{el}(:,iddraw).');
          %  h = nan(size(p)); h(p<0.05) = mean(pkgamma{el}(:)); plot(t,h,'k','LineWidth',2);
            
          subplot(326); hold on;
          cishade(pkgamma{el}(:,idtrack_err).',0.4,'r',t,'LineWidth',2)
          cishade(pkgamma{el}(:,idtrack_noerr).',0.4,'g',t,'LineWidth',2)
          title(sprintf('%s',data.label{sel(el)}));plot([0 0],ylim(),'k--');
          % [~,p] = ttest2(pkgamma{el}(:,pktype{el}==1 & pkerr{el}(mid,:) > 10).',pkgamma{el}(:,pktype{el}==1 & pkerr{el}(mid,:) < 10).');
          % h = nan(size(p)); h(p<0.05) = mean(pkgamma{el}(:)); plot(t,h,'k','LineWidth',2);
            
            
           pause(0.1);
        end
    end
end
   
%%
baseline = t>-0.5 & t<0.5;

allpkgamma_track_z = bsxfun(@rdivide,allpkgamma_track,mean(allpkgamma_track(baseline,:)));
allpkgamma_draw_z = bsxfun(@rdivide,allpkgamma_draw,mean(allpkgamma_draw(baseline,:)));
    figure; hold on;
   cishade(allpkgamma_draw_z.',0.4,'c',t,'LineWidth',2)  
   cishade(allpkgamma_track_z.',0.4,'m',t,'LineWidth',2)  
   [~,p] = ttest(allpkgamma_track_z.',allpkgamma_draw_z.');
   h = nan(size(p)); h(p<0.05) = mean(allpkgamma_track_z(:));
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
   plot([0 0],ylim(),'k--'); 
   
   %%
   allpkgamma_track_z = bsxfun(@rdivide,allpkgamma_track,mean(allpkgamma_track(baseline,:)));
   allpkgamma_track_err_z = bsxfun(@rdivide,allpkgamma_track_err,mean(allpkgamma_track_err(baseline,:)));
   allpkgamma_track_noerr_z = bsxfun(@rdivide,allpkgamma_track_noerr,mean(allpkgamma_track_noerr(baseline,:)));
   figure; hold on;
   cishade(allpkgamma_track_err_z.',0.4,'r',t,'LineWidth',2)  
   cishade(allpkgamma_track_noerr_z.',0.4,'g',t,'LineWidth',2)  
   [~,p] = ttest(allpkgamma_track_err_z.',allpkgamma_track_noerr_z.');
   h = nan(size(p)); h(p<0.05) = mean(allpkgamma_track_noerr_z(:));
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
   plot([0 0],ylim(),'k--'); 
   
     figure; hold on;
   cishade(allpkacc_track_err.',0.4,'r',t,'LineWidth',2)  
   cishade(allpkacc_track_noerr.',0.4,'g',t,'LineWidth',2)  
   [~,p] = ttest(allpkacc_track_err.',allpkacc_track_noerr.');
   h = nan(size(p)); h(p<0.05) = mean(allpkacc_track_noerr(:));
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
   plot([0 0],ylim(),'k--'); 
   
    figure; hold on;
   cishade(allpkerr_track_err.',0.4,'r',t,'LineWidth',2)  
   cishade(allpkerr_track_noerr.',0.4,'g',t,'LineWidth',2)  
   [~,p] = ttest(allpkerr_track_err.',allpkerr_track_noerr.');
   h = nan(size(p)); h(p<0.05) = mean(allpkerr_track_noerr(:));
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
   plot([0 0],ylim(),'k--'); 
   
   
    
    if 0
   %%
allpkgamma_track_fwd_z = bsxfun(@rdivide,allpkgamma_track_fwd,mean(allpkgamma_track_fwd(baseline,:)));
allpkgamma_track_bwd_z = bsxfun(@rdivide,allpkgamma_track_bwd,mean(allpkgamma_track_bwd(baseline,:)));
    figure; hold on;
   cishade(allpkgamma_track_bwd_z.',0.4,'r',t,'LineWidth',2)  
   cishade(allpkgamma_track_fwd_z.',0.4,'g',t,'LineWidth',2)  
   [~,p] = ttest(allpkgamma_track_bwd_z.',allpkgamma_track_fwd_z.');
   h = nan(size(p)); h(p<0.05) = mean(allpkgamma_track_bwd_z(:));
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
   plot([0 0],ylim(),'k--'); 
    end
    
    %%
    err = -10:1:10;
    meas = 'nextacc';
    clear histhga histhga_cnt histnextacc
    for el=unique(tbl.el).'
        for e=1:length(err)-1
            histhga(el,e) = nanmean(tbl.hga(tbl.type==1 & tbl.el==el & tbl.(meas) > err(e) & tbl.(meas) <= err(e+1)));
            histnextacc(el,e) = nanmean(tbl.nextacc(tbl.type==1 & tbl.el==el & tbl.(meas) > err(e) & tbl.(meas) <= err(e+1)));
            histacc(el,e) = nanmean(tbl.acc(tbl.type==1 & tbl.el==el & tbl.(meas) > err(e) & tbl.(meas) <= err(e+1)));
            histhga_cnt(el,e) = nansum((tbl.type==1 & tbl.el==el & tbl.(meas) > err(e) & tbl.(meas) <= err(e+1)));
            
        end
        histhga(el,:) = histhga(el,:)./mean(tbl.hga(tbl.type==1 & tbl.el==el));
    end
        
    figure(); 
    subplot(121); hold on;
    errorbar(err(1:end-1)+0.5*mode(diff(err)),nanmean(histhga),nanstd(histhga)./sqrt(el),'ko-')
     subplot(122); hold on;
     errorbar(err(1:end-1)+0.5*mode(diff(err)),nanmean(histnextacc),nanstd(histnextacc)./sqrt(el),'ro-')
    errorbar(err(1:end-1)+0.5*mode(diff(err)),nanmean(histacc),nanstd(histacc)./sqrt(el),'co-')
    
   