
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);

band = 'zhg';
tstat = [-0.2,0];
totel = 0;
allel = 0;
doplot = 0;
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 4 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 4 ] Processing %s\n',subj);
    end
    
    basedir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/'];
    
    datafile = [basedir '/ieeg/' subj '_ses-001_' band '.mat'];
    fprintf('Loading %s\n',datafile);
    load(datafile,'data');
    
    behfile = [basedir '/behav/' subj '_ses-001_behav.mat'];
    fprintf('Loading %s\n',behfile);
    load(behfile);
    sel = find(elsel(data.label,conf.sub(s).anat.mpfc));
    ntr = length(data.trial);
    %%
    
    
    track = ([beh.type]==1);
    draw = ([beh.type]==0);
    clear avg
    for tr = 1:ntr
        for el=1:length(sel)
            avg(el,:,tr) = sgolayfilt(data.trial{tr}(sel(el),:,:),2,51);
            %avg(el,:,tr) = data.trial{tr}(sel(el),:,:);
        end
    end
    
    if doplot
    figure();
    for el=1:length(sel)
        if el > 20, break; end
        subplot(5,4,el);hold on;
        cishade(squeeze(avg(el,:,track)).',0.4,'r',data.time{1},'LineWidth',1);
        cishade(squeeze(avg(el,:,draw)).',0.4,'c',data.time{1},'LineWidth',1);
        plot([0,0],ylim(),'k--');
        plot([10,10],ylim(),'k--');
        title([subj ': ' data.label{sel(el)}]);
        
        pause(0.01);
    end
    end
    %%
    
    
    pk = 0;
    clear  pkgamma pkerr pkprojerr pkperperr pksp pkacc pktype
    for tr = 1:ntr
        locs = beh(tr).cpk.loc;
        npk = length(locs);
        acc = beh(tr).cacc;
        vel = beh(tr).cvel;
        err = abs(beh(tr).curs - beh(tr).targ);
        projerr = real(beh(tr).erroncurs);
        perperr = imag(beh(tr).erroncurs);
        
        for k = 1:npk
            if locs(k) > conf.fs/2 && locs(k)+conf.fs/2 < (conf.time.trial-0.2)*conf.fs
                pk = pk+1;
                pkgamma(:,:,pk) = avg(:,locs(k)+(-conf.fs/2:conf.fs/2),tr);
                pkerr(:,pk) = err(locs(k)+(-conf.fs/2:conf.fs/2));
                pkprojerr(:,pk) = projerr(locs(k)+(-conf.fs/2:conf.fs/2));
                pkperperr(:,pk) = perperr(locs(k)+(-conf.fs/2:conf.fs/2));
                pksp(:,pk) = vel(locs(k)+(-conf.fs/2:conf.fs/2));
                pkacc(:,pk) = acc(locs(k)+(-conf.fs/2:conf.fs/2));
                pktype(pk) = beh(tr).type;
                
            end
        end
    end
    %%
    t = linspace(-0.5,0.5,conf.fs+1);
    %tsel = find(t > 0.0 & t < 0.2);
    tsel = find(t > tstat(1) & t < tstat(2));
    
    it = conf.fs/2+1;
    err = pkerr(it,:);%-pkerr(conf.fs/2+1-0.2*conf.fs,:);
    projerr = pkprojerr(it,:);%-pkerr(conf.fs/2+1-0.2*conf.fs,:);
    perperr = pkperperr(it,:);%-pkerr(conf.fs/2+1-0.2*conf.fs,:);
    acc = pkacc(it,:);
    
    totel = totel  + length(sel);
    
    draw = pktype == 0 & acc > 0;
    track = pktype == 1 & acc > 0;
    track_fwd = track & projerr > -10;%median(err(pktype == 1));
    track_bwd = track & projerr <= -10;%median(err(pktype == 1));
    
    track_perpdev = track & abs(perperr) > 10;%median(err(pktype == 1));
    track_perpnodev = track & abs(perperr) <= 10;%median(err(pktype == 1));
    
    track_acc3 = track & acc > 0 & acc > quantile(acc(acc>0),2/3);
    track_acc2 = track & acc > 0 & acc <= quantile(acc(acc>0),2/3) & acc > quantile(acc(acc>0),1/3);
    track_acc1 = track & acc > 0 & acc <= quantile(acc(acc>0),1/3);
    
    fprintf('fwd/bwd: %d / %d perpdev/perpnodev: %d / %d\n',...
        sum(track_fwd),sum(track_bwd),sum(track_perpdev),sum(track_perpnodev));
    for el=1:length(sel)
        
        
        %zpkgamma = bsxfun(@rdivide,pkgamma,mean(pkgamma(:,t>-0.2 & t<0,:),2));
        allel = allel+1;
        avgtrack(:,allel) = mean(pkgamma(el,:,track),3);
        avgdraw(:,allel) = mean(pkgamma(el,:,draw),3);
        avgtrack_bwd(:,allel) = mean(pkgamma(el,:,track_bwd),3);
        avgtrack_fwd(:,allel)= mean(pkgamma(el,:,track_fwd),3);
        avgtrack_highacc(:,allel) = mean(pkgamma(el,:,track_acc3),3);
        avgtrack_midacc(:,allel) = mean(pkgamma(el,:,track_acc2),3);
        avgtrack_lowacc(:,allel) = mean(pkgamma(el,:,track_acc1),3);
        
        avgtrack_fwd(:,allel)= mean(pkgamma(el,:,track_fwd),3);
        avgtrack_perpdev(:,allel) = mean(pkgamma(el,:,track_perpdev),3);
        avgtrack_perpnodev(:,allel) = mean(pkgamma(el,:,track_perpnodev),3);
        
        if doplot
            figure(); hold on;
        cishade(squeeze(pkgamma(el,:,track_bwd)).',0.4,'r',t,'LineWidth',2)
        cishade(squeeze(pkgamma(el,:,track_fwd)).',0.4,'g',t,'LineWidth',2)
        cishade(squeeze(pkgamma(el,:,draw)).',0.4,'c',t,'LineWidth',2)
        rng('default');
        [h,~,p_] = permtest(squeeze(pkgamma(el,tsel,track_bwd)).',squeeze(pkgamma(el,tsel,track_fwd)).',1000);
        if any(h)
            m = min(mean(pkgamma(el,:,:),3))*0.7;
            sig = nan(size(t));
            sig(tsel(h==1)) = m;
            plot(t,sig,'k','LineWidth',2);
        end
        %[~,p] = ttest2(squeeze(mean(pkgamma(el,tsel,track_higherr),2)),squeeze(mean(pkgamma(el,tsel,track_lowerr),2)));
        p = min(p_);
        yl = ylim();
        fill([tstat(1),tstat(2),tstat(2),tstat(1),tstat(1)],[yl(1),yl(1),yl(2),yl(2),yl(1)],'k','FaceAlpha',0.2);
        title(sprintf('%s: %s (p = %.4f)',subj,data.label{sel(el)},p));
        pause(0.01);
        
        end
        
    end
end
%%
baseline = t>-0.2 & t<0;
avgtrack_z = bsxfun(@rdivide,avgtrack,mean(avgtrack(baseline,:)));
avgdraw_z = bsxfun(@rdivide,avgdraw,mean(avgdraw(baseline,:)));
avgtrack_bwd_z = bsxfun(@rdivide,avgtrack_bwd,mean(avgtrack_bwd(baseline,:)));
avgtrack_fwd_z = bsxfun(@rdivide,avgtrack_fwd,mean(avgtrack_fwd(baseline,:)));
avgtrack_perpdev_z = bsxfun(@rdivide,avgtrack_perpdev,mean(avgtrack_perpdev(baseline,:)));
avgtrack_perpnodev_z = bsxfun(@rdivide,avgtrack_perpnodev,mean(avgtrack_perpnodev(baseline,:)));

avgtrack_highacc_z = bsxfun(@rdivide,avgtrack_highacc,mean(avgtrack_highacc(baseline,:)));
avgtrack_midacc_z = bsxfun(@rdivide,avgtrack_midacc,mean(avgtrack_midacc(baseline,:)));
avgtrack_lowacc_z = bsxfun(@rdivide,avgtrack_lowacc,mean(avgtrack_lowacc(baseline,:)));

      figure; hold on;
   cishade(avgdraw_z.',0.4,'c',t,'LineWidth',2)  
   cishade(avgtrack_z.',0.4,'r',t,'LineWidth',2)  
   
    figure; hold on;
   cishade(avgdraw_z.',0.4,'c',t,'LineWidth',2)  
   cishade(avgtrack_bwd_z.',0.4,'r',t,'LineWidth',2)  
   cishade(avgtrack_fwd_z.',0.4,'g',t,'LineWidth',2)  
   
    figure; hold on;
   %cishade(avgtrack_lowacc_z.',0.4,'b',t,'LineWidth',2)  
   cishade(avgtrack_midacc_z.',0.4,'c',t,'LineWidth',2)  
   cishade(avgtrack_highacc_z.',0.4,'m',t,'LineWidth',2)  
   [h,p] = ttest(avgtrack_midacc_z.',avgtrack_highacc_z.');
   h(p>0.05) = NaN;
   plot(t,h,'k','LineWidth',2);
   %cishade(avgdraw.',0.4,avgtrack_midacc_z,'g',t,'LineWidth',2)  
        
% %%
% allel = 0;
% clear avgtrack2 avgdraw2 avgtrack_bwd2 avgtrack_fwd2 avgtrack_perpdev2 avgtrack_perpnodev2
% keep = squeeze(avgtrack(1,:,:));
% for s=1:7
%     for e=1:13
%         if keep(s,e)%sum(avgdraw(:,s,el))
%             allel = allel + 1;
%             fprintf('keep s=%d, e=%d\n',s,e);
%             avgtrack2(:,allel) = avgtrack(:,s,e);
%             avgdraw2(:,allel) = avgdraw(:,s,e);
%             avgtrack_bwd2(:,allel) = avgtrack_bwd(:,s,e);
%             avgtrack_fwd2(:,allel)= avgtrack_fwd(:,s,e);
%             avgtrack_perpdev2(:,allel) = avgtrack_perpdev(:,s,e);
%             avgtrack_perpnodev2(:,allel) = avgtrack_perpnodev(:,s,e);
%         else
%             fprintf('rm s=%d, e=%d\n',s,e);
%         end
%     end
% end
