

clear
addpath ../admin
addpath functions/
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);


[ftver, ftpath] = ft_version;
lh = load([ftpath filesep 'template/anatomy/surface_pial_left.mat']);
rh = load([ftpath filesep 'template/anatomy/surface_pial_right.mat']);

for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 1 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 1 ] Processing %s\n',subj);
    end
    % read electrode tsv file
    chanfile = sprintf('%s/%s/ses-001/ieeg/%s_ses-001_channels.tsv',conf.dir.bids,subj,subj);
    fprintf(' [ step 1 ] Loading channel file %s\n',chanfile);
    elecs = readtable(chanfile,'Delimiter','\t','FileType','text');
    
    %% read electrode position file
    
    %elecfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_mni_v.mat'];
    elecfile = [conf.dir.bidsproc '/recon/' subj '/ses-001/anat/' subj '_elec_mni_*v.mat'];
    d = dir(elecfile);
    if ~isempty(d)
        el = load([d(1).folder '/' d(1).name]);
        
        sys = d(1).name(8:end-4);
    else
        warning(['Did not find electrode positions: ' elecfile]);
    end
    
    if 0
    figure();
    ft_plot_mesh(lh.mesh,'facecolor', 'skin', 'facealpha' ,0.7,'edgecolor', 'none');
    ft_plot_mesh(rh.mesh,'facecolor', 'skin', 'facealpha' ,0.7,'edgecolor', 'none');
    
    view([-90 20]);
    material dull;
    lighting gouraud;
    camlight;
    
    ft_plot_sens(el.(sys));
    
    pause(1);
    end
    %%
    % read ieeg directory
    files = dir(sprintf('%s/%s/ses-001/ieeg/%s_ses-001_run-*_ieeg.vhdr',conf.dir.bids,subj,subj));
    nrun = length(files);
    stage1 = cell(1,length(conf.bands));
    
    % for every file (i.e. run)
    for r = 1:nrun
        % load data
        fprintf(' [ step 1 ] Loading functional data %s\n',files(r).name);
        cfg = [];
        cfg.demean         = 'yes';
        cfg.dataset = [files(r).folder '/' files(r).name];
        alldata = ft_preprocessing(cfg);
        alldata.elec = el.(sys);
        alldata.elec.type = 'eeg';
        data = [];
        
        %% BEHAV
        behfile = sprintf('%s/%s/ses-001/behav/%s_ses-001_.tsv',conf.dir.bids,subj,subj);

        %% REREFERENCING
        if isfield(conf.sub(s),'grids')
            % reref for GRIDS
            ngrid = length(conf.sub(s).grids);
            
            chid = cell(1,ngrid);
            
            for g = 1:ngrid
                grid = conf.sub(s).grids{g};
                % find electrode on grid
                chid{g} = find(strncmp(grid,elecs.name.',length(grid)));
                if isempty(chid{g})
                    warning(['Did not find electrodes for grid ' grid]);
                end
                % remove bad
                chid{g}(strcmp(elecs.status(chid{g}),'bad')) = [];
            end
            uniqref = unique(conf.sub(s).ref);
            for ref = 1:length(uniqref)
                id = cell2mat(chid(conf.sub(s).ref==ref));
                fprintf(' [ step 1 ] Common average with %d electrodes\n',length(id));
                chans = elecs.name(id);

                %%
                
                cfg = [];
                cfg.reref = 'yes';
                cfg.refmethod = 'avg';
                cfg.channel = chans;
                cfg.refchannel = chans;
                newdata = ft_preprocessing(cfg,alldata);
                if isempty(data)
                    data = newdata;
                else
                    data = ft_appenddata([],data,newdata);
                end
            end
        end
        
        
        elecsel = strcmp(elecs.type,'SEEG') & strcmp(elecs.status,'good');
        if any(elecsel)
            %% reref for DEPTHS
            cfg = [];
            cfg.channel = elecs.name(elecsel);
            cfg.reref = 'yes';
            cfg.refchannel = 'all';
            cfg.refmethod = 'bipolar';
            cfg.updatesens = 'yes';
            newdata = ft_preprocessing(cfg,alldata);
            ndepths = length(newdata.label);
            bad = false(1,ndepths);
            for e=1:ndepths
                spl = strsplit(newdata.label{e},'-');
                if length(spl) ~= 2
                    warning('Something super fishy here');
                else
                    if ~strcmp(spl{1}(1:3),spl{2}(1:3))
                        fprintf(' [ step 1 ] removing channel %s from two different electrodes \n',newdata.label{e});
                        bad(e) = 1;
                    elseif abs(str2double(spl{1}(4:end))-str2double(spl{2}(4:end))>2)
                        fprintf(' [ step 1 ] removing channel %s with large difference \n',newdata.label{e});
                        bad(e) = 1;
                    end
                end
            end
            cfg = [];
            cfg.channel = newdata.label(~bad);
            newdata = ft_selectdata(cfg, newdata);
            if isempty(data)
                data = newdata;
            else
                data = ft_appenddata([],data,newdata);
            end
            
            
        end
        
        [excl,chanexcl] = setdiff(data.elec.label,data.label);
        data.elec.label(chanexcl) = [];
        data.elec.chanpos(chanexcl,:) = [];
        data.elec.chantype(chanexcl) = [];
        data.elec.chanunit(chanexcl) = [];
        if isfield(data.elec,'tra')
            data.elec.tra(chanexcl,:) = [];
        end
        %data.elec.elecpos(excl,:) = [];
        
        
        cfg=[];
        cfg.resamplefs = conf.fs;
        fprintf(' [ stage1 ] resampling at %.0 Hz\n',cfg.resamplefs);
        data = ft_resampledata(cfg,data);
        
        
        %% FILTERING
        nband = length(conf.bands);
        fdata = cell(1,nband);
        for b = 1:nband
            
            
            cfg = [];
            cfg.lpfilter = 'yes';
            cfg.hpfilter = 'yes';
            
            cfg.lpfreq = conf.bands(b).lowpf;
            cfg.hpfreq = conf.bands(b).highpf;
            
            if conf.bands(b).usepwr==1
                cfg.hilbert = 'abs';
            else
                cfg.hilbert = 'no';
            end
            
            fprintf(' [ step 1 ] Filtering in %s band [%.0f - %.0f]\n',conf.bands(b).name,cfg.lpfreq,cfg.hpfreq);
            
            fdata{b} = ft_preprocessing(cfg,data);
            %% EPOCHING
            eventfile = sprintf('%s/%s/ses-001/ieeg/%s_ses-001_run-00%d_events.tsv',conf.dir.bids,subj,subj,r);
            
            fprintf(' [ step 1 ] loading event file %s\n',eventfile);
            
            %event = ft_read_event(eventfile);
            tblevent = readtable(eventfile,'Delimiter','\t','FileType','text');
            
            event = struct();
            for e=1:size(tblevent,1)
                event(e).duration = tblevent.duration(e)*fdata{b}.fsample;
                event(e).sample = tblevent.onset(e)*fdata{b}.fsample;
                event(e).offset = event(e).sample + event(e).duration;
                event(e).value = tblevent.trial_type(e);
                event(e).score = tblevent.score(e);
            end
            
            %finaldata{b}.event = event;
            
            cfg = [];
            cfg.dataset = [files(r).folder '/' files(r).name];
            cfg.trialfun = 'getevents';
            cfg.event = event;
            cfg.fsample = fdata{b}.fsample;
            cfg.trialdef.eventtype      = 'onset';
            cfg.trialdef.eventvalue     = [1 3]; % the values of the stimulus trigger for all three conditions
            cfg.trialdef.prestim        = 4; % in seconds
            cfg.trialdef.poststim       = 12; % in seconds
            cfg.trialdef.offset       = -4; % in seconds
            
            cfg_stim = ft_definetrial(cfg);
            
            fdata{b} = ft_redefinetrial(cfg_stim, fdata{b});
            % cfg = ft_definetrial(cfg);
            if isempty(stage1{b})
                stage1{b} = fdata{b};
            else
                stage1{b} = ft_appenddata([],stage1{b},fdata{b});
            end
        end
        
        
        
    end
    %% 
    %ft_databrowser([],stage1{1});
    
    %% SAVE
    stage1_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    if ~exist(stage1_dir,'dir')
        fprintf(' [ step 1 ] creating %s\n',stage1_dir);
        system(sprintf('mkdir -p %s',stage1_dir));
    end
    for b = 1:nband
        band = conf.bands(b).name;
        stage1_filename = [stage1_dir '/' files(r).name(1:end-18) '_' band '.mat'];
        data = stage1{b};
        fprintf('Saving %s band of %s in %s\n',band,subj,stage1_filename);
        save(stage1_filename,'data','-v7.3');
    end
    
    
end