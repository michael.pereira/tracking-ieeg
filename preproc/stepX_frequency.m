
clear
addpath ../admin
conf = getconfig();
addpath(conf.dir.fieldtrip);
ft_defaults;
nsub = length(conf.subjects);
for s = 1:nsub
    subj = conf.subjects{s};
    
    
    if all(strcmp(conf.subjectspreproc,subj)==0)
        fprintf(' [ step 3 ] skipping %s\n',subj);
        continue
    else
        fprintf(' [ step 3 ] Processing %s\n',subj);
    end
    
    stage1_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    
    band = 'bb';
    stage1_filename = [stage1_dir '/' subj '_ses-001_' band '.mat'];
    fprintf('Loading %s band of %s in %s\n',band,subj,stage1_filename);
    load(stage1_filename,'data');
    
    %%
    cfg = [];
    cfg.method = 'wavelet';
    cfg.keeptrials  = 'yes';
    cfg.foi = 1:1:150;
    cfg.toi = -2:0.01:12;
    cfg.width = 7;
    
    freq = ft_freqanalysis(cfg, data);
    
    %%
    stage3_dir = [conf.dir.bidsproc '/ieegproc/' subj '/ses-001/ieeg'];
    if ~exist(stage1_dir,'dir')
        fprintf(' [ step 3 ] creating %s\n',stage3_dir);
        system(sprintf('mkdir -p %s',stage3_dir));
    end
    
    stage3_filename = [stage1_dir '/' subj '_ses-001_tf.mat'];
    fprintf('Saving tf for %s in %s\n',subj,stage3_filename);
    save(stage3_filename,'freq','-v7.3');
    
    if 0
       %%
       cfg = [];
       cfg.channel = 'MF1';
       freq2 = ft_selectdata(cfg,freq);
       
       idtrack = find(freq.trialinfo(:,2) == 1);c
       track = ft_selectdata(cfg,freq);
       
       draw = ft_selectdata(cfg,freq);
       
    end
end


