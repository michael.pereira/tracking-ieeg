Visuomotor tracking protocol
============================

Michael Pereira <michael.pereira@epfl.ch> +41 794 099 746

Quick start:
------------
Step 1: Install python 2.7, pygame, numpy (scipy) and tkinter
Step 2: Launch the protocol in demo mode (click on "Demo")
Step 2b: If the task is too difficult or easy for the participant, adjust the speed factor
Step 3: Launch the protocol without demo mode for two blocks -> (10 tracking - 1 watch + 10 drawing - 10 tracking - 1 watch + 10 drawing)
Step 4: If possible, launch the protocol for one block: set "Number of blocks" to "1" -> (10 tracking - 1 watch + 10 drawing)


Instructions:
-------------
Each session starts by a 20 second baseline during which the subject should simply fixate the red ball in the center.
To start a trial, the subject should place the mouse cursor on the red ball (target). 
The target will change to orange for a short time (5 seconds) during which a short instruction is presented. 

* (tracking condition [>> python flashtrack.py subjectID 1]) 10 x 10 second trial during which the subject tries to track the target (color ball). The target starts moving along the trajectory in the direction indicated by the red segment. The subject should follow the target with the mouse cursor, keeping it as close as possible to the target center. At the end, a score between 0 and 100 indicates the performance of the subject (100 is excellent). 

* (doodling condition [>> python flashtrack.py subjectID 2]) The subject first watches an example trajectory being "drawn" (i.e. the cursor moves on the screen at a certain velocity). Then the subject "draws" his own trajectory. She/he does not need to reproduce the example. There is no right or wrong drawing here. She/he should just freely doodle something of the type and try to go quite slow, like in the example.

Behavioral data:
----------------
The data is recorded in the "data" directory configured in the configflash.py script. Two files are recorded: one with the score and one with the behavior:
* SUBJECT_score_DATE_TIME.txt: 
Column 1: Trial number
Column 2: Trial type (0=draw, 1=track, 2=watch)
Column 3: Trajectory ID (0-9)
Column 4: [ignore]
Column 5: Mean error (=1-(score-20)/100)

* SUBJECT_behav_DATE_TIME.txt:
Column 1: frame number
Column 2: time elapsed since trial start [ms]
Column 3: [ignore]
Column 4: [ignore]
Column 5: [ignore]
Column 6: instantaneous error (distance [px] between cursor and target center)
Column 7: target position (X, [px])
Column 8: target position (Y, [px])
Column 9: cursor position (X, [px])
Column 10: cursor position (Y, [px])
Column 11: [ignore]
Column 12: [ignore]
